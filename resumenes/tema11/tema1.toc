\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{3}{section.1}
\contentsline {paragraph}{Definici\IeC {\'o}n de Base de Datos}{3}{section*.2}
\contentsline {section}{\numberline {2}Objetivos de una Base de Datos}{3}{section.2}
\contentsline {section}{\numberline {3}Arquitectura de las Bases de Datos}{4}{section.3}
\contentsline {section}{\numberline {4}Sistemas gestores de Bases de Datos -- SGDB}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Componentes}{4}{subsection.4.1}
\contentsline {section}{\numberline {5}Modelos de Bases de Datos}{4}{section.5}
\contentsline {subsection}{\numberline {5.1}Modelo de datos Jer\IeC {\'a}rquico}{4}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Modelo de datos en Red}{4}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Modelo de datos Relacional}{4}{subsection.5.3}
\contentsline {section}{\numberline {6}Arquitectura Cliente/Servidor}{4}{section.6}
