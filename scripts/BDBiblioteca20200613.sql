USE [master]
GO
/****** Object:  Database [BDBiblioteca]    Script Date: 13/06/2020 11:12:21 ******/
CREATE DATABASE [BDBiblioteca] ON  PRIMARY 
( NAME = N'Base de datos Administrador_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BDBiblioteca.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'Base de datos Administrador_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BDBiblioteca.ldf' , SIZE = 1024KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
GO
ALTER DATABASE [BDBiblioteca] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BDBiblioteca].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [BDBiblioteca] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BDBiblioteca] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BDBiblioteca] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BDBiblioteca] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BDBiblioteca] SET ARITHABORT OFF 
GO
ALTER DATABASE [BDBiblioteca] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [BDBiblioteca] SET AUTO_SHRINK ON 
GO
ALTER DATABASE [BDBiblioteca] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BDBiblioteca] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BDBiblioteca] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BDBiblioteca] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BDBiblioteca] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BDBiblioteca] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BDBiblioteca] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BDBiblioteca] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BDBiblioteca] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BDBiblioteca] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BDBiblioteca] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BDBiblioteca] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BDBiblioteca] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BDBiblioteca] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BDBiblioteca] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BDBiblioteca] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BDBiblioteca] SET  MULTI_USER 
GO
ALTER DATABASE [BDBiblioteca] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [BDBiblioteca] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BDBiblioteca', N'ON'
GO
USE [BDBiblioteca]
GO
/****** Object:  User [frank]    Script Date: 13/06/2020 11:12:22 ******/
CREATE USER [frank] FOR LOGIN [frank] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[fun]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fun](@cod_ed char(4)) RETURNS integer
AS BEGIN
    RETURN (SELECT MAX(precio) FROM tlibros WHERE editorial = @cod_ed)
END

GO
/****** Object:  UserDefinedFunction [dbo].[funcionEscalar]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[funcionEscalar] (@libro nvarchar(50)) RETURNS nvarchar(50)
AS BEGIN
	DECLARE @TituloAutor nvarchar(50)
	SELECT @TituloAutor = titulo + ' – ' + autor
		FROM tlibros
		WHERE titulo = @libro
	RETURN (@TituloAutor)
END

GO
/****** Object:  UserDefinedFunction [dbo].[funfun]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[funfun](@cod_ed char(4)) RETURNS varchar(50)
AS BEGIN
	DECLARE @titulo varchar(50)
	SELECT TOP 1 @titulo = l.titulo
		FROM tlibros AS l, teditoriales AS e
		WHERE l.editorial = e.codigo AND e.codigo = @cod_ed
	RETURN (@titulo)
END

GO
/****** Object:  UserDefinedFunction [dbo].[multifun]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[multifun](@_genero varchar(15), @_precio money)
    RETURNS @tout TABLE (
        titulo varchar(50),
        genero varchar(15),
        precio money
    ) BEGIN
        INSERT INTO @tout
            SELECT titulo, genero, precio 
                FROM tlibros
                WHERE genero = @_genero AND precio >= @_precio
        RETURN -- a secas, porque ya lo indicamos antes
    END

GO
/****** Object:  Table [dbo].[prueba]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[prueba](
	[codigo] [int] NOT NULL,
	[nombre] [varchar](23) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[teditoriales]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[teditoriales](
	[codigo] [char](4) NOT NULL,
	[nombre] [varchar](35) NULL,
	[codigo_prov] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tlibros]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tlibros](
	[isbn] [char](13) NOT NULL,
	[titulo] [varchar](50) NOT NULL,
	[autor] [varchar](30) NOT NULL,
	[genero] [varchar](15) NOT NULL,
	[encuadernacion] [nvarchar](15) NOT NULL,
	[editorial] [char](4) NOT NULL,
	[fech_ed] [datetime] NOT NULL,
	[num_ed] [numeric](10, 0) NOT NULL,
	[num_pag] [numeric](18, 0) NOT NULL,
	[precio] [money] NOT NULL,
 CONSTRAINT [PK_tLIBROS] PRIMARY KEY CLUSTERED 
(
	[isbn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tlibros1]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tlibros1](
	[isbn] [char](13) NOT NULL,
	[titulo] [varchar](50) NOT NULL,
	[autor] [varchar](30) NOT NULL,
	[genero] [varchar](15) NOT NULL,
	[encuadernacion] [nvarchar](15) NOT NULL,
	[editorial] [char](4) NOT NULL,
	[fech_ed] [datetime] NOT NULL,
	[num_ed] [numeric](10, 0) NOT NULL,
	[num_pag] [numeric](18, 0) NOT NULL,
	[precio] [money] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tprovincias]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tprovincias](
	[codigo] [int] NOT NULL,
	[nombre] [varchar](23) NULL,
PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[prueba] ([codigo], [nombre]) VALUES (1, N'Hawai')
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ABAL', N'Abalar', 15)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ACAN', N'Acantilado', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ALFA', N'Alfaguara', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ALIA', N'Alianza editorial', 8)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ANAG', N'Anagrama', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ANAY', N'Anaya', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ARIE', N'Ariel', 8)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'AUGU', N'Augusta', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'AVE ', N'Ave Fénix', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'BENC', N'Benchmark Education Company', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'BOOK', N'Books Pocket', 15)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'CEAC', N'CEAC', 8)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'CÍRC', N'Círculo de Lectores', 3)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'DE B', N'De bolsillo', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'DEBO', N'DEBOLSILLO', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'DEST', N'Destino', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ED. ', N'Ed. Univ. Politec. Valencia', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDAF', N'EDAF', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDB ', N'Ediciones B', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDCÁ', N'Ediciones Cátedra', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDDE', N'Ediciones del 4 de agosto', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDGE', N'Ediciones Gestión', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDHA', N'Edhasa', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDIG', N'Ediciones Igitur', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDIT', N'Editorial ORBIT', 15)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDLA', N'Edicións Laiovento', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDMA', N'Ediciones Martinez Roca', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDOR', N'Ediciones Orbis Fabbri', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDPO', N'Edicións Positivas', 3)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDSI', N'Ediciones Sirio', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EDVI', N'Edicións virtuals UPC', 8)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EL H', N'El hombre del saco', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'EMC ', N'EMC editores', 3)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ESPA', N'Espasa-Calpe', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'G.E.', N'G.E. Telecinco', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'GALA', N'Galaxia', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'HARP', N'Harper Collins', 3)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'INST', N'Inst. Nac. de A. Esc. y de la Mús.', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'LA C', N'La Cúpula', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'LA T', N'La Torre', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'LA V', N'La Voz de Galicia', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'LIBR', N'Librería-Editorial Dykinson 2000', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'MC G', N'Mc Graw Hill', 8)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'MINO', N'Minotauro', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'NERI', N'Neri Pozza Editore', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'NUEV', N'Nuevas ediciones de bolsillo', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'PLAN', N'Planeta', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'PLAZ', N'Plaza & Janés', 3)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'PUNT', N'Punto de Lectura', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'RBA ', N'RBA editores', 3)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'RIOP', N'Riopiedras 2000', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ROCA', N'Roca Editorial', 8)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'S.M.', N'S.M.', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'SALA', N'Salamandra', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'SANT', N'Santillana', 2)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'SOTE', N'Sotelo Blaco', 1)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'TEMA', N'Temas de hoy', 36)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'TOXO', N'Toxosoutos', 3)
INSERT [dbo].[teditoriales] ([codigo], [nombre], [codigo_prov]) VALUES (N'ZETA', N'Zeta', 2)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'0-261-10320-2', N'Sin titulo', N'John Ronald Reuel Tolkien', N'Fantástica', N'Lujo', N'HARP', CAST(N'1994-08-01T00:00:00.000' AS DateTime), CAST(9 AS Numeric(10, 0)), CAST(1150 AS Numeric(18, 0)), 21.9900)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'14-10-82338-0', N'De las pirámides a los rascacielos', N'Judith Dupreé', N'Ensayo', N'Bolsillo', N'BENC', CAST(N'2004-09-18T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(32 AS Numeric(18, 0)), 11.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-0133-700-0', N'¿Dónde te escondes?', N'Mary Higgins Clark', N'Novela', N'Lujo', N'PLAZ', CAST(N'2009-07-21T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(213 AS Numeric(18, 0)), 21.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-0133-720-8', N'El nombre del viento', N'Patrick Rothfuss', N'Novela', N'Rústica', N'PLAZ', CAST(N'2009-05-22T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(880 AS Numeric(18, 0)), 22.9000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-013-4219-6', N'El umbral de la eternidad', N'Ken Follet', N'Novela', N'Lujo', N'PLAZ', CAST(N'2014-09-02T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(1152 AS Numeric(18, 0)), 24.9000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-013-5319-2', N'El invierno del mundo', N'Ken Follet', N'Novela', N'Lujo', N'PLAZ', CAST(N'2012-09-15T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(936 AS Numeric(18, 0)), 24.9000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-42282-5', N'Fahrenheit 451', N'Ray Bradbury', N'Novela', N'Rústica', N'PLAZ', CAST(N'1994-08-05T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(175 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-46178-1', N'De la Tierra a la Luna', N'Julio Verne', N'Novela', N'Rústica', N'PLAZ', CAST(N'1998-08-04T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(199 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-46272-X', N'Rebeca', N'Daphne Du Maurier', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2008-02-07T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(540 AS Numeric(18, 0)), 8.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-49136-3', N'Fundación', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1991-04-05T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(255 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-49652-7', N'Fundación e imperio', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1991-08-03T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(254 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-49653-5', N'Segunda Fundación', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1991-04-05T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(251 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-49654-3', N'Los límites de la Fundación', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1997-10-09T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(476 AS Numeric(18, 0)), 11.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-08-04895-3', N'Net Force: Prioridades ocultas', N'Tom Clancy', N'Novela', N'Bolsillo', N'BOOK', CAST(N'2003-07-07T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(411 AS Numeric(18, 0)), 7.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-080-7492-2', N'Una noche de perros', N'Hugh Laurie', N'Novela', N'Rústica', N'BOOK', CAST(N'2007-01-08T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(416 AS Numeric(18, 0)), 7.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-08-08925-4', N'El Símbolo Perdido', N'Dan Brown', N'Novela', N'Rústica', N'PLAN', CAST(N'2009-05-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(590 AS Numeric(18, 0)), 21.9000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-1111-111-1', N'Los hijos de Húrin', N'John Ronald Reuel Tolkien', N'Fantástica', N'Rústica', N'MINO', CAST(N'2007-04-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(277 AS Numeric(18, 0)), 19.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-113-2415-X', N'Rimas y Leyendas', N'Gustavo Adolfo Bécquer', N'Poesía', N'Lujo', N'PLAN', CAST(N'1995-02-01T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(115 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-1137-542-X', N'Analísis estructurado moderno', N'Edward Fraguez', N'Ensayo', N'Rústica', N'PLAN', CAST(N'1993-08-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(723 AS Numeric(18, 0)), 28.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-135-7924-6', N'La Salchicha Peleona', N'Dennis Dugan', N'Novela', N'Lujo', N'PLAN', CAST(N'1997-01-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(666 AS Numeric(18, 0)), 14.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-149-4860-9', N'Dragonmaster', N'Chris Bunch', N'Novela', N'Bolsillo', N'EDIT', CAST(N'2007-07-07T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(850 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-2040-167-6', N'La colina de los trols', N'Katherine Langrish', N'Novela', N'Rústica', N'RIOP', CAST(N'2004-01-14T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(288 AS Numeric(18, 0)), 13.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-204-0170-6', N'Ensayo sobre la lucidez', N'José Saramago', N'Ensayo', N'Bolsillo', N'ALFA', CAST(N'2004-04-01T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(423 AS Numeric(18, 0)), 18.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-204-0494-3', N'Cuando ya no importe', N'Juan Carlos Onetti', N'Novela', N'Bolsillo', N'ALFA', CAST(N'2009-08-06T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(176 AS Numeric(18, 0)), 16.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-204-2353-1', N'El último Dickens', N'Matthew Pearl', N'Novela', N'Lujo', N'ALFA', CAST(N'2009-05-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(488 AS Numeric(18, 0)), 22.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-204-2354-8', N'Tormenta sobre Alejandría', N'Luis Manuel', N'Novela', N'Bolsillo', N'ALFA', CAST(N'2009-09-06T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(400 AS Numeric(18, 0)), 19.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-206-3311-9', N'El Aleph', N'Jorge Luís Borges', N'Relato Corto', N'Bolsillo', N'ALIA', CAST(N'1995-05-05T00:00:00.000' AS DateTime), CAST(14 AS Numeric(10, 0)), CAST(203 AS Numeric(18, 0)), 9.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-206-3319-4', N'Asi habló Zaratustra', N'Friedrich Nietzsche', N'Ensayo', N'Bolsillo', N'ALIA', CAST(N'1997-12-15T00:00:00.000' AS DateTime), CAST(6 AS Numeric(10, 0)), CAST(498 AS Numeric(18, 0)), 19.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-206-3491-3', N'Casa de muñecas', N'Henrik Ibsen', N'Teatro', N'Rústica', N'ALIA', CAST(N'2002-12-02T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(177 AS Numeric(18, 0)), 10.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-207-2634-6', N'El sí de las niñas', N'Leandro Fernández de Moratín', N'Teatro', N'Rústica', N'ANAY', CAST(N'1985-01-05T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(175 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-226-5005-3', N'El principito', N'Antoine De Saint Exupéry', N'Cuentos', N'Lujo', N'EMC ', CAST(N'2001-05-05T00:00:00.000' AS DateTime), CAST(15 AS Numeric(10, 0)), CAST(94 AS Numeric(18, 0)), 17.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-226-5312-5', N'África llora', N'Alberto Vázquez Figueroa', N'Novela', N'Bolsillo', N'CÍRC', CAST(N'1995-06-07T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(235 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-226-6412-7', N'Negreros', N'Alberto Vázquez Figueroa', N'Novela', N'Bolsillo', N'CÍRC', CAST(N'1997-09-07T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(317 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-0924-X', N'Historia de Rampa', N'T. Lobsang Rampa', N'Novela', N'Bolsillo', N'DEST', CAST(N'1987-08-02T00:00:00.000' AS DateTime), CAST(6 AS Numeric(10, 0)), CAST(252 AS Numeric(18, 0)), 7.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-1029-9', N'Un hombre', N'José María Gironella', N'Novela', N'Bolsillo', N'DEST', CAST(N'1980-07-02T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(363 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-1157-0', N'El tiempo', N'Ana María Matute', N'Novela', N'Bolsillo', N'DEST', CAST(N'1981-09-01T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(251 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-3872-X', N'El informe de Brodie', N'Jorge Luís Borges', N'Cuentos', N'Lujo', N'DEST', CAST(N'2006-10-11T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(153 AS Numeric(18, 0)), 21.7000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-4035-4', N'Cosmofobia', N'Lucía Etxebarría', N'Novela', N'Bolsillo', N'DEST', CAST(N'2008-07-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(432 AS Numeric(18, 0)), 8.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-2334-161-0', N'La reina en el palacio de las corrientes de aire', N'Stieg larsson', N'Novela', N'Bolsillo', N'DEST', CAST(N'2009-01-02T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(812 AS Numeric(18, 0)), 22.5500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-0150-5', N'El ingenioso hidalgo don quijote de la mancha', N'Miguel de Cervantes Saavedra', N'Novela', N'Bolsillo', N'ESPA', CAST(N'1982-02-03T00:00:00.000' AS DateTime), CAST(25 AS Numeric(10, 0)), CAST(673 AS Numeric(18, 0)), 25.3000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-1569-7', N'La Fundación', N'Antonio Buero Vallejo', N'Teatro', N'Bolsillo', N'ESPA', CAST(N'2006-09-05T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(158 AS Numeric(18, 0)), 8.9000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-9486-4', N'Ficciones', N'Jorge Luís Borges', N'Relato Corto', N'Lujo', N'ESPA', CAST(N'1999-08-03T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(156 AS Numeric(18, 0)), 6.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-9583-6', N'La Celestina', N'Fernando de Rojas', N'Teatro', N'Rústica', N'ESPA', CAST(N'2005-06-06T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(438 AS Numeric(18, 0)), 6.9200)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-9682-4', N'Nueve ensayos dantescos', N'Jorge Luís Borges', N'Relato Corto', N'Bolsillo', N'ESPA', CAST(N'1999-05-01T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(153 AS Numeric(18, 0)), 8.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-246-8013-5', N'Bollos preñados', N'Karlos Arguiñano', N'Ensayo', N'Bolsillo', N'PLAN', CAST(N'2007-07-07T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(321 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-270-3429-7', N'Lo que los hombres no saben', N'Lucía Etxebarría', N'Cuentos', N'Rústica', N'EDMA', CAST(N'2008-04-04T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(349 AS Numeric(18, 0)), 19.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-310-3074-0', N'Tokio Blues', N'Haruki Murakami', N'Novela', N'Lujo', N'CÍRC', CAST(N'2005-05-13T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(383 AS Numeric(18, 0)), 21.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-329-1710-3', N'El Lararillo de Tormes', N'Anónimo', N'Picaresca', N'Lujo', N'CEAC', CAST(N'1989-06-06T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(250 AS Numeric(18, 0)), 12.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-3321-962-5', N'Microeconomía', N'Stiglitz', N'Ensayo', N'Bolsillo', N'ARIE', CAST(N'1998-12-22T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(345 AS Numeric(18, 0)), 20.3700)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-339-2158-3', N'El antropólogo inocente', N'Nigel Barley', N'Ensayo', N'Bolsillo', N'ANAG', CAST(N'1983-04-03T00:00:00.000' AS DateTime), CAST(21 AS Numeric(10, 0)), CAST(235 AS Numeric(18, 0)), 18.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-339-6634-0', N'Compañía', N'Samuel Beckett', N'Novela', N'Bolsillo', N'ANAG', CAST(N'1999-12-15T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(76 AS Numeric(18, 0)), 5.2000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-350-1512-7', N'Historias de cronopios y de famas', N'Julio Cortazar', N'Cuentos', N'Bolsillo', N'EDHA', CAST(N'2007-02-12T00:00:00.000' AS DateTime), CAST(29 AS Numeric(10, 0)), CAST(144 AS Numeric(18, 0)), 14.7000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-350-3467-4', N'La muerte en Venecia', N'Thomas Mann', N'Novela', N'Lujo', N'EDHA', CAST(N'2006-10-16T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(106 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-373-0038-3', N'El sí de las niñas', N'Leandro fernández de moratín', N'Teatro', N'Rústica', N'EDCÁ', CAST(N'1989-03-03T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(167 AS Numeric(18, 0)), 10.4000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0057-X', N'Don álvaro o la fuerza del sino', N'Duque de rivas', N'Teatro', N'Rústica', N'EDCÁ', CAST(N'1990-10-04T00:00:00.000' AS DateTime), CAST(9 AS Numeric(10, 0)), CAST(170 AS Numeric(18, 0)), 19.9000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0092-8', N'La vida es sueño', N'Pedro calderón de la barca', N'Teatro', N'Rústica', N'EDCÁ', CAST(N'1991-09-02T00:00:00.000' AS DateTime), CAST(10 AS Numeric(10, 0)), CAST(207 AS Numeric(18, 0)), 6.9000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0114-2', N'Poema del cante jondo. Romacero gitano', N'Federico García Lorca', N'Poesía', N'Bolsillo', N'EDCÁ', CAST(N'1985-05-05T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(315 AS Numeric(18, 0)), 10.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0121-4', N'El alcalde de Zalamea', N'Pedro Calderon de la Barca', N'Teatro', N'Bolsillo ', N'EDCÁ', CAST(N'1995-02-03T00:00:00.000' AS DateTime), CAST(13 AS Numeric(10, 0)), CAST(200 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0121-5', N'Un marido ideal. Una mujer sin importancia', N'Oscar Wilde', N'Teatro', N'Bolsillo', N'EDAF', CAST(N'2003-08-03T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(243 AS Numeric(18, 0)), 6.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0191-1', N'Soledades', N'Luis de Góngora y Argote', N'Poesía', N'Bolsillo', N'EDCÁ', CAST(N'2006-04-12T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(94 AS Numeric(18, 0)), 6.6000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0279-3', N'Altazor y Temblor de cielo', N'Vicente Huidobro', N'Novela', N'Bolsillo', N'EDCÁ', CAST(N'1981-12-16T00:00:00.000' AS DateTime), CAST(12 AS Numeric(10, 0)), CAST(190 AS Numeric(18, 0)), 7.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-3761354-0 ', N'Abel Sánchez', N'Miguel de Unamuno', N'Novela', N'Bolsillo', N'EDCÁ', CAST(N'2003-06-04T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(207 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8367-2', N'En la  hoguera', N'J. Fernández Santos', N'Novela', N'Lujo', N'PLAN', CAST(N'2000-03-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(199 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8367-5', N'La vieja sirena', N'José Luís Sampedro', N'Novela', N'Rústica', N'PLAN', CAST(N'1999-03-03T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(297 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8371-0', N'Crónica del rey pasmado', N'Gonzálo Torrente Ballester', N'Novela', N'Lujo', N'PLAN', CAST(N'2000-05-01T00:00:00.000' AS DateTime), CAST(11 AS Numeric(10, 0)), CAST(230 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8546-2', N'Bella y oscura', N'Rosa Montero', N'Novela', N'Lujo', N'PLAN', CAST(N'2000-04-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(204 AS Numeric(18, 0)), 14.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8550-0', N'Mujeres de ojos grandes', N'Ángeles Mastretta', N'Novela', N'Lujo', N'PLAN', CAST(N'2000-10-17T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(221 AS Numeric(18, 0)), 14.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8551-9', N'Lola', N'María de la Pau Janer', N'Novela', N'Lujo', N'PLAN', CAST(N'2000-10-09T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(381 AS Numeric(18, 0)), 16.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-9026-1', N'El engaño de Beth Loring', N'Fernando Schwartz', N'Novela', N'Lujo', N'PLAN', CAST(N'2000-06-02T00:00:00.000' AS DateTime), CAST(6 AS Numeric(10, 0)), CAST(294 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-402-2246-7', N'Muerte accidental de un anarquista', N'Dario Fo', N'Teatro', N'Lujo', N'EDOR', CAST(N'1998-07-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(144 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-406-2955-9', N'La amenaza de Andrómeda', N'Michael Crichton', N'Novela', N'Bolsillo', N'EDB ', CAST(N'1992-08-03T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(334 AS Numeric(18, 0)), 7.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-414-0280-9', N'El sueño de una noche de verano', N'William Shakespeare', N'Teatro', N'Bolsillo', N'EDAF', CAST(N'2003-07-03T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(225 AS Numeric(18, 0)), 6.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-414-0517-2', N'Anillos para una dama', N'Antonio Gala', N'Teatro', N'Bolsillo', N'EDAF', CAST(N'2001-11-05T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(217 AS Numeric(18, 0)), 6.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-414-0894-7', N'Leyendas', N'Gustavo Adolfo Béquer', N'Novela', N'Bolsillo', N'EDAF', CAST(N'2001-05-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(267 AS Numeric(18, 0)), 8.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-415-1922-6', N'Hacker', N'Justo Pérez', N'Ensayo', N'Lujo', N'ANAY', CAST(N'2006-06-06T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(1132 AS Numeric(18, 0)), 54.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-450-7038-4', N'El Silmarillion', N'John Ronald Reuel Tolkien', N'Novela', N'Bolsillo', N'MINO', CAST(N'1986-10-22T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(494 AS Numeric(18, 0)), 16.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-450-7140-8', N'El Señor de los Anillos: La comunidad del anillo', N'John Ronald Reuel Tolkien', N'Novela', N'Lujo', N'MINO', CAST(N'1996-04-01T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(547 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-450-7176-9', N'El Señor de los Anillos: Las dos torres', N'John Ronald Reuel Tolkien', N'Novela', N'Lujo', N'MINO', CAST(N'1996-04-01T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(461 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-450-7177-7', N'El Señor de los Anillos: El retorno del Rey', N'John Ronald Reuel Tolkien', N'Novela', N'Lujo', N'MINO', CAST(N'1996-04-01T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(414 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-473-0619-4', N'Cien años de soledad', N'Gabriel García Márquez', N'Novela', N'Bolsillo', N'RBA ', CAST(N'1994-06-05T00:00:00.000' AS DateTime), CAST(18 AS Numeric(10, 0)), CAST(315 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-473-3393-0', N'Relato de un náufrago', N'Gabriel García Márquez', N'Novela', N'Rústica', N'RBA ', CAST(N'2004-05-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(128 AS Numeric(18, 0)), 7.6400)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-486-0203-X', N'Harrison: principios de la medicina interna. VOL.1', N'VV.AA.', N'Ensayo', N'Lujo', N'MC G', CAST(N'1989-05-03T00:00:00.000' AS DateTime), CAST(24 AS Numeric(10, 0)), CAST(1600 AS Numeric(18, 0)), 119.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-486-0204-8', N'Harrison: principios de la medicina interna. VOL.2', N'VV.AA.', N'Ensayo', N'Lujo', N'MC G', CAST(N'1989-05-03T00:00:00.000' AS DateTime), CAST(24 AS Numeric(10, 0)), CAST(1598 AS Numeric(18, 0)), 119.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-60-56058-6', N'Pedazo de dic. of. y cab. de comp. de Chiquitistán', N'Lucas Grijander', N'Cuentos', N'Bolsillo', N'G.E.', CAST(N'1997-02-02T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(222 AS Numeric(18, 0)), 12.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-613-3001-0', N'El viaje íntimo de la locura', N'Roberto Iniesta', N'Novela', N'Rústica', N'EL H', CAST(N'2009-09-10T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(371 AS Numeric(18, 0)), 18.2700)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-663-0848-2', N'Sefarad', N'Antonio Múñoz Molina', N'Novela', N'Bolsillo', N'SANT', CAST(N'2002-05-05T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(516 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-663-1899-2', N'Momo', N'Michael Endel', N'Novela', N'Rústica', N'PUNT', CAST(N'2006-11-03T00:00:00.000' AS DateTime), CAST(18 AS Numeric(10, 0)), CAST(258 AS Numeric(18, 0)), 12.6000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-666-3352-9', N'Coltan', N'Alberto Vázquez Figueroa', N'Novela', N'Lujo', N'EDB ', CAST(N'2008-06-22T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(304 AS Numeric(18, 0)), 17.3100)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-6663-392-5', N'Venganza', N'Brian Freeman', N'Novela', N'Lujo', N'EDB ', CAST(N'2008-03-07T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(400 AS Numeric(18, 0)), 20.6500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-667-5188-2', N'Todos los detectives se llaman Flanagan', N'Andreu Martín', N'Novela', N'Bolsillo', N'ANAY', CAST(N'2006-04-13T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(232 AS Numeric(18, 0)), 9.6200)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-1192-0', N'La dama boba', N'Félix Lope de Vega', N'Teatro', N'Bolsillo', N'ESPA', CAST(N'2003-07-06T00:00:00.000' AS DateTime), CAST(15 AS Numeric(10, 0)), CAST(134 AS Numeric(18, 0)), 8.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-1994-0', N'De todo lo visible y lo invisible', N'Lucía Etxebarría', N'Novela', N'Bolsillo', N'PLAN', CAST(N'2007-08-04T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(541 AS Numeric(18, 0)), 12.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-2178-0', N'Cuatro corazones con freno y marcha atrás', N'Enrique Jardiel Poncela', N'Teatro', N'Bolsillo', N'ESPA', CAST(N'2006-04-02T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(187 AS Numeric(18, 0)), 9.9000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-2232-2', N'La sirena varada. Los árboles mueren de pie', N'Alejandro Casona', N'Teatro', N'Bolsillo', N'ESPA', CAST(N'2007-11-03T00:00:00.000' AS DateTime), CAST(17 AS Numeric(10, 0)), CAST(200 AS Numeric(18, 0)), 7.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-2611-5', N'El pensamiento negativo', N'Risto Mejide', N'Ensayo', N'Rústica', N'ESPA', CAST(N'2008-03-03T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(192 AS Numeric(18, 0)), 6.6800)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-672-0688-8', N'Un pez gordo', N'Daniel Wallace', N'Novela', N'Lujo', N'CÍRC', CAST(N'2005-07-05T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(168 AS Numeric(18, 0)), 18.7500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-67-53523-5', N'Bel Amor más allá de la muerte', N'Care Santos', N'Novela', N'Bolsillo', N'S.M.', CAST(N'2009-08-04T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(387 AS Numeric(18, 0)), 17.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-6840-237-1', N'Corina fácil', N'Martín', N'Ensayo', N'Bolsillo', N'ANAY', CAST(N'2004-11-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(123 AS Numeric(18, 0)), 12.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7154-091-1', N'Made in Galiza', N'Séchu Sende', N'Novela', N'Bolsillo', N'GALA', CAST(N'2008-10-01T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(160 AS Numeric(18, 0)), 13.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7213-154-5', N'Porque soy judio', N'Edmond Fleg', N'Ensayo', N'Bolsillo', N'ALFA', CAST(N'2000-03-31T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(92 AS Numeric(18, 0)), 8.8000)
GO
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7461-034-6', N'La divina comedia', N'Dante Alighieri', N'Novela', N'Rústica', N'AUGU', CAST(N'1992-02-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(384 AS Numeric(18, 0)), 25.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7640-724-6', N'La tercera palabra', N'Alejandro Casona', N'Teatro', N'Bolsillo', N'EDAF', CAST(N'2001-08-04T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(147 AS Numeric(18, 0)), 5.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7789-332-8', N'Historia del siglo XX', N'Eric Hobs Bawn', N'Narrativa', N'Bolsillo', N'ABAL', CAST(N'1998-10-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(154 AS Numeric(18, 0)), 10.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7824-072-1', N'Os cravos de prata', N'Nicolás Bela', N'Teatro', N'Rústica', N'SOTE', CAST(N'1990-01-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(49 AS Numeric(18, 0)), 6.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7833-690-6', N'Pobre cabrón', N'Joe Matt', N'Cuentos', N'Bolsillo', N'LA C', CAST(N'2008-06-12T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(180 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7871-791-9', N'Matemática, ¿estás ahí?', N'Adrián Paenza', N'Ensayo', N'Bolsillo', N'RBA ', CAST(N'2006-05-01T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(253 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7960-198-1', N'El año de la liebre', N'Arto Paosilino', N'Novela', N'Bolsillo', N'LA T', CAST(N'1998-10-30T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(195 AS Numeric(18, 0)), 14.3000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8130-133-7', N'El túnel', N'Ernesto Sábato', N'Novela', N'Rústica', N'ALFA', CAST(N'1999-07-20T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(128 AS Numeric(18, 0)), 8.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-81-55685-8', N'Globalización ciudadanía y derechos  h.', N'Maria Jose Fariñas Dulce', N'Ensayo', N'Bolsillo', N'LIBR', CAST(N'2009-07-11T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(65 AS Numeric(18, 0)), 16.7500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-83-01785-7', N'Dimensiones de la sostenibilidad', N'Ezequiel Usón Guardiola', N'Ensayo', N'Rústica', N'EDVI', CAST(N'1996-06-12T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(65 AS Numeric(18, 0)), 11.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8346-667-3', N'La historia de Lisey', N'Stephen King', N'Terror', N'Rústica', N'DE B', CAST(N'2007-04-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(605 AS Numeric(18, 0)), 16.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8346-934-7', N'Te daré la tierra', N'Chufo Llorens', N'Novela', N'Rústica', N'NUEV', CAST(N'2009-04-03T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(752 AS Numeric(18, 0)), 11.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-83-63054-0', N'Estr. y tact. en Dir. y Gest. de proy.', N'Luis Jose Amendola', N'Ensayo', N'Bolsillo', N'ED. ', CAST(N'2007-01-31T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(218 AS Numeric(18, 0)), 21.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8450-037-3', N'La montaña mágica', N'Thomas Mann', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2001-04-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(974 AS Numeric(18, 0)), 14.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8450-205-8', N'El péndulo de Foucault', N'Umberto Eco', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2001-04-04T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(818 AS Numeric(18, 0)), 10.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8450-211-2', N'Siddhartha', N'Hermann Hesse', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2001-04-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(211 AS Numeric(18, 0)), 7.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-84-60796-0', N'Preguntale al economista camuflado', N'Tim Harford', N'Ensayo', N'Bolsillo', N'TEMA', CAST(N'2009-09-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(300 AS Numeric(18, 0)), 18.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8487-061-8', N'Rinoceronte', N'Eugene Ionesco', N'Teatro', N'Rústica', N'EDLA', CAST(N'2004-02-06T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(165 AS Numeric(18, 0)), 10.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8487-065-0', N'Paso de cebra', N'Gustavo Pernas Cora', N'Teatro', N'Rústica', N'EDLA', CAST(N'2004-06-02T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(187 AS Numeric(18, 0)), 10.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8487-070-7', N'A estrela da mañá.', N'Michael Löwy', N'Ensayo', N'Rústica', N'EDLA', CAST(N'2005-10-03T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(133 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8773-150-1', N'Dentro de la tierra', N'Paco Bezerra', N'Teatro', N'Rústica', N'INST', CAST(N'2007-05-03T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(104 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-87783-10-4', N'Retallos do diario de Adán', N'Mark Twain', N'Cuentos', N'Lujo', N'EDPO', CAST(N'1992-05-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(90 AS Numeric(18, 0)), 20.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-89669-27-9', N'La metamorfósis y otros relatos', N'Franz Kafka', N'Novela', N'Lujo', N'ANAY', CAST(N'2002-06-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(159 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9242-933-7', N'Helena de Troya', N'Margaret George', N'Novela', N'Rústica', N'ROCA', CAST(N'2008-06-23T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(784 AS Numeric(18, 0)), 23.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9251-651-3', N'Ángeles y demonios', N'Dan Brown', N'Novela', N'Bolsillo', N'BOOK', CAST(N'2000-08-02T00:00:00.000' AS DateTime), CAST(6 AS Numeric(10, 0)), CAST(736 AS Numeric(18, 0)), 19.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-933571-5-3', N'León manso come mierda', N'Kutxi Romero', N'Poesia', N'Rústica', N'EDDE', CAST(N'2004-05-23T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(86 AS Numeric(18, 0)), 9.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-937-1104-8', N'La metamorfósis y otros relatos', N'Franz Kafka', N'Novela', N'Bolsillo', N'EDCÁ', CAST(N'2008-02-23T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(280 AS Numeric(18, 0)), 12.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-95142-05-8', N'Huesos de sepia', N'Eugenio Montale', N'Poesía', N'Rústica', N'EDIG', CAST(N'2000-11-12T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(169 AS Numeric(18, 0)), 11.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-95359-92-8', N'Momentos estelares de la humanidad', N'Stefan Zweig', N'Ensayo', N'Bolsillo', N'ACAN', CAST(N'2002-10-21T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(306 AS Numeric(18, 0)), 20.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-96259-20-X', N'A unlla da águia', N'Ramón Carredano Cobas', N'Novela', N'Bolsillo', N'TOXO', CAST(N'2004-04-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(60 AS Numeric(18, 0)), 8.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-96390-97-7', N'Don Juan Tenorio', N'José Zorrila', N'Teatro', N'Bolsillo', N'SANT', CAST(N'2004-06-02T00:00:00.000' AS DateTime), CAST(14 AS Numeric(10, 0)), CAST(205 AS Numeric(18, 0)), 9.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-975-6476-6', N'Esperando a Godót', N'Samuel Beckett', N'Teatro', N'Bolsillo', N'EDSI', CAST(N'2007-04-03T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(208 AS Numeric(18, 0)), 18.5000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9757-233-5', N'Cuando éramos caníbales', N'Carles Lalueza Fox', N'Ensayo', N'Bolsillo', N'LA V', CAST(N'2006-08-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(112 AS Numeric(18, 0)), 3.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9759-922-5', N'Fundación y Tierra', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2005-10-27T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(524 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9793-924-7', N'La torre de la sociedad', N'Valerio Massimo Manfredi', N'Novela', N'Bolsillo', N'DE B', CAST(N'2006-11-11T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(317 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9838-241-9', N'La maestra de piano', N'Janice Y.K. Lee', N'Novela', N'Rústica', N'SALA', CAST(N'2009-09-19T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(352 AS Numeric(18, 0)), 19.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-98-67601-3', N'Las marismas', N'Arnaldur Indridason', N'Novela', N'Rústica', N'RBA ', CAST(N'2009-02-03T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(288 AS Numeric(18, 0)), 16.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9872-276-5', N'Vivos y muertos', N'Alberto Vázquez Figueroa', N'Narrativa', N'Bolsillo', N'ZETA', CAST(N'2009-04-03T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(384 AS Numeric(18, 0)), 8.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-98-75042-3', N'Claves del nuevo marketing', N'VV. AA.', N'Ensayo', N'Rústica', N'EDGE', CAST(N'2009-04-04T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(208 AS Numeric(18, 0)), 14.9600)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-998-9980-0', N'La caída de los gigantes', N'Ken Follet', N'Novela', N'Lujo', N'DEBO', CAST(N'2010-06-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(1024 AS Numeric(18, 0)), 24.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'88-545-0003-1', N'L''arte della guerra', N'Sun Tzu', N'Ensayo', N'Bolsillo', N'NERI', CAST(N'2007-03-03T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(377 AS Numeric(18, 0)), 11.0000)
INSERT [dbo].[tlibros] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'987-1138-01-6', N'Crónica de una muerte anunciada', N'Gabriel García Márquez', N'Novela', N'Rústica', N'AVE ', CAST(N'1994-07-15T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(144 AS Numeric(18, 0)), 9.8200)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'0-261-10320-2', N'Sin titulo', N'John Ronald Reuel Tolkien', N'Fantástica', N'Lujo', N'HARP', CAST(N'1994-08-01T00:00:00.000' AS DateTime), CAST(9 AS Numeric(10, 0)), CAST(1150 AS Numeric(18, 0)), 21.9900)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'14-10-82338-0', N'De las pirámides a los rascacielos', N'Judith Dupreé', N'Ensayo', N'Bolsillo', N'BENC', CAST(N'2004-09-18T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(32 AS Numeric(18, 0)), 11.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-0133-700-0', N'¿Dónde te escondes?', N'Mary Higgins Clark', N'Novela', N'Lujo', N'PLAZ', CAST(N'2009-07-21T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(213 AS Numeric(18, 0)), 21.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-0133-720-8', N'El nombre del viento', N'Patrick Rothfuss', N'Novela', N'Lujo', N'PLAZ', CAST(N'2009-05-22T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(880 AS Numeric(18, 0)), 22.9000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-013-4219-6', N'El umbral de la eternidad', N'Ken Follet', N'Novela', N'Lujo', N'PLAZ', CAST(N'2014-09-02T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(1152 AS Numeric(18, 0)), 24.9000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-013-5319-2', N'El invierno del mundo', N'Ken Follet', N'Novela', N'Lujo', N'PLAZ', CAST(N'2012-09-15T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(936 AS Numeric(18, 0)), 24.9000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-42282-5', N'Fahrenheit 451', N'Ray Bradbury', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1994-08-05T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(175 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-46178-1', N'De la Tierra a la Luna', N'Julio Verne', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1998-08-04T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(199 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-46272-X', N'Rebeca', N'Daphne Du Maurier', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2008-02-07T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(540 AS Numeric(18, 0)), 8.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-49136-3', N'Fundación', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1991-04-05T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(255 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-49652-7', N'Fundación e imperio', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1991-08-03T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(254 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-49653-5', N'Segunda Fundación', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1991-04-05T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(251 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-01-49654-3', N'Los límites de la Fundación', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'1997-10-09T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(476 AS Numeric(18, 0)), 11.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-08-04895-3', N'Net Force: Prioridades ocultas', N'Tom Clancy', N'Novela', N'Bolsillo', N'BOOK', CAST(N'2003-07-07T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(411 AS Numeric(18, 0)), 7.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-080-7492-2', N'Una noche de perros', N'Hugh Laurie', N'Novela', N'Bolsillo', N'BOOK', CAST(N'2007-01-08T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(416 AS Numeric(18, 0)), 7.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-08-08925-4', N'El Símbolo Perdido', N'Dan Brown', N'Novela', N'Lujo', N'PLAN', CAST(N'2009-05-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(590 AS Numeric(18, 0)), 21.9000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-1111-111-1', N'Los hijos de Húrin', N'John Ronald Reuel Tolkien', N'Fantástica', N'Lujo', N'MINO', CAST(N'2007-04-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(277 AS Numeric(18, 0)), 19.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-113-2415-X', N'Rimas y Leyendas', N'Gustavo Adolfo Bécquer', N'Poesía', N'Bolsillo', N'PLAN', CAST(N'1995-02-01T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(115 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-1137-542-X', N'Analísis estructurado moderno', N'Edward Fraguez', N'Ensayo', N'Lujo', N'PLAN', CAST(N'1993-08-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(723 AS Numeric(18, 0)), 28.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-135-7924-6', N'La Salchicha Peleona', N'Dennis Dugan', N'Novela', N'Lujo', N'PLAN', CAST(N'1997-01-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(666 AS Numeric(18, 0)), 14.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-149-4860-9', N'Dragonmaster', N'Chris Bunch', N'Novela', N'Lujo', N'EDIT', CAST(N'2007-07-07T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(850 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-2040-167-6', N'La colina de los trols', N'Katherine Langrish', N'Novela', N'Bolsillo', N'RIOP', CAST(N'2004-01-14T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(288 AS Numeric(18, 0)), 13.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-204-0170-6', N'Ensayo sobre la lucidez', N'José Saramago', N'Ensayo', N'Lujo', N'ALFA', CAST(N'2004-04-01T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(423 AS Numeric(18, 0)), 18.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-204-0494-3', N'Cuando ya no importe', N'Juan Carlos Onetti', N'Novela', N'Lujo', N'ALFA', CAST(N'2009-08-06T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(176 AS Numeric(18, 0)), 16.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-204-2353-1', N'El último Dickens', N'Matthew Pearl', N'Novela', N'Lujo', N'ALFA', CAST(N'2009-05-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(488 AS Numeric(18, 0)), 22.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-204-2354-8', N'Tormenta sobre Alejandría', N'Luis Manuel', N'Novela', N'Lujo', N'ALFA', CAST(N'2009-09-06T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(400 AS Numeric(18, 0)), 19.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-206-3311-9', N'El Aleph', N'Jorge Luís Borges', N'Relato Corto', N'Bolsillo', N'ALIA', CAST(N'1995-05-05T00:00:00.000' AS DateTime), CAST(14 AS Numeric(10, 0)), CAST(203 AS Numeric(18, 0)), 9.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-206-3319-4', N'Asi habló Zaratustra', N'Friedrich Nietzsche', N'Ensayo', N'Lujo', N'ALIA', CAST(N'1997-12-15T00:00:00.000' AS DateTime), CAST(6 AS Numeric(10, 0)), CAST(498 AS Numeric(18, 0)), 19.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-206-3491-3', N'Casa de muñecas', N'Henrik Ibsen', N'Teatro', N'Bolsillo', N'ALIA', CAST(N'2002-12-02T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(177 AS Numeric(18, 0)), 10.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-207-2634-6', N'El sí de las niñas', N'Leandro Fernández de Moratín', N'Teatro', N'Bolsillo', N'ANAY', CAST(N'1985-01-05T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(175 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-226-5005-3', N'El principito', N'Antoine De Saint Exupéry', N'Cuentos', N'Lujo', N'EMC ', CAST(N'2001-05-05T00:00:00.000' AS DateTime), CAST(15 AS Numeric(10, 0)), CAST(94 AS Numeric(18, 0)), 17.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-226-5312-5', N'África llora', N'Alberto Vázquez Figueroa', N'Novela', N'Bolsillo', N'CÍRC', CAST(N'1995-06-07T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(235 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-226-6412-7', N'Negreros', N'Alberto Vázquez Figueroa', N'Novela', N'Bolsillo', N'CÍRC', CAST(N'1997-09-07T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(317 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-0924-X', N'Historia de Rampa', N'T. Lobsang Rampa', N'Novela', N'Bolsillo', N'DEST', CAST(N'1987-08-02T00:00:00.000' AS DateTime), CAST(6 AS Numeric(10, 0)), CAST(252 AS Numeric(18, 0)), 7.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-1029-9', N'Un hombre', N'José María Gironella', N'Novela', N'Bolsillo', N'DEST', CAST(N'1980-07-02T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(363 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-1157-0', N'El tiempo', N'Ana María Matute', N'Novela', N'Bolsillo', N'DEST', CAST(N'1981-09-01T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(251 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-3872-X', N'El informe de Brodie', N'Jorge Luís Borges', N'Cuentos', N'Lujo', N'DEST', CAST(N'2006-10-11T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(153 AS Numeric(18, 0)), 21.7000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-233-4035-4', N'Cosmofobia', N'Lucía Etxebarría', N'Novela', N'Bolsillo', N'DEST', CAST(N'2008-07-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(432 AS Numeric(18, 0)), 8.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-2334-161-0', N'La reina en el palacio de las corrientes de aire', N'Stieg larsson', N'Novela', N'Lujo', N'DEST', CAST(N'2009-01-02T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(812 AS Numeric(18, 0)), 22.5500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-0150-5', N'El ingenioso hidalgo don quijote de la mancha', N'Miguel de Cervantes Saavedra', N'Novela', N'Lujo', N'ESPA', CAST(N'1982-02-03T00:00:00.000' AS DateTime), CAST(25 AS Numeric(10, 0)), CAST(673 AS Numeric(18, 0)), 25.3000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-1569-7', N'La Fundación', N'Antonio Buero Vallejo', N'Teatro', N'Bolsillo', N'ESPA', CAST(N'2006-09-05T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(158 AS Numeric(18, 0)), 8.9000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-9486-4', N'Ficciones', N'Jorge Luís Borges', N'Relato Corto', N'Bolsillo', N'ESPA', CAST(N'1999-08-03T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(156 AS Numeric(18, 0)), 6.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-9583-6', N'La Celestina', N'Fernando de Rojas', N'Teatro', N'Bolsillo', N'ESPA', CAST(N'2005-06-06T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(438 AS Numeric(18, 0)), 6.9200)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-239-9682-4', N'Nueve ensayos dantescos', N'Jorge Luís Borges', N'Relato Corto', N'Bolsillo', N'ESPA', CAST(N'1999-05-01T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(153 AS Numeric(18, 0)), 8.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-246-8013-5', N'Bollos preñados', N'Karlos Arguiñano', N'Ensayo', N'Bolsillo', N'PLAN', CAST(N'2007-07-07T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(321 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-270-3429-7', N'Lo que los hombres no saben', N'Lucía Etxebarría', N'Cuentos', N'Lujo', N'EDMA', CAST(N'2008-04-04T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(349 AS Numeric(18, 0)), 19.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-310-3074-0', N'Tokio Blues', N'Haruki Murakami', N'Novela', N'Lujo', N'CÍRC', CAST(N'2005-05-13T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(383 AS Numeric(18, 0)), 21.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-329-1710-3', N'El Lararillo de Tormes', N'Anónimo', N'Picaresca', N'Bolsillo', N'CEAC', CAST(N'1989-06-06T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(250 AS Numeric(18, 0)), 12.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-3321-962-5', N'Microeconomía', N'Stiglitz', N'Ensayo', N'Lujo', N'ARIE', CAST(N'1998-12-22T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(345 AS Numeric(18, 0)), 20.3700)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-339-2158-3', N'El antropólogo inocente', N'Nigel Barley', N'Ensayo', N'Lujo', N'ANAG', CAST(N'1983-04-03T00:00:00.000' AS DateTime), CAST(21 AS Numeric(10, 0)), CAST(235 AS Numeric(18, 0)), 18.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-339-6634-0', N'Compañía', N'Samuel Beckett', N'Novela', N'Bolsillo', N'ANAG', CAST(N'1999-12-15T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(76 AS Numeric(18, 0)), 5.2000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-350-1512-7', N'Historias de cronopios y de famas', N'Julio Cortazar', N'Cuentos', N'Bolsillo', N'EDHA', CAST(N'2007-02-12T00:00:00.000' AS DateTime), CAST(29 AS Numeric(10, 0)), CAST(144 AS Numeric(18, 0)), 14.7000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-350-3467-4', N'La muerte en Venecia', N'Thomas Mann', N'Novela', N'Bolsillo', N'EDHA', CAST(N'2006-10-16T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(106 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-373-0038-3', N'El sí de las niñas', N'Leandro fernández de moratín', N'Teatro', N'Bolsillo', N'EDCÁ', CAST(N'1989-03-03T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(167 AS Numeric(18, 0)), 10.4000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0057-X', N'Don álvaro o la fuerza del sino', N'Duque de rivas', N'Teatro', N'Lujo', N'EDCÁ', CAST(N'1990-10-04T00:00:00.000' AS DateTime), CAST(9 AS Numeric(10, 0)), CAST(170 AS Numeric(18, 0)), 19.9000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0092-8', N'La vida es sueño', N'Pedro calderón de la barca', N'Teatro', N'Bolsillo', N'EDCÁ', CAST(N'1991-09-02T00:00:00.000' AS DateTime), CAST(10 AS Numeric(10, 0)), CAST(207 AS Numeric(18, 0)), 6.9000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0114-2', N'Poema del cante jondo. Romacero gitano', N'Federico García Lorca', N'Poesía', N'Bolsillo', N'EDCÁ', CAST(N'1985-05-05T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(315 AS Numeric(18, 0)), 10.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0121-4', N'El alcalde de Zalamea', N'Pedro Calderon de la Barca', N'Teatro', N'Bolsillo', N'EDCÁ', CAST(N'1995-02-03T00:00:00.000' AS DateTime), CAST(13 AS Numeric(10, 0)), CAST(200 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0121-5', N'Un marido ideal. Una mujer sin importancia', N'Oscar Wilde', N'Teatro', N'Bolsillo', N'EDAF', CAST(N'2003-08-03T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(243 AS Numeric(18, 0)), 6.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0191-1', N'Soledades', N'Luis de Góngora y Argote', N'Poesía', N'Bolsillo', N'EDCÁ', CAST(N'2006-04-12T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(94 AS Numeric(18, 0)), 6.6000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-376-0279-3', N'Altazor y Temblor de cielo', N'Vicente Huidobro', N'Novela', N'Bolsillo', N'EDCÁ', CAST(N'1981-12-16T00:00:00.000' AS DateTime), CAST(12 AS Numeric(10, 0)), CAST(190 AS Numeric(18, 0)), 7.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-3761354-0 ', N'Abel Sánchez', N'Miguel de Unamuno', N'Novela', N'Lujo', N'EDCÁ', CAST(N'2003-06-04T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(207 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8367-2', N'En la  hoguera', N'J. Fernández Santos', N'Novela', N'Bolsillo', N'PLAN', CAST(N'2000-03-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(199 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8367-5', N'La vieja sirena', N'José Luís Sampedro', N'Novela', N'Bolsillo', N'PLAN', CAST(N'1999-03-03T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(297 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8371-0', N'Crónica del rey pasmado', N'Gonzálo Torrente Ballester', N'Novela', N'Bolsillo', N'PLAN', CAST(N'2000-05-01T00:00:00.000' AS DateTime), CAST(11 AS Numeric(10, 0)), CAST(230 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8546-2', N'Bella y oscura', N'Rosa Montero', N'Novela', N'Bolsillo', N'PLAN', CAST(N'2000-04-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(204 AS Numeric(18, 0)), 14.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8550-0', N'Mujeres de ojos grandes', N'Ángeles Mastretta', N'Novela', N'Bolsillo', N'PLAN', CAST(N'2000-10-17T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(221 AS Numeric(18, 0)), 14.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-8551-9', N'Lola', N'María de la Pau Janer', N'Novela', N'Lujo', N'PLAN', CAST(N'2000-10-09T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(381 AS Numeric(18, 0)), 16.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-395-9026-1', N'El engaño de Beth Loring', N'Fernando Schwartz', N'Novela', N'Bolsillo', N'PLAN', CAST(N'2000-06-02T00:00:00.000' AS DateTime), CAST(6 AS Numeric(10, 0)), CAST(294 AS Numeric(18, 0)), 9.7500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-402-2246-7', N'Muerte accidental de un anarquista', N'Dario Fo', N'Teatro', N'Bolsillo', N'EDOR', CAST(N'1998-07-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(144 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-406-2955-9', N'La amenaza de Andrómeda', N'Michael Crichton', N'Novela', N'Bolsillo', N'EDB ', CAST(N'1992-08-03T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(334 AS Numeric(18, 0)), 7.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-414-0280-9', N'El sueño de una noche de verano', N'William Shakespeare', N'Teatro', N'Bolsillo', N'EDAF', CAST(N'2003-07-03T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(225 AS Numeric(18, 0)), 6.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-414-0517-2', N'Anillos para una dama', N'Antonio Gala', N'Teatro', N'Bolsillo', N'EDAF', CAST(N'2001-11-05T00:00:00.000' AS DateTime), CAST(8 AS Numeric(10, 0)), CAST(217 AS Numeric(18, 0)), 6.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-414-0894-7', N'Leyendas', N'Gustavo Adolfo Béquer', N'Novela', N'Bolsillo', N'EDAF', CAST(N'2001-05-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(267 AS Numeric(18, 0)), 8.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-415-1922-6', N'Hacker', N'Justo Pérez', N'Ensayo', N'Lujo', N'ANAY', CAST(N'2006-06-06T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(1132 AS Numeric(18, 0)), 54.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-450-7038-4', N'El Silmarillion', N'John Ronald Reuel Tolkien', N'Novela', N'Lujo', N'MINO', CAST(N'1986-10-22T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(494 AS Numeric(18, 0)), 16.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-450-7140-8', N'El Señor de los Anillos: La comunidad del anillo', N'John Ronald Reuel Tolkien', N'Novela', N'Lujo', N'MINO', CAST(N'1996-04-01T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(547 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-450-7176-9', N'El Señor de los Anillos: Las dos torres', N'John Ronald Reuel Tolkien', N'Novela', N'Lujo', N'MINO', CAST(N'1996-04-01T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(461 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-450-7177-7', N'El Señor de los Anillos: El retorno del Rey', N'John Ronald Reuel Tolkien', N'Novela', N'Lujo', N'MINO', CAST(N'1996-04-01T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(414 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-473-0619-4', N'Cien años de soledad', N'Gabriel García Márquez', N'Novela', N'Bolsillo', N'RBA ', CAST(N'1994-06-05T00:00:00.000' AS DateTime), CAST(18 AS Numeric(10, 0)), CAST(315 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-473-3393-0', N'Relato de un náufrago', N'Gabriel García Márquez', N'Novela', N'Bolsillo', N'RBA ', CAST(N'2004-05-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(128 AS Numeric(18, 0)), 7.6400)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-486-0203-X', N'Harrison: principios de la medicina interna. VOL.1', N'VV.AA.', N'Ensayo', N'Lujo', N'MC G', CAST(N'1989-05-03T00:00:00.000' AS DateTime), CAST(24 AS Numeric(10, 0)), CAST(1600 AS Numeric(18, 0)), 119.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-486-0204-8', N'Harrison: principios de la medicina interna. VOL.2', N'VV.AA.', N'Ensayo', N'Lujo', N'MC G', CAST(N'1989-05-03T00:00:00.000' AS DateTime), CAST(24 AS Numeric(10, 0)), CAST(1598 AS Numeric(18, 0)), 119.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-60-56058-6', N'Pedazo de dic. of. y cab. de comp. de Chiquitistán', N'Lucas Grijander', N'Cuentos', N'Bolsillo', N'G.E.', CAST(N'1997-02-02T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(222 AS Numeric(18, 0)), 12.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-613-3001-0', N'El viaje íntimo de la locura', N'Roberto Iniesta', N'Novela', N'Lujo', N'EL H', CAST(N'2009-09-10T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(371 AS Numeric(18, 0)), 18.2700)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-663-0848-2', N'Sefarad', N'Antonio Múñoz Molina', N'Novela', N'Bolsillo', N'SANT', CAST(N'2002-05-05T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(516 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-663-1899-2', N'Momo', N'Michael Endel', N'Novela', N'Bolsillo', N'PUNT', CAST(N'2006-11-03T00:00:00.000' AS DateTime), CAST(18 AS Numeric(10, 0)), CAST(258 AS Numeric(18, 0)), 12.6000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-666-3352-9', N'Coltan', N'Alberto Vázquez Figueroa', N'Novela', N'Lujo', N'EDB ', CAST(N'2008-06-22T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(304 AS Numeric(18, 0)), 17.3100)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-6663-392-5', N'Venganza', N'Brian Freeman', N'Novela', N'Lujo', N'EDB ', CAST(N'2008-03-07T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(400 AS Numeric(18, 0)), 20.6500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-667-5188-2', N'Todos los detectives se llaman Flanagan', N'Andreu Martín', N'Novela', N'Bolsillo', N'ANAY', CAST(N'2006-04-13T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(232 AS Numeric(18, 0)), 9.6200)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-1192-0', N'La dama boba', N'Félix Lope de Vega', N'Teatro', N'Bolsillo', N'ESPA', CAST(N'2003-07-06T00:00:00.000' AS DateTime), CAST(15 AS Numeric(10, 0)), CAST(134 AS Numeric(18, 0)), 8.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-1994-0', N'De todo lo visible y lo invisible', N'Lucía Etxebarría', N'Novela', N'Bolsillo', N'PLAN', CAST(N'2007-08-04T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(541 AS Numeric(18, 0)), 12.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-2178-0', N'Cuatro corazones con freno y marcha atrás', N'Enrique Jardiel Poncela', N'Teatro', N'Bolsillo', N'ESPA', CAST(N'2006-04-02T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(187 AS Numeric(18, 0)), 9.9000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-2232-2', N'La sirena varada. Los árboles mueren de pie', N'Alejandro Casona', N'Teatro', N'Bolsillo', N'ESPA', CAST(N'2007-11-03T00:00:00.000' AS DateTime), CAST(17 AS Numeric(10, 0)), CAST(200 AS Numeric(18, 0)), 7.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-670-2611-5', N'El pensamiento negativo', N'Risto Mejide', N'Ensayo', N'Bolsillo', N'ESPA', CAST(N'2008-03-03T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(192 AS Numeric(18, 0)), 6.6800)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-672-0688-8', N'Un pez gordo', N'Daniel Wallace', N'Novela', N'Lujo', N'CÍRC', CAST(N'2005-07-05T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(168 AS Numeric(18, 0)), 18.7500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-67-53523-5', N'Bel Amor más allá de la muerte', N'Care Santos', N'Novela', N'Lujo', N'S.M.', CAST(N'2009-08-04T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(387 AS Numeric(18, 0)), 17.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-6840-237-1', N'Corina fácil', N'Martín', N'Ensayo', N'Bolsillo', N'ANAY', CAST(N'2004-11-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(123 AS Numeric(18, 0)), 12.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7154-091-1', N'Made in Galiza', N'Séchu Sende', N'Novela', N'Bolsillo', N'GALA', CAST(N'2008-10-01T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(160 AS Numeric(18, 0)), 13.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7213-154-5', N'Porque soy judio', N'Edmond Fleg', N'Ensayo', N'Bolsillo', N'ALFA', CAST(N'2000-03-31T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(92 AS Numeric(18, 0)), 8.8000)
GO
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7461-034-6', N'La divina comedia', N'Dante Alighieri', N'Novela', N'Lujo', N'AUGU', CAST(N'1992-02-05T00:00:00.000' AS DateTime), CAST(5 AS Numeric(10, 0)), CAST(384 AS Numeric(18, 0)), 25.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7640-724-6', N'La tercera palabra', N'Alejandro Casona', N'Teatro', N'Bolsillo', N'EDAF', CAST(N'2001-08-04T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(147 AS Numeric(18, 0)), 5.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7789-332-8', N'Historia del siglo XX', N'Eric Hobs Bawn', N'Narrativa', N'Bolsillo', N'ABAL', CAST(N'1998-10-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(154 AS Numeric(18, 0)), 10.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7824-072-1', N'Os cravos de prata', N'Nicolás Bela', N'Teatro', N'Bolsillo', N'SOTE', CAST(N'1990-01-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(49 AS Numeric(18, 0)), 6.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7833-690-6', N'Pobre cabrón', N'Joe Matt', N'Cuentos', N'Bolsillo', N'LA C', CAST(N'2008-06-12T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(180 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7871-791-9', N'Matemática, ¿estás ahí?', N'Adrián Paenza', N'Ensayo', N'Lujo', N'RBA ', CAST(N'2006-05-01T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(253 AS Numeric(18, 0)), 15.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-7960-198-1', N'El año de la liebre', N'Arto Paosilino', N'Novela', N'Bolsillo', N'LA T', CAST(N'1998-10-30T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(195 AS Numeric(18, 0)), 14.3000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8130-133-7', N'El túnel', N'Ernesto Sábato', N'Novela', N'Bolsillo', N'ALFA', CAST(N'1999-07-20T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(128 AS Numeric(18, 0)), 8.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-81-55685-8', N'Globalización ciudadanía y derechos  h.', N'Maria Jose Fariñas Dulce', N'Ensayo', N'Lujo', N'LIBR', CAST(N'2009-07-11T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(65 AS Numeric(18, 0)), 16.7500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-83-01785-7', N'Dimensiones de la sostenibilidad', N'Ezequiel Usón Guardiola', N'Ensayo', N'Bolsillo', N'EDVI', CAST(N'1996-06-12T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(65 AS Numeric(18, 0)), 11.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8346-667-3', N'La historia de Lisey', N'Stephen King', N'Terror', N'Lujo', N'DE B', CAST(N'2007-04-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(605 AS Numeric(18, 0)), 16.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8346-934-7', N'Te daré la tierra', N'Chufo Llorens', N'Novela', N'Bolsillo', N'NUEV', CAST(N'2009-04-03T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(752 AS Numeric(18, 0)), 11.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-83-63054-0', N'Estr. y tact. en Dir. y Gest. de proy.', N'Luis Jose Amendola', N'Ensayo', N'Lujo', N'ED. ', CAST(N'2007-01-31T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(218 AS Numeric(18, 0)), 21.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8450-037-3', N'La montaña mágica', N'Thomas Mann', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2001-04-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(974 AS Numeric(18, 0)), 14.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8450-205-8', N'El péndulo de Foucault', N'Umberto Eco', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2001-04-04T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(818 AS Numeric(18, 0)), 10.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8450-211-2', N'Siddhartha', N'Hermann Hesse', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2001-04-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(211 AS Numeric(18, 0)), 7.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-84-60796-0', N'Preguntale al economista camuflado', N'Tim Harford', N'Ensayo', N'Lujo', N'TEMA', CAST(N'2009-09-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(300 AS Numeric(18, 0)), 18.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8487-061-8', N'Rinoceronte', N'Eugene Ionesco', N'Teatro', N'Bolsillo', N'EDLA', CAST(N'2004-02-06T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(165 AS Numeric(18, 0)), 10.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8487-065-0', N'Paso de cebra', N'Gustavo Pernas Cora', N'Teatro', N'Bolsillo', N'EDLA', CAST(N'2004-06-02T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(187 AS Numeric(18, 0)), 10.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8487-070-7', N'A estrela da mañá.', N'Michael Löwy', N'Ensayo', N'Bolsillo', N'EDLA', CAST(N'2005-10-03T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(133 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-8773-150-1', N'Dentro de la tierra', N'Paco Bezerra', N'Teatro', N'Bolsillo', N'INST', CAST(N'2007-05-03T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(104 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-87783-10-4', N'Retallos do diario de Adán', N'Mark Twain', N'Cuentos', N'Lujo', N'EDPO', CAST(N'1992-05-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(90 AS Numeric(18, 0)), 20.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-89669-27-9', N'La metamorfósis y otros relatos', N'Franz Kafka', N'Novela', N'Bolsillo', N'ANAY', CAST(N'2002-06-01T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(159 AS Numeric(18, 0)), 6.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9242-933-7', N'Helena de Troya', N'Margaret George', N'Novela', N'Lujo', N'ROCA', CAST(N'2008-06-23T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(784 AS Numeric(18, 0)), 23.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9251-651-3', N'Ángeles y demonios', N'Dan Brown', N'Novela', N'Lujo', N'BOOK', CAST(N'2000-08-02T00:00:00.000' AS DateTime), CAST(6 AS Numeric(10, 0)), CAST(736 AS Numeric(18, 0)), 19.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-933571-5-3', N'León manso come mierda', N'Kutxi Romero', N'Poesia', N'Bolsillo', N'EDDE', CAST(N'2004-05-23T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(86 AS Numeric(18, 0)), 9.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-937-1104-8', N'La metamorfósis y otros relatos', N'Franz Kafka', N'Novela', N'Bolsillo', N'EDCÁ', CAST(N'2008-02-23T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(280 AS Numeric(18, 0)), 12.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-95142-05-8', N'Huesos de sepia', N'Eugenio Montale', N'Poesía', N'Bolsillo', N'EDIG', CAST(N'2000-11-12T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(169 AS Numeric(18, 0)), 11.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-95359-92-8', N'Momentos estelares de la humanidad', N'Stefan Zweig', N'Ensayo', N'Lujo', N'ACAN', CAST(N'2002-10-21T00:00:00.000' AS DateTime), CAST(7 AS Numeric(10, 0)), CAST(306 AS Numeric(18, 0)), 20.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-96259-20-X', N'A unlla da águia', N'Ramón Carredano Cobas', N'Novela', N'Bolsillo', N'TOXO', CAST(N'2004-04-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(60 AS Numeric(18, 0)), 8.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-96390-97-7', N'Don Juan Tenorio', N'José Zorrila', N'Teatro', N'Bolsillo', N'SANT', CAST(N'2004-06-02T00:00:00.000' AS DateTime), CAST(14 AS Numeric(10, 0)), CAST(205 AS Numeric(18, 0)), 9.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-975-6476-6', N'Esperando a Godót', N'Samuel Beckett', N'Teatro', N'Lujo', N'EDSI', CAST(N'2007-04-03T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(208 AS Numeric(18, 0)), 18.5000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9757-233-5', N'Cuando éramos caníbales', N'Carles Lalueza Fox', N'Ensayo', N'Bolsillo', N'LA V', CAST(N'2006-08-05T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(112 AS Numeric(18, 0)), 3.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9759-922-5', N'Fundación y Tierra', N'Isaac Asimov', N'Novela', N'Bolsillo', N'PLAZ', CAST(N'2005-10-27T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(524 AS Numeric(18, 0)), 12.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9793-924-7', N'La torre de la sociedad', N'Valerio Massimo Manfredi', N'Novela', N'Bolsillo', N'DE B', CAST(N'2006-11-11T00:00:00.000' AS DateTime), CAST(4 AS Numeric(10, 0)), CAST(317 AS Numeric(18, 0)), 9.9500)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9838-241-9', N'La maestra de piano', N'Janice Y.K. Lee', N'Novela', N'Lujo', N'SALA', CAST(N'2009-09-19T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(352 AS Numeric(18, 0)), 19.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-98-67601-3', N'Las marismas', N'Arnaldur Indridason', N'Novela', N'Lujo', N'RBA ', CAST(N'2009-02-03T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(288 AS Numeric(18, 0)), 16.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-9872-276-5', N'Vivos y muertos', N'Alberto Vázquez Figueroa', N'Narrativa', N'Bolsillo', N'ZETA', CAST(N'2009-04-03T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(384 AS Numeric(18, 0)), 8.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-98-75042-3', N'Claves del nuevo marketing', N'VV. AA.', N'Ensayo', N'Lujo', N'EDGE', CAST(N'2009-04-04T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(208 AS Numeric(18, 0)), 14.9600)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'84-998-9980-0', N'La caída de los gigantes', N'Ken Follet', N'Novela', N'Lujo', N'DEBO', CAST(N'2010-06-05T00:00:00.000' AS DateTime), CAST(2 AS Numeric(10, 0)), CAST(1024 AS Numeric(18, 0)), 24.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'88-545-0003-1', N'L''arte della guerra', N'Sun Tzu', N'Ensayo', N'Bolsillo', N'NERI', CAST(N'2007-03-03T00:00:00.000' AS DateTime), CAST(3 AS Numeric(10, 0)), CAST(377 AS Numeric(18, 0)), 11.0000)
INSERT [dbo].[tlibros1] ([isbn], [titulo], [autor], [genero], [encuadernacion], [editorial], [fech_ed], [num_ed], [num_pag], [precio]) VALUES (N'987-1138-01-6', N'Crónica de una muerte anunciada', N'Gabriel García Márquez', N'Novela', N'Bolsillo', N'AVE ', CAST(N'1994-07-15T00:00:00.000' AS DateTime), CAST(1 AS Numeric(10, 0)), CAST(144 AS Numeric(18, 0)), 9.8200)
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (1, N'Alava')
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (2, N'Albacete')
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (3, N'Alicante')
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (8, N'Barcelona')
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (15, N'Coruña, A')
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (27, N'Lugo')
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (28, N'Madrid')
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (32, N'Ourense')
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (36, N'Pontevedra')
INSERT [dbo].[tprovincias] ([codigo], [nombre]) VALUES (50, N'Zaragoza')
ALTER TABLE [dbo].[teditoriales]  WITH CHECK ADD FOREIGN KEY([codigo_prov])
REFERENCES [dbo].[tprovincias] ([codigo])
GO
/****** Object:  StoredProcedure [dbo].[consultaRangoFecha]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[consultaRangoFecha] @_fecha1 datetime, @_fecha2 datetime AS
    DECLARE @aux datetime
    IF @_fecha1 > @_fecha2 BEGIN
        SET @aux = @_fecha1
        SET @_fecha1 = @_fecha2
        SET @_fecha2 = @aux
    END
    SELECT titulo, autor, fech_ed
        FROM tlibros
        WHERE fech_ed > @_fecha1 AND fech_ed < @_fecha2
        ORDER BY fech_ed

GO
/****** Object:  StoredProcedure [dbo].[Fechas1]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Fechas1]
@coded varchar(4)
AS
declare @tituloAnt varchar(35)
declare @tituloMod varchar(35)
declare @media money
declare @fecha1 datetime
declare @fecha2 datetime
declare @tabla table (titant varchar(35),titmod varchar(35),media money)
SET @tituloMod = (select top 1 título from tlibros where editorial = @coded order by fech_ed desc)
SET @tituloAnt = (select top 1 título from tlibros where editorial = @coded order by fech_ed)
SET @media = (select avg(precio) from tlibros where editorial = @coded);
insert into @tabla values (@tituloAnt,@tituloMod,@media)
select * from @tabla
GO
/****** Object:  StoredProcedure [dbo].[get_precio]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[get_precio] @isbn char(13), @precio money OUTPUT AS
	-- SET @precio = (SELECT precio from tlibros WHERE @isbn = isbn)
	SELECT @precio = precio from tlibros WHERE @isbn = isbn

GO
/****** Object:  StoredProcedure [dbo].[P_Contar_Letra]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_Contar_Letra]
@cadena varchar(25),@caracter char, @ca int output
as
declare  @cc int
set @cc = 0
while @cc <= len(@cadena)
begin
	if substring(@cadena,@cc,1)=@caracter
		set @ca = @ca + 1
	set @cc = @cc + 1
end
print 'El numero de '+@caracter+' en '+@cadena+'es de: '+str(@ca,6)

GO
/****** Object:  StoredProcedure [dbo].[P_Fechas1]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_Fechas1]
	@coded varchar(4)
as

declare @fecha1 datetime
declare @fecha2 datetime

set @fecha1 = (select max(fech_ed) from tlibros where editorial = @coded)
set @fecha2 = (select min(fech_ed) from tlibros where editorial = @coded)
select @fecha1,@fecha2 
exec p_listadoFechas @fecha1,@fecha2
GO
/****** Object:  StoredProcedure [dbo].[P_ISBN_Editorial_01]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[P_ISBN_Editorial_01]
	@isbn char(13)
as

	declare @editorial char(4)
	select @editorial = editorial from tlibros 
		where isbn=@isbn
	exec P_Listado_Editorial @editorial


print 'FIN'
GO
/****** Object:  StoredProcedure [dbo].[P_Letra_En_Titulo]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_Letra_En_Titulo]
 @letra char
as
declare Leer_Título cursor
	keyset
	local
	for select título from tlibros
declare @título char(50),@conlet int, @sumlet int,@totlet int
set @totlet=0
open Leer_Título
fetch Leer_Título into @título
while @@fetch_status = 0
begin
	set @conlet=1
	set @sumlet=0
	while @conlet<=len(@título)
	begin
		if substring(@título,@conlet,1)=@letra
			set @sumlet=@sumlet+1
		set @conlet=@conlet+1
	end
	print @título+' '+str(@sumlet,4)
	set @totlet=@totlet+@sumlet
	fetch Leer_Título into @título
end
print' '
print 'Total de '+@letra+' en el listado = '+str(@totlet,5)
close Leer_Título
deallocate Leer_Título
GO
/****** Object:  StoredProcedure [dbo].[P_Letra_En_Titulo_T]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_Letra_En_Titulo_T]
 @letra char
as
declare Leer_Título cursor
	keyset
	local
	for select título from tlibros
declare @título char(50),@conlet int, @sumlet int,@totlet int
declare @tabla table (tít varchar(50),n_let int)
set @totlet=0
open Leer_Título
fetch Leer_Título into @título
while @@fetch_status = 0
begin
	set @conlet=1
	set @sumlet=0
	while @conlet<=len(@título)
	begin
		if substring(@título,@conlet,1)=@letra
			set @sumlet=@sumlet+1
		set @conlet=@conlet+1
	end
	if @sumlet <> 0
		insert @tabla values(@título,@sumlet)
	print @título+' '+str(@sumlet,4)
	set @totlet=@totlet+@sumlet
	fetch Leer_Título into @título
end
insert @tabla values('Total caracter '+@letra,@totlet)
select * from @tabla
close Leer_Título
deallocate Leer_Título
GO
/****** Object:  StoredProcedure [dbo].[P_List_Entre_precios]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_List_Entre_precios]
@precio1 money, @precio2 money
as
declare @pr_aux money
if @precio1 > @precio2
begin
	set @pr_aux = @precio1
	set @precio1 = @precio2
	set @precio2 = @pr_aux
end
set @pr_aux = (@precio1+@precio2)/2

select título,autor,precio,case when precio >= @pr_aux then 'Alto' else 'Bajo' end as tipo
	from tlibros where precio between @precio1 and @precio2
print 'Precio medio = '+str(@pr_aux,6,2)
GO
/****** Object:  StoredProcedure [dbo].[P_Listado_Editorial]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[P_Listado_Editorial]
	@editorial char(4)
as
	select isbn, título, autor,nombre  from tlibros, teditoriales 
		where editorial = código and editorial = @editorial
GO
/****** Object:  StoredProcedure [dbo].[P_Listado_Entre_Fechas]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_Listado_Entre_Fechas]
@fecha1 datetime, @fecha2 datetime
AS

select título,autor,fech_Ed from  tlibros 
	where fech_ed between @fecha1 and @fecha2
GO
/****** Object:  StoredProcedure [dbo].[P_Listado_entre_posiciones]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_Listado_entre_posiciones]
 @posicion1 int, @posicion2 int, @intervalo int
as
declare Leer_Libro cursor
	keyset
	local
	for select título,autor from tlibros
declare @título char(50),@autor char(30)
open Leer_Libro
if @posicion1 > @posicion2
	set @intervalo = -@intervalo
fetch absolute @posicion1 from Leer_Libro into @título,@autor
while @@fetch_status = 0 and (@posicion1 <= @posicion2 and @intervalo >0 or  @posicion1 >= @posicion2 and @intervalo <0) 
begin
	print @título+' '+@autor + str(@posicion1,6)+ str(@posicion2,6)+str(@intervalo,6)
	set @posicion1 = @posicion1 + @intervalo
	fetch relative @intervalo from Leer_Libro into @título,@autor
end

close Leer_Libro
deallocate Leer_Libro
GO
/****** Object:  StoredProcedure [dbo].[P_ListadoEntrePrecios1]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_ListadoEntrePrecios1]
@precio1 money, @precio2 money
as
select título,precio from tlibros where precio between @precio1 and @precio2
GO
/****** Object:  StoredProcedure [dbo].[P_ListadoFechas]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure  [dbo].[P_ListadoFechas]
	@f1 datetime,@f2 datetime
as
declare @faux datetime
if @f1>@f2
begin
	set @faux = @f1
	set @f1 = @f2
	set @f2 = @faux
end
select título, autor, fech_ed from tlibros where fech_ed between @f1 and @f2
		order by fech_ed
GO
/****** Object:  StoredProcedure [dbo].[P_Mayúsculas_minúsculas]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_Mayúsculas_minúsculas]
as
declare MayMin cursor
	keyset
	read_only
	for select título from tlibros
declare @título varchar(50),@pc int,@sw int,@titmm varchar(50)
declare @tabtit table (tit varchar(50))
open MayMin 
	fetch first from MayMin into @título
	while @@fetch_status = 0
	begin
		set @sw=0
		set @pc=1
		set @titmm = ''
		while @pc <= len(@título)
		begin
			if @sw=0
			begin
				set @titmm = @titmm + upper(substring(@título,@pc,1))
				set @sw = 1
			end
			else
			begin
				set @titmm = @titmm + lower(substring(@título,@pc,1))
				set @sw = 0
			end
			set @pc = @pc + 1
		end
		insert into @tabtit values (@titmm)
		
		fetch MayMin into @título
	end
	select * from @tabtit
close MayMin
deallocate MayMin		


GO
/****** Object:  StoredProcedure [dbo].[P_Numero_En_Mes]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_Numero_En_Mes]
@mes int,@numero int output
AS

select @numero = count(*) from  tlibros where month(fech_ed) = @mes
GO
/****** Object:  StoredProcedure [dbo].[P_Precio_Max]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[P_Precio_Max]
@precio_max money output
AS

select @precio_max = max(precio) from  tlibros
GO
/****** Object:  StoredProcedure [dbo].[p1]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p1] @isbn char(13), @precio money OUTPUT AS
	SELECT @precio = precio from tlibros
	WHERE @isbn = ISBN

GO
/****** Object:  StoredProcedure [dbo].[p2]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p2] AS
	IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tlibros1' AND type = 'U')
		DROP TABLE tlibros1
	SELECT * INTO tlibros1 FROM tlibros

GO
/****** Object:  StoredProcedure [dbo].[PAEncuaderna1]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PAEncuaderna1]
@título varchar(35)
AS
declare @encuadernación varchar(15)
set @encuadernación = (select encuadernación from tlibros where título = @título)
exec PAEncuaderna2 @encuadernación

GO
/****** Object:  StoredProcedure [dbo].[PAEncuaderna2]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PAEncuaderna2]
@encuadernación varchar(15)
as
select título,autor,encuadernación from tlibros where encuadernación = @encuadernación

GO
/****** Object:  StoredProcedure [dbo].[PAFechas1]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PAFechas1]
@coded varchar(4)
AS
declare @tituloAnt varchar(50)
declare @tituloMod varchar(50)
declare @media money
--declare @fecha1 datetime
--declare @fecha2 datetime
declare @tabla table (titant varchar(35),titmod varchar(35),media money)
SET @tituloMod = (select top 1 título from tlibros where editorial = @coded order by fech_ed desc)
SET @tituloAnt = (select top 1 título from tlibros where editorial = @coded order by fech_ed)
SET @media = (select avg(precio) from tlibros where editorial = @coded);
insert into @tabla values (@tituloAnt,@tituloMod,@media)
select * from @tabla
exec PAFechas2 @coded
GO
/****** Object:  StoredProcedure [dbo].[PAFechas2]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PAFechas2]
@coded varchar(4)
AS
declare @tituloBara varchar(50)
declare @tituloCaro varchar(50)
declare @fecha1 datetime
declare @fecha2 datetime
select @tituloBara = título from tlibros where @coded = editorial and precio = (select min(precio) from tlibros where @coded=editorial)
select @tituloCaro = título from tlibros where @coded = editorial and precio = (select max(precio) from tlibros where @coded=editorial)

select @fecha1 = fech_ed from tlibros where título = @tituloCaro
select @fecha2 = fech_ed from tlibros where título = @tituloBara
print 'Título del libro más caro: '+@tituloCaro+' editado el: '+convert(varchar(12),@fecha1,106)
print 'Título del libro más barato: '+@tituloBara+' editado el: '+convert(varchar(12),@fecha2,106)

if @fecha1<@fecha2
begin
	exec PAFechas3  @fecha1,@fecha2
end
else
begin
	exec PAFechas3  @fecha2,@fecha1
end
GO
/****** Object:  StoredProcedure [dbo].[PAFechas3]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PAFechas3]
@fecha1 datetime,
@fecha2 datetime
AS
select título, autor, fech_ed,precio, case  
				   when precio>15 then 'CARO'
				   else 'BARATO'
				   end Valor
from tlibros where fech_ed between @fecha1 and @fecha2
		order by fech_ed
GO
/****** Object:  StoredProcedure [dbo].[PALinFactura]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PALinFactura]
@isbn varchar(13),
@numlib integer
as
declare @titulo varchar(35)
declare @precio money
declare @pretotal money
declare @descuento money
declare @impdesc money
declare @impiva money
declare @prefinal money
if exists (select isbn from tlibros where isbn = @isbn)
begin
declare @linfac table(isbn varchar(13),título varchar(35),precio money,unidades integer,
		prectotal money,dto money,impdesc money,impiva money, impfinal money)
set @descuento = 0
select @isbn = isbn, @titulo = título, @precio = precio from tlibros where isbn = @isbn
set @pretotal =  @precio * @numlib
if @pretotal >=50
	set @descuento = 0.02
else
	if @pretotal >= 25
		set @descuento = 0.01
set @impdesc = @pretotal * @descuento
set @impiva = (@pretotal - @impdesc)*0.16
set @prefinal = @pretotal - @impdesc + @impiva
insert into @linfac values (@isbn,@titulo,@precio,@numlib,@pretotal,@descuento,@impdesc,@impiva,@prefinal)
select * from @linfac
end
else
	print 'El libro con isbn: ' + @isbn + ' no está en la tabla'
GO
/****** Object:  StoredProcedure [dbo].[PAprecios]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PAprecios]
@coded varchar(4)
AS

declare @tituloCaro varchar(35)
declare @tituloBara varchar(35)
declare @media money
declare @tabla table (titcar varchar(35),titbar varchar(35),media money)
set @tituloCaro = (select top 1 título from tlibros 
			where precio = (select max(precio) from tlibros where editorial = @coded)
			      and editorial = @coded)
set @tituloBara = (select top 1 título from tlibros 
			where precio = (select min(precio) from tlibros where editorial = @coded)
			      and editorial = @coded)
set @media = (select avg(precio) from tlibros where editorial = @coded)

insert into @tabla values(@tituloCaro,@tituloBara,@media)
select * from @tabla 
GO
/****** Object:  StoredProcedure [dbo].[PEncuadernaPrecio]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PEncuadernaPrecio]
	@titulo varchar(35), @precio money
as
select título,encuadernación,precio from tlibros 
	where precio > @precio and encuadernación = (select encuadernación from tlibros
							where título=@titulo)
GO
/****** Object:  StoredProcedure [dbo].[pfecha]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pfecha] @_fecha date AS
	SELECT titulo, autor, fech_ed
        FROM tlibros
        WHERE fech_ed > @_fecha
        ORDER BY fech_ed

GO
/****** Object:  StoredProcedure [dbo].[pfecha_from_isbn]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pfecha_from_isbn] @_isbn char(13) AS
	DECLARE @fech_ed datetime
	SET @fech_ed = (SELECT fech_ed FROM tlibros WHERE isbn = @_isbn)
	EXEC pfecha @fech_ed

GO
/****** Object:  StoredProcedure [dbo].[PR_Listado_Lib_Ed]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PR_Listado_Lib_Ed]
as
	select título,autor,nombre from tlibros,teditoriales 
		where editorial =  código and editorial = 'PLJA'
GO
/****** Object:  StoredProcedure [dbo].[PR_Listado_Lib_Ed_Ext]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[PR_Listado_Lib_Ed_Ext]
	@editorial char (4)
as
	select título,autor,nombre from tlibros,teditoriales 
		where editorial =  código and editorial = @editorial
GO
/****** Object:  StoredProcedure [dbo].[Procedimiento1]    Script Date: 13/06/2020 11:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Procedimiento1] @num int
AS
  DECLARE tituloVocales CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

  DECLARE
    @titulo char (50),
    @cont int,
	@pos int

  SET @titulo = ''
  SET @cont = 0
  SET @pos = 1

  OPEN tituloVocales
    FETCH FIRST FROM tituloVocales INTO @titulo
    WHILE @@fetch_status = 0
    BEGIN
		SET @cont = 0
		set @pos = 1
        WHILE @pos <= len(@titulo) 
		BEGIN
			IF substring(@titulo,@pos,1) like '[aeiouáéíóúü]'
				SET @cont = @cont +1
		    SET @pos = @pos +1
        END
		IF @cont >= @num
			PRINT @titulo + ': ' + str(@cont,3,0)
		FETCH NEXT FROM tituloVocales INTO @titulo
	END
  CLOSE tituloVocales
  DEALLOCATE tituloVocales
GO
USE [master]
GO
ALTER DATABASE [BDBiblioteca] SET  READ_WRITE 
GO
