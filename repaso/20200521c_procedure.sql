USE BD_Alumnos
GO

/*
USE BD_Alumnos
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p_backup' AND type = 'P')
    DROP PROCEDURE p_backup
GO

CREATE PROCEDURE p_backup AS
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'talumnos1' AND type = 'U')
        DROP TABLE talumnos1
    SELECT * INTO talumnos1 FROM talumnos
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tnotas1' AND type = 'U')
        DROP TABLE tnotas1
    SELECT * INTO tnotas1 FROM tnotas
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tciclos1' AND type = 'U')
        DROP TABLE tciclos1
    SELECT * INTO tciclos1 FROM tciclos
GO

EXEC p_backup
GO
*/

-- procedimiento que muestre en la cuadricula la media de las tres notas del dni que le mandas

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'procedure1' AND type = 'P')
        DROP PROCEDURE procedure1
GO

CREATE PROCEDURE procedure1 @dni char(9) AS
    SELECT (nota1 + nota2 + nota3) + 3 FROM tnotas WHERE dni = @dni
GO

EXEC procedure1 '77777777J'
GO

/*
SELECT * FROM tnotas1
SELECT * FROM thistorico
*/