-- TRIGERS

USE bdmuebles
GO

SELECT * FROM tmuebles1
GO

/*
USE bdmuebles
SELECT * INTO tmuebles1 FROM tmuebles
GO
*/

-- Crear un trigger
IF EXISTS (SELECT name FROM sysobjects WHERE name = 't1' AND type = 'TR')
    DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tmuebles1
    AFTER UPDATE -- || INSTEAD OF
    AS
    	SELECT * FROM INSERTED
    	UNION
    	SELECT * FROM DELETED
GO

UPDATE tmuebles1
    SET stock_actual = 12
    WHERE codigo = 'ERTY23'
GO

SELECT * FROM tmuebles1
GO


-- MOLDE
/*IF EXISTS (SELECT name FROM sysobjects WHERE name = 't1' AND type = 'TR')
	DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tmuebles1
    AFTER UPDATE -- || INSTEAD OF
    AS
		-- CODIGO AQUÍ
GO*/


















