-- # Repaso SQL Alfredo RG 20191213a
select titulo + space(4) + precio -- error de tipos
    from tlibros

select titulo + space(4) + str(precio, 8, 2) -- it works
    from tlibros
