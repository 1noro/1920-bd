USE BD_Alumnos
GO

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

declare cc cursor
keyset local
for select a.dni, a.nombre, ape1, ape2, (nota1+nota2+nota3)/3, c.nombre from tnotas n, talumnos a, tciclos c
    where a.dni = n.dni and n.ciclo = c.ciclo
    order by c.ciclo

declare @dni char(9)
declare @nombrealum varchar(15)
declare @ape1 varchar(15)
declare @ape2 varchar(15)
declare @media numeric(5,2) 
declare @countal int
declare @countc int
declare @ciclo varchar(28)
declare @cicloant varchar(28)
declare @mediac numeric(5,2)
declare @mediac2 numeric(5,2)

set @mediac = 0
set @mediac2 = 0
set @countal = 0
set @countc = 0

open cc
    fetch first from cc into @dni, @nombrealum, @ape1, @ape2, @media, @ciclo
    set @cicloant = @ciclo
    while @@fetch_status = 0
    begin
        if @cicloant != @ciclo 
        begin
	        print ' '
            print '-'+@cicloant+':  '+str(@mediac2/@countal,5,2)
            print ' '
            set @countal= 0
            set @mediac2 = 0
            set @cicloant = @ciclo
            set @mediac = @mediac + @mediac2
            set @countc = @countc + 1
        end
        print @dni+' '+@nombrealum+' '+@ape1+'  '+@ape2+'  '+str(@media,5,2)
        set @mediac2 = @mediac2 + @media
        set @countal = @countal + 1
        fetch next from cc into @dni, @nombrealum, @ape1, @ape2, @media, @ciclo
    end

set @mediac = @mediac + @mediac2
set @countc = @countc + 1
print ' '
print '-'+@cicloant+':  '+str(@mediac2/@countal,5,2)
print 'Media total: '+str(@mediac/@countc,5,2)
print 'Ciclos: '+str(@countc,5,0)

close cc
deallocate cc