-- # Apuntes SQL Alfredo RG 20200109a
USE BD_biblioteca

-- #cursor titulo y numero de vocales (acentos y dieresis)
DECLARE cursorPrueba CURSOR
    KEYSET
    LOCAL
    FOR SELECT titulo
        FROM tlibros

DECLARE @titulo varchar(50), @vocales int

set @vocales = 0

OPEN cursorPrueba
    FETCH ABSOLUTE 1 FROM cursorPrueba INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            set @vocales = len(@titulo) - len(replace(replace(replace(replace(replace(lower(@titulo), 'a', ''), 'e', ''), 'i', ''), 'o', ''), 'u', ''))
            PRINT @titulo + ' (' + ltrim(str(@vocales, 5, 0)) + ' vocales)'
            FETCH NEXT FROM cursorPrueba INTO @titulo
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria

-- #cursor titulo y numero de vocales (acentos y dieresis)
DECLARE cursorPrueba CURSOR
    KEYSET
    LOCAL
    FOR SELECT titulo
        FROM tlibros

--DECLARE @titulo varchar(50), @vocales int, @i int, @c char
DECLARE @titulo char(50), @vocales int, @i int, @c char

SET @vocales = 0
SET @i = 0

OPEN cursorPrueba
    FETCH ABSOLUTE 1 FROM cursorPrueba INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            SET @vocales = 0
            SET @i = 0
            WHILE @i <= len(@titulo)
                BEGIN
                    SET @c = substring(@titulo, @i, 1)
                    IF @c LIKE '[aeiouáéíóúü]' SET @vocales = @vocales + 1
                    SET @i = @i + 1
                END
            PRINT @titulo + ' (' + ltrim(str(@vocales, 5, 0)) + ' vocales)'
            FETCH NEXT FROM cursorPrueba INTO @titulo
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria
