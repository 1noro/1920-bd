USE BDBiblioteca
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            PRINT @titulo
            FETCH NEXT FROM c1 INTO @titulo
        END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

/*
 * Crear un CURSOR que visualice en la pestaña de "mensajes" el título, el precio y el nombre de la editorial a la 
 * que pertenece, de los libros con el precio mas alto y mas bajo. 
 */

PRINT '------------------------------------------------------------------'

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, precio, editorial FROM tlibros

DECLARE @titulo char(50)
DECLARE @precio money
DECLARE @cod_ed char(4)
DECLARE @n_ed char(35)

DECLARE @titulo_max char(50)
DECLARE @precio_max money
DECLARE @n_ed_max char(35)

DECLARE @titulo_min char(50)
DECLARE @precio_min money
DECLARE @n_ed_min char(35)

SET @precio_max = 0
SET @precio_min = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo, @precio, @cod_ed
    SET @precio_min = @precio
    WHILE @@fetch_status = 0
        BEGIN
            IF @precio > @precio_max BEGIN
	            SET @titulo_max = @titulo
	            SET @precio_max = @precio
	            SET @n_ed_max = (SELECT nombre FROM teditoriales WHERE codigo =  @cod_ed)
            END
            IF @precio < @precio_min BEGIN
	            SET @titulo_min = @titulo
	            SET @precio_min = @precio
	            SET @n_ed_min = (SELECT nombre FROM teditoriales WHERE codigo =  @cod_ed)
            END
            FETCH NEXT FROM c1 INTO @titulo, @precio, @cod_ed
        END
CLOSE c1
DEALLOCATE c1

PRINT 'MAX: ' + @titulo_max + ' ' + str(@precio_max, 7, 2) + ' ' + @n_ed_max
PRINT 'MIN: ' + @titulo_min + ' ' + str(@precio_min, 7, 2) + ' ' + @n_ed_min

/*
SELECT titulo FROM tlibros WHERE precio = (SELECT MAX(precio) FROM tlibros)
GO
SELECT titulo FROM tlibros WHERE precio = (SELECT MIN(precio) FROM tlibros)
GO
*/



