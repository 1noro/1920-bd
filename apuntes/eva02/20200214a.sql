-- # Apuntes SQL Alfredo RG 20200214a
USE BD_biblioteca

-- #EX: Proc <-- codigo_ed y visualiza en cuadriculas isbn, titulo, autor y nombre ed de todos los libros de esa editorial

-- SELECT * FROM teditoriales

/*
SELECT l.isbn, l.titulo, l.autor, e.nombre
    FROM tlibros AS l, teditoriales AS e
    WHERE l.editorial = e.codigo AND e.codigo = 'PLAZ'
*/

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'consultaCodigoEd' AND type = 'P')
        DROP PROCEDURE consultaCodigoEd
GO

CREATE PROCEDURE consultaCodigoEd @codigo char(4)
AS
    SELECT l.isbn, l.titulo, l.autor, e.nombre
        FROM tlibros AS l, teditoriales AS e
        WHERE l.editorial = e.codigo AND e.codigo = @codigo
GO

/*EXEC consultaCodigoEd 'PLAZ'
GO*/


-- #EX: proc <-- isbn y averiguar la editorial y llamar al proc anterior
-- SELECT * FROM tlibros
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'consultaCodigoEd' AND type = 'P')
        DROP PROCEDURE consultaCodigoEdFromISBN
GO

CREATE PROCEDURE consultaCodigoEdFromISBN @_isbn char(13)
AS
    DECLARE @codigo char(4)
    SELECT @codigo = editorial
        FROM tlibros
        WHERE isbn = @_isbn
    EXEC consultaCodigoEd @codigo
GO

EXEC consultaCodigoEdFromISBN '84-013-4219-6'
GO
