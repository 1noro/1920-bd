-- # Apuntes SQL Alfredo RG 20191108a
USE BD_biblioteca

-- #tabla basica -------------------------------------------------
CREATE TABLE prueba (
    numero int,
    nombre varchar(15),
    edad int
)

INSERT INTO prueba (numero, nombre, edad)
    VALUES (5, 'Juan', 23)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (3, 'Luis', 27)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (7, 'Sara', 20)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (2, 'Lara', 30)

-- salen ordenados por como estan guardados internamente
SELECT * FROM prueba

DROP TABLE prueba

-- #con primary key -------------------------------------------------
CREATE TABLE prueba (
    numero int primary key,
    nombre varchar(15),
    edad int
)

INSERT INTO prueba (numero, nombre, edad)
    VALUES (5, 'Juan', 23)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (3, 'Luis', 27)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (7, 'Sara', 20)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (2, 'Lara', 30)

INSERT INTO prueba (numero, edad) -- -> nulo en nombre
    VALUES (1, 40)

-- salen ordenadas por la primary key
SELECT * FROM prueba

DROP TABLE prueba

-- #con primary key y not null -------------------------------------------------
CREATE TABLE prueba (
    numero int primary key,
    nombre varchar(15) not null,
    edad int
)

INSERT INTO prueba (numero, nombre, edad)
    VALUES (5, 'Juan', 23)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (3, 'Luis', 27)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (7, 'Sara', 20)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (2, 'Lara', 30)

/*INSERT INTO prueba (numero, edad) -- -> da error, porque el nombre no puede ser nulo
    VALUES (1, 40)*/
INSERT INTO prueba (numero, nombre, edad)
    VALUES (1, 'José', 40)

-- salen ordenadas por la primary key
SELECT * FROM prueba

DROP TABLE prueba

-- #con primary key, not null y default -------------------------------------------------
CREATE TABLE prueba (
    numero int primary key,
    nombre varchar(15) not null default 'Desconocido',
    edad int
)

INSERT INTO prueba (numero, nombre, edad)
    VALUES (5, 'Juan', 23)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (3, 'Luis', 27)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (7, 'Sara', 20)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (2, 'Lara', 30)

INSERT INTO prueba (numero, nombre, edad)
    VALUES (1, 'José', 40)
INSERT INTO prueba (numero, edad) -- -> 'Desconocido' en nombre
    VALUES (9, 40)

-- salen ordenadas por la primary key
SELECT * FROM prueba

DROP TABLE prueba

-- #con resticción general -------------------------------------------------
CREATE TABLE prueba (
    numero int primary key,
    nombre varchar(15) not null default 'Desconocido',
    edad int check (edad between 18 and 65)
)

INSERT INTO prueba (numero, nombre, edad)
    VALUES (5, 'Juan', 23)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (3, 'Luis', 27)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (7, 'Sara', 20)
INSERT INTO prueba (numero, nombre, edad)
    VALUES (2, 'Lara', 30)

INSERT INTO prueba (numero, nombre, edad)
    VALUES (1, 'José', 40)
INSERT INTO prueba (numero, edad)
    VALUES (9, 40)
    
INSERT INTO prueba (numero, edad) -- -> Error por la restricción general
    VALUES (8, 66)

-- salen ordenadas por la primary key
SELECT * FROM prueba

DROP TABLE prueba



