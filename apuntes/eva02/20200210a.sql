-- # Apuntes SQL Alfredo RG 20200210a
USE BD_biblioteca

-- #proc << int --> visualiza todos los libros que tengan los titulos con igual o mas vocales que envio en el int y el numero de vocales

/*SELECT titulo, len(titulo)
    FROM tlibros
    WHERE len(titulo) >= 12*/

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'p1' AND type = 'P')
        DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @nvocales int
AS
    -- CURSOR
    DECLARE c1 CURSOR
        KEYSET LOCAL
        FOR SELECT titulo
            FROM tlibros

    -- DECLARACIÓN DE VARIABLES
    DECLARE
        @tit char(50),
        @i int,
        @vcont int

    -- VALORES INICIALES
    SET @i = 1
    SET @vcont = 0

    -- MECANICA DEL CURSOR
    OPEN c1
        FETCH FIRST FROM c1 INTO @tit
        WHILE @@fetch_status = 0 BEGIN
            SET @i = 1
            SET @vcont = 0
            WHILE @i <= len(@tit) BEGIN
                IF substring(@tit, @i, 1) LIKE '[aeiouáéíóú]'
                    SET @vcont = @vcont + 1
                SET @i = @i + 1
            END
            IF @vcont >= @nvocales
                PRINT '' + @tit + '  ' + ltrim(str(@vcont, 7, 0)) 
            FETCH NEXT FROM c1 INTO @tit
        END
    CLOSE c1
    DEALLOCATE c1 -- liberamos la memoria
GO

EXEC p1 13
GO
