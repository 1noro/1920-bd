USE bdalumnos
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            PRINT @titulo
            FETCH NEXT FROM c1 INTO @titulo
        END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

PRINT '------------------------------------------------------------------'
/*
 * Crear un cursor que visualice en la pestaña de “mensajes” la nota media más 
 * alta de los alumnos, sin importar sexo, ciclo ni curso, matriculados en el 
 * centro. También visualizará el sexo (figurará “Varón” o “Mujer”), que haya 
 * obtenido la nota media más alta.
 */

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT a.nombre, a.sexo, (n.nota1 + n.nota2 + n.nota3) / 3
    	FROM talumnos a, tnotas n
    	WHERE a.dni = n.dni

DECLARE @nombre char(15)
DECLARE @sexo   char(1)
DECLARE @media  numeric(6, 2)

DECLARE @nombre_max char(15)
DECLARE @sexo_max   char(1)
DECLARE @media_max  numeric(6, 2)

SET @media_max = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @nombre, @sexo, @media
    WHILE @@fetch_status = 0
        BEGIN
            IF @media > @media_max BEGIN
	            SET @nombre_max = @nombre
	            SET @sexo_max = @sexo
	            SET @media_max = @media
            END
            FETCH NEXT FROM c1 INTO @nombre, @sexo, @media
        END
CLOSE c1
DEALLOCATE c1

PRINT 'MAX: ' + @nombre_max + ' ' + @sexo_max + ' ' + str(@media_max, 7, 2)

/*
SELECT * FROM tnotas
SELECT a.nombre, a.sexo, (n.nota1 + n.nota2 + n.nota3) / 3
    	FROM talumnos a, tnotas n
    	WHERE a.dni = n.dni
* */

declare c4 cursor
keyset local
for select a.nombre, a.sexo, (n.nota1 + n.nota2 + n.nota3)/3 from talumnos a, tnotas n where a.dni = n.dni

declare @name varchar(15)
declare @sexo char(1)
declare @media numeric(6,2)
declare @mediaMax numeric(6,2)

set @media = 0
set @mediaMax = 0

open c4
fetch next from c4 into @name, @sexo, @media
while @@fetch_status = 0
begin

    if @media > @mediaMax 
        set @mediaMax = @media
    fetch next from c4 into @name, @sexo, @media

end

print ' '
print 'A nota media mais alta e: '+ str(@mediaMax, 7, 2) +' '+@name

close c4
deallocate c4

