# Apuntes Alfredo RG 20191010a

# Configuración servidor inicial:
Asistente > ... > Agregamos (local) > ... > Finalizar

# Cread BD
Raiz > Servidores Microsoft SQL Server > Grupo de SQL Server > 
    (local) (Windows NT) > Bases de datos > (click der) > 
    Nueva Base de Datos...

Nombre: BDPrueba > Aceptar

# Buscar los archivos de datos
(click der sobre la DB) > Propiedades > Archivos de datos

# Editar una nueva consulta
Desde el administrador coorporativo (SQL Server Enterprise Manager)
Herramientas > Analizador de consultas SQL 

F5 para ejecutar el script SQL.

# Consulta SQL para crear una nueva tabla
CREATE TABLE t_personas(
    dni char(9),
    nombre varchar(15),
    edad int
)

# Revisar estructura de una tabla
(clic der en una tabla) > Diseño tabla

# Establecer clave primaria de forma gráfica
(clic der en una tabla) > Diseño tabla
Nos situamosa sobre el atributo desado y pulsamos el botón de la 
llave (en la barra de herramientas).
NOTA: Las claves primarias (PK) no pueden ser NULL.

# Introducir datos graficamente
(clic der en una tabla) > Abrir tabla > Devolver todas las filas

# Comentarios en Transact SQL
-- linea comentada

texto ejecutable -- texto comentado en linea

/* múltiples
múltiples
múltiples lineas
comentadas*/








