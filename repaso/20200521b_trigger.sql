USE BD_Alumnos
GO

/*
USE BD_Alumnos
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p_backup' AND type = 'P')
    DROP PROCEDURE p_backup
GO

CREATE PROCEDURE p_backup AS
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'talumnos1' AND type = 'U')
        DROP TABLE talumnos1
    SELECT * INTO talumnos1 FROM talumnos
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tnotas1' AND type = 'U')
        DROP TABLE tnotas1
    SELECT * INTO tnotas1 FROM tnotas
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tciclos1' AND type = 'U')
        DROP TABLE tciclos1
    SELECT * INTO tciclos1 FROM tciclos
GO

EXEC p_backup
GO
*/

-- Crear un desencadenador que guarde una copia de seguridad cada vez que se
-- haga una modificación en algún campo de la tabla de tnotas, teniendo en 
-- cuenta que figurarán todos los campos de la misma (después de la modificación)
-- además del campo de ‘fech_mod’ que será la fecha correspondiente al día que se
-- produce dicha modificación. Si existiese un desencadenador previo con dicho
-- nombre, se borrará. (La tabla en la que se guardarán las copias de seguridad se
-- llamará thistorico y no tendrá ningún campo clave ni ningún tipo de restricción).

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'trigger1' AND type = 'TR')
    DROP TRIGGER trigger1
GO

CREATE TRIGGER trigger1
    ON tnotas1
    AFTER UPDATE
    AS BEGIN
        IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = 'thistorico' AND type = 'U') BEGIN
            CREATE TABLE thistorico (
                ciclo char(3),
                curso int,
                numero int,
                dni char(9),
                nota1 numeric(5, 2),
                nota2 numeric(5, 2),
                nota3 numeric(5, 2),
                fech_mod datetime
            )
        END
        INSERT INTO thistorico SELECT *, getdate() FROM DELETED
    END
GO

UPDATE tnotas1
    SET nota2 = 3.50
    WHERE dni = '77777777J'
GO

/*
SELECT * FROM tnotas1
SELECT * FROM thistorico
*/