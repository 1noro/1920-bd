-- # Apuntes SQL Alfredo RG 20191121a
USE BD_biblioteca

-- VISTAS
-- una vista es una tabla virtual
-- CREAR UNA VISTA
CREATE VIEW vista1
    AS SELECT titulo, autor, editorial, precio 
            FROM tlibros

SELECT * FROM vista1
-- SELECT isbn, titulo FROM viasta1 -- -> error, isbn no esta en la vista

DROP VIEW vista1

CREATE VIEW vista1
    AS SELECT titulo, autor, editorial, precio 
            FROM tlibros
            WHERE precio > 25

SELECT * FROM vista1

-- * UTILIDAD (A PARTE)
SELECT * FROM sysobjects

SELECT * 
    FROM sysobjects 
    WHERE type = 'V'

SELECT * 
    FROM sysobjects 
    WHERE type = 'V' AND name = 'vista1'

IF EXISTS (SELECT *  FROM sysobjects WHERE type = 'V' AND name = 'vista1')
    PRINT 'Existe'
ELSE
    PRINT 'No existe'

IF EXISTS (SELECT *  FROM sysobjects WHERE type = 'V' AND name = 'vista1')
    SELECT 'Existe' mensaje
ELSE
    SELECT 'No existe' mensaje

-- CREACIÓN CONDICIONAL DE UNA VISTA
IF EXISTS (SELECT *  FROM sysobjects WHERE type = 'V' AND name = 'vista1')
    DROP VIEW vista1
GO

CREATE VIEW vista1
    AS SELECT titulo, autor, editorial, precio 
            FROM tlibros
            WHERE precio > 25
GO

-- ALTERNATIVA MAS CORTA
IF OBJECT_ID('vista1') IS NOT NULL DROP VIEW vista1
GO

CREATE VIEW vista1
    AS SELECT titulo, autor, editorial, precio 
            FROM tlibros
            WHERE precio > 25
GO

-- EX: Crea una vista que muestre titulo, autor, nombre ed, nombre provincia, precio
SELECT l.titulo, l.autor
    FROM tlibros as l

SELECT l.titulo, l.autor, e.nombre
    FROM tlibros as l, teditoriales as e
    WHERE l.editorial = e.codigo

SELECT l.titulo, l.autor, e.nombre, p.nombre, l.precio
    FROM tlibros as l, teditoriales as e, tprovincias as p
    WHERE l.editorial = e.codigo AND e.codigo_prov = p.codigo

-- creamos la vista
IF OBJECT_ID('vista2') IS NOT NULL DROP VIEW vista2
GO
CREATE VIEW vista2
    AS SELECT l.titulo, l.autor, e.nombre 'nombre_ed', p.nombre 'nombre_pro', l.precio
        FROM tlibros as l, teditoriales as e, tprovincias as p
        WHERE l.editorial = e.codigo AND e.codigo_prov = p.codigo
GO

SELECT * FROM vista2
GO

-- otra forma
IF OBJECT_ID('vista2') IS NOT NULL DROP VIEW vista2
GO
CREATE VIEW vista2 (titulo, autor, nombre_ed, nombre_pro, precio)
    AS SELECT l.titulo, l.autor, e.nombre, p.nombre, l.precio
        FROM tlibros as l, teditoriales as e, tprovincias as p
        WHERE l.editorial = e.codigo AND e.codigo_prov = p.codigo
GO

SELECT * FROM vista2
GO













