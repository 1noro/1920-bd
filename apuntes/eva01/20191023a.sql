-- # Apuntes SQL Alfredo RG 20191023a
USE BD_biblioteca

-- # TEMA 5 (GRUPOS)

-- ##EX: cuantos libros hay de cada genero
SELECT genero, count(*) -- -> gÃ©nero, nÃºmero_de_libros_por_gÃ©nero
    FROM tlibros
    GROUP BY genero

SELECT genero, count(*)
    FROM tlibros
    GROUP BY genero
    ORDER BY count(*) DESC

SELECT genero, count(genero)
    FROM tlibros
    GROUP BY genero
    ORDER BY count(genero) DESC

-- ##EX: cuantos libros tengo de cada genero con precio superior a 15 â‚¬
SELECT genero, count(*)
    FROM tlibros
    WHERE precio > 15
    GROUP BY genero
    ORDER BY count(*) DESC

-- ##EX: libros que su precio supere la media de precios de la tabla
SELECT genero, count(*)
    FROM tlibros
    WHERE precio >= (SELECT avg(precio) FROM tlibros)
    GROUP BY genero
    ORDER BY count(*) DESC

-- ##EX: lo mismo que el anterior pero solo los libros que (precio > avg) y los titulos superen la media de las longitudes de los títulos
--SELECT AVG(LEN(titulo)) FROM tlibros -- -> la media de las longitudes de los títulos de la tabla
SELECT genero, count(*)
    FROM tlibros
    WHERE 
        precio >= (SELECT avg(precio) FROM tlibros) AND
        LEN(titulo) >= (SELECT AVG(LEN(titulo)) FROM tlibros )
    GROUP BY genero
    ORDER BY count(*) DESC

-- ## los libros mas caros
SELECT titulo, precio 
    FROM tlibros

SELECT titulo, precio
    FROM tlibros
    WHERE precio = (SELECT max(precio) FROM tlibros )

-- ## los 5 libros mas caros
SELECT titulo, precio 
    FROM tlibros
    ORDER BY precio DESC

-- ## solo el primero de la lista
SELECT TOP 1 titulo, precio 
    FROM tlibros
    ORDER BY precio DESC

-- ## el primero de la lista (o los primeros si son iguales)
SELECT TOP 1 WITH TIES titulo, precio 
    FROM tlibros
    ORDER BY precio DESC

-- ## los 4 mas baratos (con repetidos)
SELECT TOP 4 WITH TIES titulo, precio 
    FROM tlibros
    ORDER BY precio ASC

-- ## los 10% primeros de la tabla (mustra solo el 10% superior de la tabla)
SELECT TOP 10 PERCENT titulo, precio 
    FROM tlibros
    ORDER BY precio DESC
