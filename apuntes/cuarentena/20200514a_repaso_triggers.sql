-- REPASO EXAMEN 2a EVA
USE bdmuebles
GO

SELECT * INTO tmuebles1 FROM tmuebles
GO

SELECT * FROM tmuebles1
GO

-- EX: Trigger que cuando modifiquemos solo el stock actual, va a comprobar si está por encima
-- o por debajo del minimo y el máximo, y cambia el aviso.

-- AVISOS:
-- B: baja
-- A: alta
-- N: normal

-- USAR: tmuebles1

IF EXISTS (SELECT name FROM sysobjects WHERE name = 't1' AND type = 'TR')
        DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tmuebles1
    AFTER UPDATE -- || INSTEAD OF
    AS
    	IF UPDATE(stock_actual) BEGIN
	    	DECLARE @cod char(10)
	    	DECLARE @sact decimal(9, 2)
	    	DECLARE @smin decimal(9, 2)
	    	DECLARE @smax decimal(9, 2)
	    	SET @cod = (SELECT codigo FROM INSERTED)
	        SET @sact = (SELECT stock_actual FROM INSERTED)
	        SET @smin = (SELECT stock_min FROM INSERTED)
	        SET @smax = (SELECT stock_max FROM INSERTED)
	        -- SELECT @cod, @sact, @smin, @smax
	        IF @sact > @smin AND @sact < @smax
	        	UPDATE tmuebles1 SET aviso = 'N' WHERE codigo = @cod
	        ELSE BEGIN 
		        IF @sact <= @smin
		        	UPDATE tmuebles1 SET aviso = 'B' WHERE codigo = @cod
		        ELSE
		        	UPDATE tmuebles1 SET aviso = 'A' WHERE codigo = @cod
	        END
        END
GO

UPDATE tmuebles1
    SET stock_actual = 13
    WHERE codigo = 'ERTY23'
GO

SELECT * FROM tmuebles1
GO

-- VERSION 2 (ESTE ES EL QUE MAS LE GUSTA A EMILIO)
IF EXISTS (SELECT name FROM sysobjects WHERE name = 't1' AND type = 'TR')
        DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tmuebles1
    AFTER UPDATE -- || INSTEAD OF
    AS
    	IF UPDATE(stock_actual) BEGIN
	    	DECLARE @cod char(10)
	    	DECLARE @sact decimal(9, 2)
	    	DECLARE @smin decimal(9, 2)
	    	DECLARE @smax decimal(9, 2)
	        SELECT @cod = codigo, @sact = stock_actual, @smin = stock_min, @smax = stock_max FROM INSERTED
	        -- SELECT @cod, @sact, @smin, @smax
	        IF @sact > @smin AND @sact < @smax
	        	UPDATE tmuebles1 SET aviso = 'N' WHERE codigo = @cod
	        ELSE BEGIN 
		        IF @sact <= @smin
		        	UPDATE tmuebles1 SET aviso = 'B' WHERE codigo = @cod
		        ELSE
		        	UPDATE tmuebles1 SET aviso = 'A' WHERE codigo = @cod
	        END
        END
GO

UPDATE tmuebles1
    SET stock_actual = 1
    WHERE codigo = 'ERTY23'
GO

SELECT * FROM tmuebles1
GO

-- VERSIÓN 3
IF EXISTS (SELECT name FROM sysobjects WHERE name = 't1' AND type = 'TR')
        DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tmuebles1
    AFTER UPDATE -- || INSTEAD OF
    AS
    	IF UPDATE(stock_actual) BEGIN
	    	DECLARE @cod char(10)
	    	DECLARE @sact decimal(9, 2)
	    	DECLARE @smin decimal(9, 2)
	    	DECLARE @smax decimal(9, 2)
	        SELECT @cod = codigo, @sact = stock_actual, @smin = stock_min, @smax = stock_max FROM INSERTED
	        UPDATE tmuebles1 SET aviso = 'N' WHERE codigo = @cod AND @sact > @smin AND @sact < @smax
	        UPDATE tmuebles1 SET aviso = 'B' WHERE codigo = @cod AND @sact <= @smin
	        UPDATE tmuebles1 SET aviso = 'A' WHERE codigo = @cod AND @sact >= @smax
        END
GO

UPDATE tmuebles1
    SET stock_actual = 1
    WHERE codigo = 'ERTY23'
GO

SELECT * FROM tmuebles1
GO

-- VERSIÓN 4 (para múltiples updates)
IF EXISTS (SELECT name FROM sysobjects WHERE name = 't1' AND type = 'TR')
        DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tmuebles1
    AFTER UPDATE -- || INSTEAD OF
    AS
    	IF UPDATE(stock_actual) BEGIN
	    	DECLARE c1 CURSOR
			    KEYSET LOCAL
			    FOR SELECT codigo, stock_actual, stock_min, stock_max FROM INSERTED
            
	    	DECLARE @cod char(10)
	    	DECLARE @sact decimal(9, 2)
	    	DECLARE @smin decimal(9, 2)
	    	DECLARE @smax decimal(9, 2)
	    	
	    	OPEN c1
			    FETCH FIRST FROM c1 INTO @cod, @sact, @smin, @smax
			    WHILE @@fetch_status = 0
			        BEGIN
			            UPDATE tmuebles1 SET aviso = 'N' WHERE codigo = @cod AND @sact > @smin AND @sact < @smax
				        UPDATE tmuebles1 SET aviso = 'B' WHERE codigo = @cod AND @sact <= @smin
				        UPDATE tmuebles1 SET aviso = 'A' WHERE codigo = @cod AND @sact >= @smax
			            FETCH NEXT FROM c1 INTO @cod, @sact, @smin, @smax
			        END
			
			CLOSE c1
			DEALLOCATE c1 -- liberamos la memoria
        END
GO

UPDATE tmuebles1
    SET stock_actual = 1
    WHERE codigo = 'ERTY23' OR codigo = 'FE2345'
GO

SELECT * FROM tmuebles1
GO

-- DEBERES: 
-- Creamos una tabla de pedidos si no existe (codigo, familia, cod_proveedor, unidades_pedidas, precio_pedido)
-- Y la borramos si se queda vacía
-- Con el trigger:
-- Se llenará una tupla cada vez que al modificar el estock actual entro en stock mínimo
-- Si modifico el sact y vuelvo a la normalidad borro el pedido
-- Sin comprobar que bajen dos veces en el stock minimo





