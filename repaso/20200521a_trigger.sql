USE BD_Alumnos
GO

/*
USE BD_Alumnos
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p_backup' AND type = 'P')
    DROP PROCEDURE p_backup
GO

CREATE PROCEDURE p_backup AS
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'talumnos1' AND type = 'U')
        DROP TABLE talumnos1
    SELECT * INTO talumnos1 FROM talumnos
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tnotas1' AND type = 'U')
        DROP TABLE tnotas1
    SELECT * INTO tnotas1 FROM tnotas
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tciclos1' AND type = 'U')
        DROP TABLE tciclos1
    SELECT * INTO tciclos1 FROM tciclos
GO

EXEC p_backup
GO
*/

-- Crear un desencadenador que visualice los siguientes mensajes 
-- ‘Se ha modificado la nota 1’, ‘Se ha modificado la nota 2’ o 
-- ‘Se ha modificado la nota 3’, según que en la tabla de tnotas 
-- se modifique la nota1, nota2 o nota 3, respectivamente. Si 
-- existiese un desencadenador previo con dicho nombre, se borrará.

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'trigger1' AND type = 'TR')
    DROP TRIGGER trigger1
GO

CREATE TRIGGER trigger1
    ON tnotas1
    AFTER UPDATE
    AS BEGIN
        IF UPDATE(nota1) PRINT 'Se ha modificado la nota 1'
        IF UPDATE(nota2) PRINT 'Se ha modificado la nota 2'
        IF UPDATE(nota3) PRINT 'Se ha modificado la nota 3'
    END
GO

UPDATE tnotas1 
    SET nota2 = 3.50
    WHERE dni = '77777777J'
GO

/*
SELECT * FROM tnotas1
*/