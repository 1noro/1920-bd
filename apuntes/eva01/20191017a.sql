-- # Apuntes SQL Alfredo RG 20191017a
USE BD_biblioteca
-- # CARACTERES Y STRINGS
-- ##EX: titulo y autor donde conicida la última letra del titulo con la primera del autor
SELECT titulo, autor
    FROM tlibros
    WHERE right(titulo,1) = left(autor,1) 

-- ## patindex(patron, string) -- -> posicion del primer caracter de la coincidencia (ermpieza en 0)
SELECT patindex('%ana%', 'badana') -- -> 4 (coincide y el primero en el 4)
SELECT patindex('%col%', 'casa') -- -> 0 (no coincide)
-- NOTA: % = 0 o N caracteres

-- ##EX: todos los titulos que tengan %un%
SELECT titulo
    FROM tlibros
    WHERE titulos LIKE '%un%'

SELECT titulo
    FROM tlibros
    WHERE patindex('%un%', titulo) <> 0

-- ##EX: todos los titulos que tengan %un% en la segunda pos
SELECT titulo
    FROM tlibros
    WHERE patindex('%un%', titulo) = 2

SELECT titulo
    FROM tlibros
    WHERE titulo LIKE '_un%'
-- NOTA: _ = 1 caracter cualquiera

-- ##EX: la negación de lo anterior
SELECT titulo
    FROM tlibros
    WHERE patindex('%un%', titulo) <> 2

SELECT titulo
    FROM tlibros
    WHERE titulo NOT LIKE '_un%'

-- ##EX: lo anterior pero mostrar solo los que contiene %un% (en cualquier otra posición)
SELECT titulo
    FROM tlibros
    WHERE patindex('%un%', titulo) <> 2 AND patindex('%un%', titulo) <> 0

SELECT titulo
    FROM tlibros
    WHERE titulo NOT LIKE '_un%' AND titulo LIKE '%un%'

-- ## quotename(valor, '{}' || '()' || '[]' || '""')
SELECT quotename(titulo,'{}') -- -> '{valor}'
    FROM tlibros

-- ## reverse(valor)
SELECT 'zorra asa arroz a', reverse('zorra asa arroz a') -- -> 'zorra asa arroz a', 'a zorra asa arroz'
SELECT titulo, reverse(titulo) FROM tlibros

