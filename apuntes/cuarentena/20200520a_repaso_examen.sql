USE bdmuebles
GO

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- tmuebles
-- EX: cursor, ruptura.
-- Por cada familia, de cada mueble que me salga, el tipo_largo, la denom, el precio, el sact y el valor del stock.
-- Al acabar cada familia que me visualize el nombre de la familia, el tototal de stock de todo lo de esa familia, 
-- y el total de muebles en la familia.
-- Al final el valor total del stock indistintamente de las familias.

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT tipo, denominacion, stock_actual, precio, stock_actual * precio, familia 
    	FROM tmuebles
    	ORDER BY familia

DECLARE @tipo char(1) 
DECLARE @tipo_largo char(9) 
DECLARE @denom char(20)
DECLARE @sact decimal(9, 2)
DECLARE @precio money
DECLARE @precio_multi money
DECLARE @familia char(5)
DECLARE @familia_ant char(5)

DECLARE @muebles_en_fam int
DECLARE @sact_en_fam decimal(9, 2)
DECLARE @precio_tot money

SET @muebles_en_fam = 0
SET @sact_en_fam = 0
SET @precio_tot = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @tipo, @denom, @sact, @precio, @precio_multi, @familia
    SET @familia_ant = @familia
    WHILE @@fetch_status = 0
        BEGIN
            IF @familia != @familia_ant BEGIN 
	            PRINT ''
	            PRINT '>> ' + @familia_ant + ' (MUEBLES: ' + ltrim(str(@muebles_en_fam, 4, 0)) + ') ' + ' (STOCK: ' + ltrim(str(@sact_en_fam, 7, 2)) + ') '
	            PRINT ''
	            SET @muebles_en_fam = 0
	            SET @sact_en_fam = 0
	            SET @familia_ant = @familia
            END
            
            IF @tipo = 'A' SET @tipo_largo = 'Muy Alta'
            IF @tipo = 'B' SET @tipo_largo = 'Alta'
            IF @tipo = 'C' SET @tipo_largo = 'Normal'
            IF @tipo = 'D' SET @tipo_largo = 'Baja'
            IF @tipo = 'F' SET @tipo_largo = 'Muy Baja'
            IF @tipo NOT IN ('A', 'B', 'C', 'D', 'F') SET @tipo_largo = '???'
            
            PRINT @tipo_largo + ' ' + @denom + ' ' + str(@sact, 10, 2) + ' ' + str(@precio, 10, 2) + ' ' + str(@precio_multi, 10, 2)
            
            SET @muebles_en_fam = @muebles_en_fam + 1
            SET @sact_en_fam = @sact_en_fam + @sact
            SET @precio_tot = @precio_tot + @precio_multi
            
            FETCH NEXT FROM c1 INTO @tipo, @denom, @sact, @precio, @precio_multi, @familia
        END
CLOSE c1
DEALLOCATE c1

PRINT ''
PRINT '>> ' + @familia_ant + ' (MUEBLES: ' + ltrim(str(@muebles_en_fam, 4, 0)) + ') ' + ' (STOCK: ' + ltrim(str(@sact_en_fam, 7, 2)) + ') '
PRINT ''
PRINT '## PRECIO TOTAL: ' + str(@precio_tot, 10, 2) 

/*
SELECT * FROM tmuebles

SELECT SUM(stock_actual * precio) FROM tmuebles
*/

