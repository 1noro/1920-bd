-- # Apuntes SQL Alfredo RG 20191213a
USE BD_biblioteca

-- #CURSORES
DECLARE cursorPrueba CURSOR
    KEYSET
    LOCAL
    FOR SELECT titulo, precio
        FROM tlibros

DECLARE
    @titulo varchar(50),
    @precio money

-- recorremos solo el primer resultado (solo un fetch)
OPEN cursorPrueba
    FETCH cursorPrueba INTO @titulo, @precio
    PRINT @titulo + ' ' + str(@precio, 8, 2)
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria

-- recorremos toda la tabla
OPEN cursorPrueba
    WHILE @@fetch_status = 0
        BEGIN
            FETCH cursorPrueba INTO @titulo, @precio -- por omision es next
            PRINT @titulo + ' ' + str(@precio, 8, 2)
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria

-- recorremos toda la tabla
OPEN cursorPrueba
    FETCH ABSOLUTE 1 FROM cursorPrueba INTO @titulo, @precio --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            FETCH NEXT FROM cursorPrueba INTO @titulo, @precio -- es recomendable poner el next
            PRINT @titulo + ' ' + str(@precio, 8, 2)
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria

-- recorremos toda la tabla
OPEN cursorPrueba
    FETCH ABSOLUTE 1 FROM cursorPrueba INTO @titulo, @precio --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            FETCH RELATIVE 1 FROM cursorPrueba INTO @titulo, @precio -- es recomendable poner el next
            PRINT @titulo + ' ' + str(@precio, 8, 2)
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria

-- recorremos toda la tabla al reves
OPEN cursorPrueba
    FETCH ABSOLUTE -1 FROM cursorPrueba INTO @titulo, @precio --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            PRINT @titulo + ' ' + str(@precio, 8, 2)
            FETCH PRIOR FROM cursorPrueba INTO @titulo, @precio -- es recomendable poner el next
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria

-- recorremos toda la tabla al reves
OPEN cursorPrueba
    FETCH ABSOLUTE -1 FROM cursorPrueba INTO @titulo, @precio --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            PRINT @titulo + ' ' + str(@precio, 8, 2)
            FETCH RELATIVE -1 FROM cursorPrueba INTO @titulo, @precio -- es recomendable poner el next
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria

-- recorremos toda la tabla al reves
OPEN cursorPrueba
    FETCH FIRST FROM cursorPrueba INTO @titulo, @precio --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            FETCH NEXT FROM cursorPrueba INTO @titulo, @precio -- es recomendable poner el next
            PRINT @titulo + ' ' + str(@precio, 8, 2)
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria

-- recorremos toda la tabla al reves
OPEN cursorPrueba
    FETCH LAST FROM cursorPrueba INTO @titulo, @precio --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            PRINT @titulo + ' ' + str(@precio, 8, 2)
            FETCH PRIOR FROM cursorPrueba INTO @titulo, @precio -- es recomendable poner el next
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria
