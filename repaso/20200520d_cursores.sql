USE BDBiblioteca
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0 BEGIN
        PRINT @titulo
        FETCH NEXT FROM c1 INTO @titulo
    END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- Empezando por el final, te saltas tres y printeas 5
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, autor FROM tlibros

DECLARE @titulo char(50)
DECLARE @autor char(30)
DECLARE @cont int

SET @cont = 0

OPEN c1
    FETCH ABSOLUTE -3 FROM c1 INTO @titulo, @autor
    WHILE @@fetch_status = 0 AND @cont < 5 BEGIN
		PRINT @titulo + ' ' + @autor
		SET @cont = @cont + 1
        FETCH PRIOR FROM c1 INTO @titulo, @autor
    END
CLOSE c1
DEALLOCATE c1






