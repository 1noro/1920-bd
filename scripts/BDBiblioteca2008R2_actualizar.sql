
IF OBJECT_ID('teditoriales') IS NOT NULL DROP TABLE teditoriales
IF OBJECT_ID('tprovincias') IS NOT NULL DROP TABLE tprovincias

CREATE TABLE tprovincias (
    codigo int primary key,
    nombre varchar(23)
)

CREATE TABLE teditoriales (
    codigo char(4) primary key,
    nombre varchar(35),
    codigo_prov int references tprovincias(codigo)
)

-- llenamos provincias
INSERT INTO tprovincias(codigo, nombre) VALUES (15, 'Coruña, A')
INSERT INTO tprovincias(codigo, nombre) VALUES (27, 'Lugo')
INSERT INTO tprovincias(codigo, nombre) VALUES (32, 'Ourense')
INSERT INTO tprovincias(codigo, nombre) VALUES (36, 'Pontevedra')
INSERT INTO tprovincias(codigo, nombre) VALUES (1, 'Alava')
INSERT INTO tprovincias(codigo, nombre) VALUES (2, 'Albacete')
INSERT INTO tprovincias(codigo, nombre) VALUES (28, 'Madrid')
INSERT INTO tprovincias(codigo, nombre) VALUES (8, 'Barcelona')
INSERT INTO tprovincias(codigo, nombre) VALUES (50, 'Zaragoza')

-- llenamos teditoriales
INSERT INTO teditoriales(codigo, nombre, codigo_prov)
    SELECT DISTINCT UPPER(left(editorial, 4)), editorial , NULL
        FROM tlibros
        WHERE UPPER(left(editorial, 4)) <> 'EDIC' -- porque se repiten

-- EL PRIMER GRUPO DE REPETIDAS ------------------------------------------------
INSERT INTO teditoriales(codigo, nombre)
    SELECT DISTINCT left(upper(editorial), 2) + substring(UPPER(editorial), 11, 2), editorial
        FROM tlibros
        WHERE left(lower(editorial), 9) = 'ediciones'

-- EL SEGUNDO GRUPO DE REPETIDAS -----------------------------------------------
INSERT INTO teditoriales(codigo, nombre)
    SELECT DISTINCT left(upper(editorial), 2) + substring(UPPER(editorial), 10, 2), editorial
        FROM tlibros
        WHERE left(lower(editorial), 9) = 'edicións'

-- Meter las provincias (manualmente)
UPDATE teditoriales
    SET codigo_prov = 15
    WHERE codigo = 'ABAL'

UPDATE teditoriales
    SET codigo_prov = 27
    WHERE codigo = 'ACAN'

INSERT INTO tprovincias(codigo, nombre) VALUES (3, 'Alicante')

UPDATE teditoriales
    SET codigo_prov = 3
    WHERE codigo = 'ALFA'

-- modificar nombre de la eiditorial por su codigo en tprueba
UPDATE tlibros
    SET editorial = (SELECT codigo
                            FROM teditoriales
                            WHERE tlibros.editorial = teditoriales.nombre)

-- modifcamos el campo editorial de la tabla tlibros para que coincida
-- con el char(4) not null de teditoriales.codigo
ALTER TABLE tlibros
    ALTER COLUMN editorial char(4) not null
