-- # Apuntes SQL Alfredo RG 20200217a
USE BD_biblioteca

-- SELECT * INTO tprueba FROM tlibros

-- #trigger (desencadenador, disparador):
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tprimero' AND type = 'TR')
        DROP TRIGGER tprimero
GO

CREATE TRIGGER tprimero
    ON tprueba
    AFTER UPDATE -- || INSTEAD OF
    AS
        PRINT 'Trigger: tprimero'
GO

-- SELECT * FROM tprueba

/*SELECT titulo, precio
    FROM tprueba
    WHERE isbn = '0-261-10320-2'
GO*/

/*UPDATE tprueba
    SET precio = 30.25
    WHERE isbn = '0-261-10320-2'
GO*/

-- ########################################
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tsegundo' AND type = 'TR')
        DROP TRIGGER tsegundo
GO

CREATE TRIGGER tsegundo
    ON tprueba
    AFTER UPDATE -- || INSTEAD OF
    AS
        PRINT 'Trigger: tsegundo'
GO

-- SELECT * FROM tprueba

/*SELECT titulo, precio
    FROM tprueba
    WHERE isbn = '0-261-10320-2'
GO*/

UPDATE tprueba
    SET precio = 30.25
    WHERE isbn = '0-261-10320-2'
GO


-- se puede cambiar la prioridad en la que se ejecutan, pero solo puedes enviarlos al principio o al final de la lista

-- el orden se modifica con: 
