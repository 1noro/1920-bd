use bdbiblioteca
go

select * from tlibros
go

/*
 * procedimiento <-- _precio
 * select precio > _precio
 */

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
    DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @_precio money AS
    SELECT titulo, autor, precio
        FROM tlibros
        WHERE precio > @_precio
GO

EXEC p1 12.00
GO

/*
 * procedimiento <-- _letra
 * select primera letra del titulo == _letra
 */

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
        DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @letra char AS
    SELECT titulo
        FROM tlibros
        WHERE titulo LIKE @letra + '%'
        -- WHERE substring(titulo, 1, 1) = @letra
        -- WHERE left(titulo, 1) = @letra
GO

EXEC p1 's'
GO

/*
 * procedimiento <-- precio
 * procedimiento --> si || no si el precio > media de todos los precios
 */

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
        DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @precio money AS
    /*DECLARE @media money
	SET @media = (SELECT AVG(precio) FROM tlibros)
	IF (@precio > @media) SELECT 'SI'*/
	IF (@precio > (SELECT AVG(precio) FROM tlibros)) PRINT 'SI'
	ELSE PRINT 'NO'
GO

EXEC p1 15
GO

/*
 * 
 */

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
        DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @precio money, @salida varchar(2) OUTPUT AS
	IF (@precio > (SELECT AVG(precio) FROM tlibros)) 
		SET @salida = 'SI'
	ELSE 
		SET @salida = 'NO'
GO

DECLARE @mi_salida varchar(2)
EXEC p1 15, @mi_salida OUTPUT
PRINT 'Resultado: ' + @mi_salida
GO


-- #EX: Que liste el titulo, autor y fecha de edicion de todos los libros editados en una
--      fecha posterior a la fecha que pasamos como parametro.

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'pfecha' AND type = 'P')
        DROP PROCEDURE pfecha
GO

CREATE PROCEDURE pfecha @_fecha date AS
	SELECT titulo, autor, fech_ed
        FROM tlibros
        WHERE fech_ed > @_fecha
        ORDER BY fech_ed
GO

EXEC pfecha '2004-09-17'
GO

-- #EX: Procedimiento al que se le envia un ISBN y visualizar en la ventana de cuadricula 
--      todos los libros editados desde la fecha del libro introducido, llamando al otro 
--      procedimiento anterior.

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'pfecha_from_isbn' AND type = 'P')
        DROP PROCEDURE pfecha_from_isbn
GO

CREATE PROCEDURE pfecha_from_isbn @_isbn char(13) AS
	DECLARE @fech_ed datetime
	SET @fech_ed = (SELECT fech_ed FROM tlibros WHERE isbn = @_isbn)
	EXEC pfecha @fech_ed
GO

EXEC pfecha_from_isbn '0-261-10320-2'
GO

-- #EX: basandonos en el anterior enviar 2 fechas, mostrar los comprendidos entre las 2 sin importar el orden de ambas.
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'consultaRangoFecha' AND type = 'P')
        DROP PROCEDURE consultaRangoFecha
GO

CREATE PROCEDURE consultaRangoFecha @_fecha1 datetime, @_fecha2 datetime AS
    DECLARE @aux datetime
    IF @_fecha1 > @_fecha2 BEGIN
        SET @aux = @_fecha1
        SET @_fecha1 = @_fecha2
        SET @_fecha2 = @aux
    END
    SELECT titulo, autor, fech_ed
        FROM tlibros
        WHERE fech_ed > @_fecha1 AND fech_ed < @_fecha2
        ORDER BY fech_ed
GO

EXEC consultaRangoFecha '1995-01-01', '1993-01-01'
GO

-- EX: proc <- nombre ed
-- se muestra el isbn, titulo y nombre ed de los libros de esa editorial
SELECT * FROM teditoriales
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
        DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @_n_ed varchar(35) AS
    SELECT l.isbn, l.titulo, e.nombre
    FROM tlibros AS l, teditoriales AS e
    WHERE l.editorial = e.codigo AND e.nombre = @_n_ed
GO

EXEC p1 'Plaza & Janés'
GO

-- EX: proc <- isbn
-- proc -> nombre editorial al que pertenece
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
        DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @_isbn char(13), @_n_ed varchar(35) OUTPUT AS
    SELECT @_n_ed = e.nombre
    FROM tlibros AS l, teditoriales AS e
    WHERE l.editorial = e.codigo AND l.isbn = @_isbn
GO

DECLARE @myNombreEd varchar(35)
EXEC p1 '0-261-10320-2', @myNombreEd OUTPUT 
SELECT @myNombreEd
GO


