IF OBJECT_ID('tprovincias') IS NOT NULL DROP TABLE tprovincias
CREATE TABLE tprovincias (
    codigo int primary key,
    nombre varchar(23)
)
