-- # Apuntes SQL Alfredo RG 20191016a
USE BD_biblioteca
-- # CARACTERES Y STRINGS
SELECT 'col'+' y '+'flor'
SELECT rtrim('col    ')+'y'+ltrim('    flor') -- quitar espacios en blanco r (der), l (izq)

-- ## substring
SELECT titulo, left(titulo, 2) -- por la izquirda
    FROM tlibros
SELECT titulo, right(titulo, 4) -- por la derecha
    FROM tlibros

-- desde el 5 hasta 5+8 (primero 1) (por la izq)
SELECT substring('Bienaventuranza', 5, 8) -- -> aventura

SELECT substring('Bienaventuranza', 1, 3) -- -> Bie
SELECT left('Bienaventuranza', 3) -- -> Bie

SELECT substring('Bienaventuranza', 13, 3) -- -> nza
SELECT right('Bienaventuranza', 3) -- -> nza
SELECT substring('Bienaventuranza', LEN('Bienaventuranza') - (3 - 1), 3) -- -> nza

-- emular el right con el substring
SELECT titulo, substring(titulo, LEN(titulo) - (3 - 1), 3), right(titulo, 3)
    FROM tlibros
-- en mayúsculas
SELECT titulo, UPPER(substring(titulo, LEN(titulo) - (3 - 1), 3)), right(titulo, 3)
    FROM tlibros
SELECT titulo, substring(UPPER(titulo), LEN(titulo) - (3 - 1), 3), right(titulo, 3)
    FROM tlibros

-- ## replace(ori, patron, valor_por_el_que_cambiar)
SELECT replace('malabares', 'a', 'u')
SELECT replace('malabares', 'mala', 'malos ')

-- ## replicate
select replicate('pa', 12) -- -> papapapapapapapapapapapa
select replicate('_', 12) -- -> ____________

-- ver titulos y al lado tantos * como letras
SELECT titulo, replicate('*', len(titulo))
    FROM tlibros
/*SELECT titulo, replace(titulo, '%', '*') -- no funciona, porque % no es all
    FROM tlibros*/

-- ## stuff(ori, desde, desde+x, valor_para_sustituir)
SELECT stuff('colesterol', 2, 7, 'arac') -- -> caracol; c'arac'ol
SELECT replace('colesterol', 'olester', 'arac') -- -> caracol; c'arac'ol (no tendría mucho sentido)

-- # conversión de int -> string; función str(n, lt, d)
select 5+8 -- -> 13 (constantes numéricas)
select '5'+'8' -- -> '58'(constantes de caracteres; strings)
/* str(valor, logitud resultante) [por defecto la longitud es 10] */
select str(5, 1)+'8' -- -> '58'(constantes de caracteres; strings)
select str(5, 2)+'8' -- -> ' 58'(constantes de caracteres; strings)
select str(23647.3457) -- -> '     23647'
select str(23647.3457, 4) -- -> '****'
select str(23647.3457, 8, 4) -- -> '23647.3'
select str(23647.3457, 11, 4) -- -> '23647.3457'



