-- # Apuntes SQL Alfredo RG 20191118a
USE BD_biblioteca

IF OBJECT_ID('tprueba1') IS NOT NULL DROP TABLE tprueba1
IF OBJECT_ID('tprueba2') IS NOT NULL DROP TABLE tprueba2

-- #TEMA 8
-- tablas temporales (locales): solo para el usuario que la creó
CREATE TABLE #tprueba (
    numero int primary key,
    nombre varchar(15)
)
GO

INSERT INTO #tprueba VALUES(1, 'Manuel')
INSERT INTO #tprueba VALUES(2, 'María')

SELECT * FROM #tprueba

-- tablas temporales (globales): para todos los usuarios
CREATE TABLE ##tprueba (
    numero int primary key,
    nombre varchar(15)
)
GO

INSERT INTO ##tprueba VALUES(1, 'Manuel')
INSERT INTO ##tprueba VALUES(2, 'María')

SELECT * FROM ##tprueba

-- #VARIABLES (@)
DECLARE @nombre varchar(15)
SET @nombre = 'Carlos'
-- SELECT @nombre
PRINT @nombre

DECLARE @numero1 int
DECLARE @numero2 int
DECLARE @suma int
SET @numero1 = 6
SET @numero2 = 34
SET @suma = @numero1 + @numero2
PRINT ltrim(str(@numero1, 4)) + ' + ' + ltrim(str(@numero2, 4)) + ' = ' + ltrim(str(@suma, 4))

-- variables del sistema (@@)
SELECT @@language

-- IMPORTANTE (PUEDE ENTRAR EN EL EXAMEN)
SELECT * FROM tlibros
SELECT @@rowcount -- -> las tuplas visulizadas en la última sentencia

SELECT @@version












