-- # Apuntes SQL Alfredo RG 20191028a
USE BD_biblioteca

SELECT * FROM tprueba1
    WHERE isbn in (SELECT isbn FROM tprueba2)

SELECT count(*) FROM tprueba2

-- # TRANSACCIONES (ROLLBACK y COMMIT)
BEGIN TRANSACTION
    SELECT * FROM tprueba1 -- muestro todo
    DELETE FROM tprueba1 -- boro todo el contenido
    SELECT * FROM tprueba1 -- muestro la tabla vacía
ROLLBACK TRANSACTION -- deshacemos hasta el BEGIN
SELECT  * FROM tprueba1 -- muestra la tabla llena como si no hubiese pasado nada (antes del BEGIN)

BEGIN TRAN
    SELECT * FROM tprueba1 
    DELETE FROM tprueba1
    SELECT * FROM tprueba1 
ROLLBACK TRAN 
SELECT  * FROM tprueba1 

BEGIN 
    SELECT * FROM tprueba1 
    DELETE FROM tprueba1
    SELECT * FROM tprueba1 
ROLLBACK  
SELECT  * FROM tprueba1 

BEGIN 
    SELECT * FROM tprueba1 
    DELETE FROM tprueba1
    SELECT * FROM tprueba1 
COMMIT  -- confirmamos todo lo que hay en la transacción (desde el BEGIN al COMMIT)
SELECT  * FROM tprueba1 -- la tabla esta vacía

-- ##EX: subir el precio un 5% a los libros de tprueba2 que son mútuos a tlibros
SELECT titulo, precio, precio * 1.05 FROM tprueba2

UPDATE tprueba2
    SET precio = precio + (precio * 0.05)
    WHERE isbn in (SELECT isbn FROM tlibros)

SELECT titulo, precio FROM tlibros
SELECT titulo, precio FROM tprueba2

-- Demostramos el cambio entre las 2 tablas 
SELECT a.titulo 'tlibros titulo', a.precio, b.titulo 'tprueba2 titulo', b.precio
    FROM tlibros as a
        JOIN tprueba2 as b
            ON a.isbn = b.isbn










