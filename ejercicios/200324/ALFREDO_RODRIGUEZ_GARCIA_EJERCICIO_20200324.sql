
USE BDBiblioteca
GO

-- EJEMPLO EMILIO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'funcionEscalar' and type = 'FN')
	DROP FUNCTION funcionEscalar
GO 

CREATE FUNCTION funcionEscalar (@libro nvarchar(50)) RETURNS nvarchar(50)
AS BEGIN
	DECLARE @TituloAutor nvarchar(50)
	SELECT @TituloAutor = titulo + ' – ' + autor
		FROM tlibros
		WHERE titulo = @libro
	RETURN (@TituloAutor)
END
GO

PRINT dbo.funcionEscalar('Rebeca');

-- --------------------------------------------------------------------------------

-- EJERCICIO 1

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'fun' and type = 'FN')
	DROP FUNCTION fun
GO 

CREATE FUNCTION fun(@isbn char(13)) RETURNS varchar(50)
AS BEGIN
	DECLARE @titulo varchar(50)
	SELECT @titulo = titulo
		FROM tlibros
		WHERE isbn = @isbn
	RETURN (@titulo)
END
GO

SELECT dbo.fun('14-10-82338-0');

-- --------------------------------------------------------------------------------

-- EJERCICIO 2

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'funfun' and type = 'FN')
	DROP FUNCTION funfun
GO 

CREATE FUNCTION funfun(@cod_ed char(4)) RETURNS varchar(50)
AS BEGIN
	DECLARE @titulo varchar(50)
	SELECT TOP 1 @titulo = l.titulo
		FROM tlibros AS l, teditoriales AS e
		WHERE l.editorial = e.codigo AND e.codigo = @cod_ed
	RETURN (@titulo)
END
GO

SELECT dbo.funfun('PLAZ');

