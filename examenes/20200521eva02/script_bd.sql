create database BD_Alumnos
go
use BD_Alumnos
go
create table talumnos(
dni char(9) primary key,
nombre varchar(15),
ape1 varchar(15),
ape2 varchar(15),
fec_nac datetime,
fec_mat datetime,
sexo char(1)
)
go
create table tciclos(
ciclo char(3)not null primary key,
nombre varchar(28)
)
go
create table tnotas(
ciclo char(3) not null references tciclos,
curso int not null,
numero int not null,
dni char(9) not null,
nota1 numeric(5,2),
nota2 numeric(5,2),
nota3 numeric(5,2)
primary key(ciclo,curso,numero)
)
go
insert into talumnos values('11111111A','Juan','Gómez','López','12/10/1980','05/09/2012','V')
insert into talumnos values('11111112M','Jacobo','Muñiz','Grandio','8/3/1986','6/9/2012','V')
insert into talumnos values('11111113N','Luís','Jato','Rovira','5/4/1985','4/9/2012','V')
insert into talumnos values('11111114O','Beatriz','Lobato','Mazaricos','27/10/1983','3/9/2012','M')
insert into talumnos values('11111115P','Laura','Geremías','Rodríguez','26/12/84','1/9/2012','M')
insert into talumnos values('22222222B','Sara','Santiso','Fernández','5/6/1984','7/9/2012','M')
insert into talumnos values('33333333C','Luís','García','Boado','6/4/1982','1/9/2012','V')
insert into talumnos values('44444444D','María','Mosquera','Serantes','5/5/1986','15/9/2012','M')
insert into talumnos values('55555555H','Oscar','López','López','18/8/1981','5/9/2012','V')
insert into talumnos values('66666666l','Pilar','Méndez','Losada','7/1/1982','2/9/2012','M')
insert into talumnos values('77777777J','Francisco','Basante','Argüelles','30/8/1984','14/9/2012','V')
insert into talumnos values('88888888K','José','Sandá','Pita','25/11/1981','11/9/2012','V')
insert into talumnos values('99999999L','Ana','Penedo','Gutierrez','7/8/1985','6/9/2012','M')
go
insert into tciclos values('AyF','Administración y Finanzas')
insert into tciclos values('Dam','Desarrollo de apl. Multiplat')
insert into tciclos values('Sec','Secretariado')
go
insert into tnotas values('AyF',1,1,'66666666l',7,2,7)
insert into tnotas values('AyF',1,2,'77777777J',8.5,9.20,6.80)
insert into tnotas values('AyF',2,1,'88888888K',4.5,6,2.5)
insert into tnotas values('DAM',1,1,'11111111A',9,8.7,9.5)
insert into tnotas values('DAM',1,2,'22222222B',6,5.6,10)
insert into tnotas values('DAM',2,1,'44444444D',9,8.70,9.5)
insert into tnotas values('DAM',2,2,'55555555H',6,4,4)
insert into tnotas values('DAM',2,3,'33333333C',5.8,7.6,9.2)
insert into tnotas values('SEC',1,1,'99999999L',6,3.6,8.10)
insert into tnotas values('SEC',1,2,'11111112M',7.5,8.2,4.8)
insert into tnotas values('SEC',1,3,'11111113N',8,2.45,10)
insert into tnotas values('SEC',1,4,'11111114O',5,6.7,5.5)
insert into tnotas values('SEC',1,5,'11111115P',8.7,6,5.5)


