\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}El concepto de radiactividad}{3}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Definiendo la radiactividad}{3}{subsection.1.1}% 
\contentsline {subsection}{\numberline {1.2}Usos de la radiactividad}{3}{subsection.1.2}% 
\contentsline {subsection}{\numberline {1.3}La radiactividad ``natural''}{3}{subsection.1.3}% 
\contentsline {paragraph}{\IeC {\textquestiondown }Est\IeC {\'a} presente la radiactividad en la naturaleza, y d\IeC {\'o}nde?}{3}{section*.2}% 
\contentsline {section}{\numberline {2}El proceso de producci\IeC {\'o}n}{3}{section.2}% 
\contentsline {section}{\numberline {3}Los riesgos}{3}{section.3}% 
\contentsline {section}{\numberline {4}Los accidentes}{3}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Three Mile Island, 1979}{3}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}Chern\IeC {\'o}bil, 1986}{3}{subsection.4.2}% 
\contentsline {subsubsection}{\numberline {4.2.1}Serie de televisi\IeC {\'o}n de 2019, HBO}{3}{subsubsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.3}Fukushima, 2011}{3}{subsection.4.3}% 
\contentsline {section}{\numberline {5}Los residuos}{3}{section.5}% 
\contentsline {paragraph}{\IeC {\textquestiondown }Podr\IeC {\'\i }as describir el proceso de almacenaje de los residuos nucleares de fisi\IeC {\'o}n?}{3}{section*.3}% 
\contentsline {section}{\numberline {6}Transici\IeC {\'o}n ecol\IeC {\'o}gica}{3}{section.6}% 
\contentsline {paragraph}{\IeC {\textquestiondown }Son las centrales nucleares la mejor soluci\IeC {\'o}n para una futura transici\IeC {\'o}n ecol\IeC {\'o}gica hacia un mundo 100\% renovable?}{3}{section*.4}% 
