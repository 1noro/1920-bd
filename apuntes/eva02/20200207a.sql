-- # Apuntes SQL Alfredo RG 20200207a
USE BD_biblioteca

-- #movidas de clase:
/*CREATE DATABASE prueba
GO*/

USE prueba
GO

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'familias' AND type = 'U')
        DROP TABLE familias
GO

CREATE TABLE familias(
    familia char(3),
    denominacion varchar(15),
    importe money
)
GO

INSERT INTO familias VALUES ('FER', 'Martilo', 27)
INSERT INTO familias VALUES ('COM', 'Cinturón', 12)
INSERT INTO familias VALUES ('ALI', 'Manzanas', 2)
INSERT INTO familias VALUES ('COM', 'Pendientes', 16)
INSERT INTO familias VALUES ('COM', 'Sortija', 5)
INSERT INTO familias VALUES ('FER', 'Tenaza', 22)
INSERT INTO familias VALUES ('COM', 'Diadema', 7)
INSERT INTO familias VALUES ('FER', 'Alicate', 25)
INSERT INTO familias VALUES ('ALI', 'Peras', 4)
GO

/*SELECT * FROM familias
GO*/

-- CURSOR
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT familia, denominacion, importe
        FROM familias
        ORDER BY familia

-- DECLARACIÓN DE VARIABLES
DECLARE
    @fam char(3),
    @famAnt char(3),
    @den char(15),
    @imp money,
    @cont int,
    @suma money

-- VALORES INICIALES
SET @cont = 0
SET @suma = 0

-- MECANICA DEL CURSOR
OPEN c1
    FETCH FIRST FROM c1 INTO @fam, @den, @imp
    SET @famAnt = @fam
    WHILE @@fetch_status = 0
        BEGIN
            IF @fam != @famAnt BEGIN
                PRINT ''
                PRINT '#' + @famAnt + ': ' + ltrim(str(@suma, 7, 2))
                PRINT ''
                SET @suma = 0
                SET @famAnt = @fam
            END
            PRINT ' ' + @den + ': ' +  ltrim(str(@imp, 7, 2))
            SET @suma = @suma + @imp
            SET @cont = @cont + 1
            FETCH NEXT FROM c1 INTO @fam, @den, @imp
        END

PRINT ''
PRINT '#' + @famAnt + ': ' + ltrim(str(@suma, 7, 2))
PRINT ''

PRINT '#Artículos listados: ' + ltrim(str(@cont, 7, 0))

CLOSE c1
DEALLOCATE c1 -- liberamos la memoria


