-- # Apuntes SQL Alfredo RG 20191021b
USE BD_biblioteca

-- # TEMA 5 (GRUPOS)
SELECT titulo, editorial
    FROM tlibros
    WHERE editorial = 'alfaguara'

SELECT editorial, count(*) -- -> nombre_editorial, número_de_libros_por_editorial
    FROM tlibros
    GROUP BY editorial

SELECT editorial, count(*)
    FROM tlibros
    GROUP BY editorial
    ORDER BY count(*) DESC

SELECT editorial, count(*)
    FROM tlibros
    GROUP BY editorial
    HAVING count(*) > 5 
    ORDER BY count(*) DESC