USE bdmuebles
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0 BEGIN
        PRINT @titulo
        FETCH NEXT FROM c1 INTO @titulo
    END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO
-- RUPTURA
-- SOBRE bdmuebles
-- Ruptura que liste el código, denominación, nombre proveedor y tipo de mueble en formato largo (CON CASE)
-- de cada uno de los muebles
-- Y que se rompa por cada una de las familias
-- Y mostrar el nombre entero de la familia y el total de muebles en la familia

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT m.codigo, m.denominacion, p.nombre AS n_proveed, f.nombre AS n_familia,
    		CASE m.tipo
                WHEN 'a' THEN 'Muy Alta'
                WHEN 'b' THEN 'Alta'
                WHEN 'c' THEN 'Normal'
                WHEN 'd' THEN 'Baja'
                WHEN 'f' THEN 'Muy Baja'
            END AS calidad
    	FROM tmuebles m, tproveed p, tfamilias f
    	WHERE m.cod_proveedor = p.codigo AND m.familia = f.codigo
    	ORDER BY f.nombre

DECLARE @codigo char(10)
DECLARE @denominacion char(20)
DECLARE @n_proveed char(30)
DECLARE @n_familia char(15)
DECLARE @n_familia_ant char(15)
DECLARE @tipo char(8)

DECLARE @cont int

SET @cont = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @codigo, @denominacion, @n_proveed, @n_familia, @tipo
    SET @n_familia_ant = @n_familia
    WHILE @@fetch_status = 0 BEGIN
		IF @n_familia_ant != @n_familia BEGIN
			PRINT ''
			PRINT '>> ' + rtrim(@n_familia_ant) + ' (' + ltrim(str(@cont, 7, 0)) + ')'
			PRINT ''
			SET @cont = 0
			SET @n_familia_ant = @n_familia
		END
		PRINT ' ' + @codigo + ' ' + @denominacion + ' ' + @n_proveed + ' ' + @tipo
		SET @cont = @cont + 1
        FETCH NEXT FROM c1 INTO @codigo, @denominacion, @n_proveed, @n_familia, @tipo
    END
CLOSE c1
DEALLOCATE c1

PRINT ''
PRINT '>> ' + rtrim(@n_familia_ant) + ' (' + ltrim(str(@cont, 7, 0)) + ')'
PRINT ''

