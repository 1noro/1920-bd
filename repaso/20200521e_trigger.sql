USE BD_Alumnos
GO

/*
USE BD_Alumnos
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p_backup' AND type = 'P')
    DROP PROCEDURE p_backup
GO

CREATE PROCEDURE p_backup AS
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'talumnos1' AND type = 'U')
        DROP TABLE talumnos1
    SELECT * INTO talumnos1 FROM talumnos
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'talumnos2' AND type = 'U')
        DROP TABLE talumnos2
    SELECT * INTO talumnos2 FROM talumnos
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tnotas1' AND type = 'U')
        DROP TABLE tnotas1
    SELECT * INTO tnotas1 FROM tnotas
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tciclos1' AND type = 'U')
        DROP TABLE tciclos1
    SELECT * INTO tciclos1 FROM tciclos
GO

EXEC p_backup
GO
*/

-- Hacer una copia de TALUMNOS en TALUMNOS1 y TALUMNOS2
-- SELECT * INTO talumnos1 FROM talumnos
-- SELECT * INTO talumnos2 FROM talumnos
-- Trigger sobre TALUMNOS1 que cuando insertas una tupla lo inserte en TALUMNOS2 
-- y no en TALUMNOS1
-- Extensión: Contemplar con 2 triggers mas el caso del DELETE y el UPDATE.

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'trigger1' AND type = 'TR')
    DROP TRIGGER trigger1
GO

CREATE TRIGGER trigger1
    ON talumnos1
    INSTEAD OF INSERT
    AS BEGIN
        INSERT INTO talumnos2 SELECT * FROM INSERTED
    END
GO

/*
INSERT INTO talumnos1 
    VALUES ('48111665X', 'Frodoric', 'Froncostin', '???', '2020-05-21', '2020-05-21', 'V') 
INSERT INTO talumnos1 
    VALUES ('11111115A', 'Juan', 'Gomez', 'Lopez', '1980-10-12 00:00:00.000', '2012-09-05 00:00:00.000', 'V') 
*/

/*
UPDATE talumnos1
    SET nota2 = 3.50
    WHERE dni = '77777777J'
GO
*/

/*
SELECT * FROM tnotas1
SELECT * FROM talumnos1
SELECT * FROM talumnos2
SELECT * FROM thistorico
*/