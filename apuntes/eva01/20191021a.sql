-- # Apuntes SQL Alfredo RG 20191021a
USE BD_biblioteca

SELECT precio
    FROM tlibros
    WHERE datepart(mm, fech_ed) = 8

-- EX: Cuanto valen todos los libros publicados en el mes 8 (Agosto).
SELECT SUM(precio) -- -> 181.2400
    FROM tlibros
    WHERE datepart(mm, fech_ed) = 8

SELECT SUM(precio)
    FROM tlibros
    WHERE datename(mm, fech_ed) = 'agosto'

SELECT SUM(precio)
    FROM tlibros
    WHERE month(fech_ed) = 8

-- # CONVERSIÓN DE TIPOS
SELECT str(34567.897) -- -> '     34568'
SELECT str(34567.897, 6) -- -> ' 34568'
SELECT str(34567.897, 6, 3) -- -> ' 34568'
SELECT str(34567.897, 10, 3) -- -> '  34567.897'
SELECT str(34567.897, 10, 2) -- -> '    34567.90'

-- convert(tipo_al_que_convertir, valor, formato)
SELECT fech_ed, convert(varchar, fech_ed, 106)
    FROM tlibros