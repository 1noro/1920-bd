-- # Apuntes SQL Alfredo RG 20200127a
USE BD_biblioteca

-- # PROCEDIMIENTOS ALMACENADOS
-- procedimiento1 (variable de entrada)
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'procedimiento1' AND type = 'P')
        DROP PROCEDURE procedimiento1
GO

CREATE PROCEDURE procedimiento1 @_genero varchar(15) AS
    SELECT titulo, autor, genero, precio
        FROM tlibros
        WHERE genero = @_genero
GO

EXEC procedimiento1 'Novela'

-- procedimiento2 (variable de salida)
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'procedimiento2' AND type = 'P')
        DROP PROCEDURE procedimiento2
GO

CREATE PROCEDURE procedimiento2 @media money OUTPUT AS
    SELECT @media = AVG(precio) FROM tlibros
GO

DECLARE @myMedia money
EXEC procedimiento2 @myMedia OUTPUT
-- PRINT 'Media: ' + ltrim(str(@myMedia, 7, 2))
PRINT @myMedia
GO

-- procedimiento3 (variable de entrada y otra de salida)
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'procedimiento3' AND type = 'P')
        DROP PROCEDURE procedimiento3
GO

CREATE PROCEDURE procedimiento3 @_genero varchar(15), @media money OUTPUT AS
    SELECT @media = AVG(precio)
        FROM tlibros
        WHERE genero = @_genero
GO

DECLARE @myMedia money
EXEC procedimiento3 'Novela', @myMedia OUTPUT
PRINT 'Media: ' + ltrim(str(@myMedia, 7, 2))
