USE BDBiblioteca
GO

/*
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p2' AND type = 'P')
	DROP PROCEDURE p2
GO

CREATE PROCEDURE p2 AS
	IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tlibros1' AND type = 'U')
		DROP TABLE tlibros1
	SELECT * INTO tlibros1 FROM tlibros
GO

EXEC p2
GO
*/

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0 BEGIN
        PRINT @titulo
        FETCH NEXT FROM c1 INTO @titulo
    END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- Crear un cursor que rellene en el campo encuadernación con ‘Lujo’ o ‘Bolsillo’, según que 
-- el precio del libro supere o no al precio medio del total de libros.

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT isbn, precio FROM tlibros1 ORDER BY editorial 

DECLARE @isbn char(13)
DECLARE @precio money

DECLARE @media money

SELECT @media = AVG(precio) FROM tlibros1

OPEN c1
    FETCH FIRST FROM c1 INTO @isbn, @precio
    WHILE @@fetch_status = 0 BEGIN
		IF @precio > @media
			UPDATE tlibros1 SET encuadernacion = 'Lujo'  where isbn = @isbn
		ELSE
			UPDATE tlibros1 SET encuadernacion = 'Bolsillo'  where isbn = @isbn
        FETCH NEXT FROM c1 INTO @isbn, @precio
    END
CLOSE c1
DEALLOCATE c1

/*
update tlibros1 set encuadernacion = 'lujoo'  where isbn = '84-226-5005-3'
SELECT * FROM tlibros1 WHERE isbn = '84-226-5005-3'
*/

SELECT isbn, encuadernacion, precio FROM tlibros1


