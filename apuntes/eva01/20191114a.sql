-- # Apuntes SQL Alfredo RG 20191114a
USE BD_biblioteca

SELECT * FROM teditoriales

-- Meter las provincias (manualmente)
UPDATE teditoriales
    SET codigo_prov = 15
    WHERE codigo = 'ABAL'

UPDATE teditoriales
    SET codigo_prov = 27
    WHERE codigo = 'ACAN'

SELECT * FROM teditoriales

INSERT INTO tprovincias(codigo, nombre) VALUES (3, 'Alicante')

UPDATE teditoriales
    SET codigo_prov = 3
    WHERE codigo = 'ALFA'

SELECT * FROM teditoriales

--prox dia
/*
    referenciar teditoriales a la tabla tlibros
    cambiando el nombre por el codigo
*/
