-- # Apuntes SQL Alfredo RG 20191018a
USE BD_biblioteca
-- # MANEJO DE FECHAS
-- ## SUMA Y RESTA DE FECHAS
-- ### RESTA dateadd(patron_de_valor_a_sumar, fecha_a_restar || num, fecha)
SELECT getdate(), datediff(yy, fech_ed, getdate()) -- -> nom de años desed fech_ed hasta hoy
    FROM tlibros
SELECT datediff(yy, '1995-11-11', getdate()) -- -> cuantos años tengo
SELECT datediff(mm, '1995-11-11', getdate()) -- -> cuantos meses tengo
SELECT datediff(dd, '1995-11-11', getdate()) -- -> cuantos días tengo
SELECT datediff(hh, '1995-11-11', getdate())/24 -- -> cuantas horas tengo
SELECT datediff(mi, '1995-11-11', getdate())/24 -- -> cuantos minutos tengo
SELECT datediff(ss, '1995-11-11', getdate())/24 -- -> cuantos segundos tengo

-- ##EX: Lista de titulos cuya fecha de edición sea dentro de los últimos 15 años
SELECT titulo, fech_ed
    FROM tlibros
    WHERE datediff(yy, fech_ed, getdate()) <= 15

-- ## Diferencias entre datepart() y datename()
SELECT datepart(dd, getdate()), datename(dd, getdate()) -- -> 18, 18
SELECT datepart(mm, getdate()), datename(mm, getdate()) -- -> 10, Octubre
SELECT datepart(yy, getdate()), datename(yy, getdate()) -- -> 2019, 2019

SELECT datepart(dw, getdate()), datename(dw, getdate()) -- -> 5, Viernes

-- ##EX: Todos los libros editados en el 3er trimestre (quarter) (verano)
SELECT titulo, fech_ed
    FROM tlibros
    WHERE datepart(qq, fech_ed) = 3

SELECT titulo, fech_ed -- forma chula
    FROM tlibros
    WHERE datepart(mm, fech_ed) IN (7, 8, 9)

-- ##EX: Todos los libros editados en un mes con '%r%'
SELECT titulo, datename(mm, fech_ed)
    FROM tlibros
    WHERE datename(mm, fech_ed) LIKE '%r%'

SELECT titulo, datename(mm, fech_ed)
    FROM tlibros
    WHERE patindex('%r%', datename(mm, fech_ed)) != 0

-- # CONVERSIÓN DE TIPOS
-- str(valor_de_otro_tipo)
SELECT '5' + '6' -- -> '56'
SELECT cast('5' as int) + cast('6' as int) -- -> 11
SELECT cast(5 as char) + cast(6 as char) -- -> '5                             6                             '
SELECT rtrim(cast(5 as char)) + cast(6 as char) -- -> '56                             '
SELECT rtrim(cast(5 as char)) + rtrim(cast(6 as char)) -- -> '56'













