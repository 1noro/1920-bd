-- Soluciones Examen Final 1a Evaluación (20191209)

-- ex1
select 
    titulo, 
    convert(char(2), day(fech_ed)) + ' ' + datename(month, fech_ed) + ' ' + str(year(fech_ed), 4)
    from tlibros
    where tema = 'Novela' and datediff(yy, fech_ed, getadate()) > 12

-- ex2
select lower(nombre), titulo, precio
    from tlibos l, editoriales e
    where l.cod_ed = e.cod_ed and precio = (select max(precio) from tlibros)

select lower(nombre), titulo, precio
    from tlibos, editoriales
    where editorial = codigo and precio = (select max(precio) from tlibros)

select top 1 with ties lower(nombre), titulo, precio
    from tlibos, editoriales
    where editorial = codigo
    order by precio desc

-- ex3
select titulo, convert(varchar, precio, 1)
    from tlibros where precio > 10
        and titulo like '%T%' -- no distingue entre mayusculas y minusculas

-- ex4
select titulo, nombre 
    from tlibros, editoriales
    where editorial = codigo and nombre not like '%l%'

-- ex5
-- create table ...


