-- a
USE BDBiblioteca
GO

SELECT * FROM tprovincias
GO

/*
DROP TABLE prueba
SELECT * INTO prueba FROM tprovincias
GO
*/

-- EX: Procedure <- nombre de provincia
-- autoincrementa el ID
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
    DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @nombrep varchar(23) AS
    DECLARE @new_id int
    SET @new_id = (SELECT MAX(codigo) FROM prueba) + 1
    INSERT INTO prueba (codigo, nombre) VALUES (@new_id, @nombrep)
GO

EXEC p1 'Hawai'
GO

SELECT * FROM prueba
GO

-- EX: Procedure <- nombre de provincia
-- autoincrementa el ID
DELETE FROM prueba

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
        DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @nombrep varchar(23) AS
    DECLARE @max_id int
    DECLARE @new_id int
    SET @max_id = (SELECT MAX(codigo) FROM prueba)
    SET @new_id = 1
    IF (@max_id IS NOT NULL) SET @new_id = @max_id + 1
    INSERT INTO prueba (codigo, nombre) VALUES (@new_id, @nombrep)
GO

EXEC p1 'Hawai'
GO

SELECT * FROM prueba
GO

-- EX: 
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
        DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 AS
    SELECT l.titulo, e.nombre, p.nombre, convert(varchar, l.fech_ed, 106) 
    FROM tlibros l, teditoriales e, tprovincias p
    WHERE l.editorial = e.codigo AND e.codigo_prov = p.codigo AND l.precio > 15
GO

EXEC p1 
GO

