USE BD_Alumnos
GO

/*
SELECT * FROM talumnos
GO
SELECT * FROM tnotas
GO
SELECT * FROM tciclos
GO
*/

-- EX1
CREATE TABLE tnotas2 (
    ciclo char(3) NOT NULL,
    curso int NOT NULL,
    numero int NOT NULL,
    dni char(9) NOT NULL,
    nota1 numeric(6, 2) NOT NULL DEFAULT 0,
    nota2 numeric(6, 2) NOT NULL DEFAULT 0,
    nota3 numeric(6, 2) NOT NULL DEFAULT 0,
    CONSTRAINT pk_claAlum PRIMARY KEY (ciclo, curso, numero),
    FOREIGN KEY (dni) REFERENCES talumnos(dni)
)

/*
SELECT * FROM tnotas2
*/

-- EX2
/*
DROP TABLE tnotas1
GO
SELECT * INTO tnotas1 FROM tnotas
GO
SELECT * FROM tnotas1
GO
*/

UPDATE tnotas1
    SET 
        nota1 = 1,
        nota2 = 2,
        nota3 = 3
    FROM tnotas1 AS n, talumnos AS a
    WHERE n.dni = a.dni AND a.sexo = 'V' AND n.ciclo = 'SEC'
GO

-- EX3
/*
DROP TABLE ##alumnas_ap
*/
SELECT a.dni, a.ape1, a.ape2, a.nombre, ((n.nota1 + n.nota2 + n.nota3) / 3) 'media' INTO  ##alumnas_ap 
    FROM talumnos AS a, tnotas AS n
    WHERE a.dni = n.dni AND sexo = 'M' AND ((nota1 + nota2 + nota3) / 3) > 4.5
    ORDER BY ((nota1 + nota2 + nota3) / 3) desc
GO

-- EX4
SELECT a.dni, a.nombre, a.ape1, a.ape2, CAST(((n.nota1 + n.nota2 + n.nota3) / 3) AS numeric(5, 2)) 'media',
    CASE
        WHEN ((n.nota1 + n.nota2 + n.nota3) / 3) > 5 THEN 'APROBADO'
        ELSE 'SUSPENSO'
    END AS media_letras
    FROM talumnos AS a, tnotas AS n
    WHERE a.dni = n.dni
GO

-- EX5
CREATE PROCEDURE p1 @dni_in char(9), @out numeric(6, 2) OUTPUT AS
    SET @out = (SELECT (nota1 + nota2 + nota3) / 3 FROM tnotas WHERE dni = @dni_in)
GO

-- EX6
Select a.dni,a.nombre as 'nombre_a',ape1,ape2,nota1,nota2,nota3,c.nombre as 'nombre_c' into tsuspensos 
from talumnos a,tnotas n,tciclos c 
where a.dni=n.dni and c.ciclo=n.ciclo and (nota1<5 or nota2<5 or nota3<5)

/*
SELECT * FROM tsuspensos
*/

-- EX7
declare c7 cursor
    keyset local
    for select dni from tnotas

declare @nota numeric(6,2),@media numeric(6,2),@dni char(9),@may char(9),@sex char(5)
set @nota=0
set @media=0
open c7
    fetch first from c7 into @dni
    while @@fetch_status = 0
        begin
        exec p1 @dni, @nota output
            if @nota>@media
        begin 
                set @may=@dni
                set @media=@nota
            end
            fetch next from c7 into @dni
        end
if @nota>@media
        begin 
                set @may=@dni
                set @media=@nota
            end
set @sex=(select sexo from talumnos where dni=@may)
if @sex='V'
set @sex='Varón'
else
set @sex='Mujer'
print @sex+'   '+str(@media)
close c7
deallocate c7


-- EX8
/*
DROP TABLE talumnos1
GO
SELECT * INTO talumnos1 FROM talumnos
GO
SELECT * FROM talumnos1
GO
*/

CREATE TRIGGER t1
    ON talumnos1
    AFTER INSERT
    AS
        DECLARE @edad int
        SET @edad = (SELECT DATEDIFF(yy, fec_nac, fec_mat) FROM INSERTED)
        PRINT @edad
GO

INSERT INTO talumnos1 VALUES('11111111R','Carlos', 'Campos', 'Canosa','1997-08-01', getdate(), 'V', null)
GO

/*
DELETE FROM talumnos
WHERE dni = '11111111R'
*/


-- EX9
/*
SELECT * FROM talumnos
GO
SELECT * FROM tnotas
GO
SELECT * FROM tciclos
GO
*/

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p9' AND type = 'P')
    DROP PROCEDURE p9
GO

CREATE PROCEDURE p9 @ciclo_in char(3), @nota_in numeric(6, 2) AS
    SELECT a.nombre, a.ape1, a.ape2, a.fec_nac
        FROM talumnos AS a, tnotas AS n
        WHERE a.dni = n.dni AND (n.ciclo = @ciclo_in OR n.nota1 > @nota_in OR n.nota2 > @nota_in OR n.nota3 > @nota_in)
GO

EXEC p9 'AyF', 5