-- # Apuntes SQL Alfredo RG 20200227a
USE BD_biblioteca

-- borramos tprueba
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tprueba' AND type = 'U')
        DROP TABLE tprueba
GO

-- borramos tprueba1
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tprueba1' AND type = 'U')
        DROP TABLE tprueba1
GO

-- creamos tprueba
SELECT * INTO tprueba FROM tlibros
GO

-- añadir a tprueba un atributo de tipo binario prestado, por defecto a 0
ALTER TABLE tprueba ADD prestado bit DEFAULT 0 NOT NULL
GO

select * from tprueba

-- trigger que cuente los libros prestados en una tabla y si el numero es 0 se borra la tabla
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 't1' AND type = 'TR')
        DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tprueba
    AFTER UPDATE -- || INSTEAD OF
    AS
        IF UPDATE(prestado) BEGIN
            DECLARE @cont int
            SET @cont = 0

            IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = 'contpresta' AND type = 'U') 
                CREATE TABLE contpresta (cont int default 0)
            ELSE SET @cont = (SELECT cont FROM contpresta)

            IF ()

        END
GO
