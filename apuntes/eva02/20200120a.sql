-- # Apuntes SQL Alfredo RG 20200120a
USE BD_biblioteca

-- #EX: ver titulo, genero y precio de todos los libros y a l final de cada genero que te diga el número de libros, la suma de los precios y el priecio medio. Y al final el total de temas que tengo, y el tema que mas libros tiene


DECLARE ruptura_00 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, genero, precio
        FROM tlibros
        ORDER BY genero

--DECLARE @titulo varchar(50), @vocales int, @i int, @c char
DECLARE
    @titulo char(50),
    @genero char(15),
    @generoAnt char(15),
    @generoMax char(15),
    @precio money,
    @sumPrecio money,
    @contGenero int,
    @contGeneroMax int,
    @totGenero int
    
SET @sumPrecio = 0
SET @contGenero = 0
SET @contGeneroMax = 0
SET @totGenero = 0

OPEN ruptura_00
    FETCH FIRST FROM ruptura_00 INTO @titulo, @genero, @precio
    SET @generoAnt = @genero
    SET @generoMax = @genero
    WHILE @@fetch_status = 0
        BEGIN
            IF @generoAnt != @genero
                BEGIN
                    -- PUNTO DE RUPTURA
                    PRINT ''
                    PRINT '#Total, ' + rtrim(@generoAnt) + ': ' + ltrim(str(@sumPrecio, 7, 2)) + ' (AVG: ' + ltrim(str(@sumPrecio / @contGenero, 7, 2)) + ')'
                    PRINT ''
                    IF @contGenero > @contGeneroMax
                        BEGIN
                            SET @contGeneroMax = @contGenero
                            SET @generoMax = @generoAnt
                        END
                    SET @sumPrecio = 0
                    SET @contGenero = 0
                    SET @generoAnt = @genero
                    SET @totGenero = @totGenero + 1
                END
            PRINT rtrim(@titulo) + ', ' + rtrim(@genero) + ', ' + ltrim(str(@precio, 7, 2))
            SET @sumPrecio = @sumPrecio + @precio
            SET @contGenero = @contGenero + 1
            FETCH NEXT FROM ruptura_00 INTO @titulo, @genero, @precio
        END

PRINT ''
PRINT '#Total, ' + rtrim(@genero) + ': ' + ltrim(str(@sumPrecio, 7, 2)) + ' (AVG: ' + ltrim(str(@sumPrecio / @contGenero, 7, 2)) + ')'
PRINT ''
PRINT '#Número total de géneros: ' + ltrim(str(@totGenero, 7, 0))
PRINT '#Género con mas libros: ' + rtrim(@generoMax) + ' (' + ltrim(str(@contGeneroMax, 7, 0)) + ')'
PRINT ''

CLOSE ruptura_00
DEALLOCATE ruptura_00 -- liberamos la memoria





