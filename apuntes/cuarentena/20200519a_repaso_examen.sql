USE bdmuebles
GO

PRINT '------------------------------------------------------------------'

-- tmuebles
-- EX: cursor, visualizar: tipo, denom, smax, smin, sact, precio_multiplicado
-- Al final el precio total del stock de los que están en stock mínimo (B)
-- hacer prints con convert

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT tipo, denominacion, stock_max, stock_min, stock_actual, (stock_actual * precio)
    	FROM tmuebles

DECLARE @tipo char(1) 
DECLARE @denom char(20)
DECLARE @smax decimal(9, 2)
DECLARE @smin decimal(9, 2)
DECLARE @sact decimal(9, 2)
DECLARE @precio_multi money
DECLARE @precio_tot money

SET @precio_tot = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @tipo, @denom, @smax, @smin, @sact, @precio_multi
    WHILE @@fetch_status = 0
        BEGIN
            -- PRINT @tipo + ' ' + @denom + ' ' + str(@smax, 7, 2) + ' ' + str(@smin, 7, 2) + ' ' + str(@sact, 7, 2) + ' ' + str(@precio_multi, 7, 2)
            PRINT @tipo + ' ' + @denom + ' ' + convert(char(10), @smax) + ' ' + convert(char(10), @smin) + ' ' + convert(char(10), @sact) + ' ' + convert(char(10), @precio_multi)
            IF @sact <= @smin SET @precio_tot = @precio_tot + @precio_multi
            FETCH NEXT FROM c1 INTO @tipo, @denom, @smax, @smin, @sact, @precio_multi
        END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria

PRINT ''
PRINT 'Total de los que están en stock mínimo: ' + convert(char(10), @precio_tot)
GO

PRINT '------------------------------------------------------------------'
-- tmuebles
-- EX: cursor, como el anterior, pero en ved de tipo poner un case que diga la calidad

/*
SELECT denominacion, tipo, 
	CASE tipo
		WHEN 'a' THEN 'Muy Alta'
		WHEN 'b' THEN 'Alta'
		WHEN 'c' THEN 'Normal'
		WHEN 'd' THEN 'Baja'
		WHEN 'f' THEN 'Muy Baja'
	END AS calidad
	FROM tmuebles
*/

DECLARE c1 CURSOR
    KEYSET LOCAL -- podría poner el case aquí
    FOR SELECT tipo, denominacion, stock_max, stock_min, stock_actual, (stock_actual * precio)
    	FROM tmuebles

DECLARE @tipo char(1) 
DECLARE @tipo_largo char(9) 
DECLARE @denom char(20)
DECLARE @smax decimal(9, 2)
DECLARE @smin decimal(9, 2)
DECLARE @sact decimal(9, 2)
DECLARE @precio_multi money
DECLARE @precio_tot money

SET @precio_tot = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @tipo, @denom, @smax, @smin, @sact, @precio_multi
    WHILE @@fetch_status = 0
        BEGIN
	        SET @tipo_largo = (
	        	SELECT CASE @tipo -- el select no es necesario, con el case solo debería bastar
						WHEN 'a' THEN 'Muy Alta'
						WHEN 'b' THEN 'Alta'
						WHEN 'c' THEN 'Normal'
						WHEN 'd' THEN 'Baja'
						WHEN 'f' THEN 'Muy Baja'
					END AS calidad
	        )
            -- PRINT @tipo + ' ' + @denom + ' ' + str(@smax, 7, 2) + ' ' + str(@smin, 7, 2) + ' ' + str(@sact, 7, 2) + ' ' + str(@precio_multi, 7, 2)
            PRINT @tipo_largo + ' ' + @denom + ' ' + convert(char(10), @smax) + ' ' + convert(char(10), @smin) + ' ' + convert(char(10), @sact) + ' ' + convert(char(10), @precio_multi)
            IF @sact <= @smin SET @precio_tot = @precio_tot + @precio_multi
            FETCH NEXT FROM c1 INTO @tipo, @denom, @smax, @smin, @sact, @precio_multi
        END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria

PRINT ''
PRINT 'Total de los que están en stock mínimo: ' + convert(char(10), @precio_tot)
GO

