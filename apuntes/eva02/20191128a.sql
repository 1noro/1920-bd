-- # Apuntes SQL Alfredo RG 20191128a
USE BD_biblioteca

-- entra solo hasta el tema 7

-- Si en una estructura de control (if, while, etc) engloba a mas de una única sentencia
--  hay que poner begin y end
declare @letra char
set @letra = 'a'

if @letra = 'a'
    begin
        print 'Acierto'
        print 'Es una: a'
    end
else
    print 'Fallo'

print 'Fin de ejecución'

-- while
declare @numero int
set @numero = 1
while (@numero <= 10) begin
    print @numero
    set @numero = @numero + 1
end
print 'Fin de ejecución'

-- while2
declare @numero int
set @numero = 1
while (@numero <= 10) begin
    print @numero
    if @numero = 5 break -- salimos del bucle al llegar al 5
    set @numero = @numero + 1
end
print 'Fin de ejecución'

-- while2
declare @numero int
set @numero = 0
while (@numero <= 10) begin
    set @numero = @numero + 1
    if @numero = 5 continue -- volvemos al principio del bucle sin visualizar el 5
    print @numero
end
print 'Fin de ejecución'

-- goto (horrible, obsoleto, reminiscencia del pasado) 
-- if fecha > '2001-01-01' print 'NO USAR NUNCA'
declare @numero int
set @numero = 5
if @numero = 5
	goto acierto
else
	goto fallo

acierto:
	print 'Se acertó con el número'
	goto continua

fallo:
	print 'Se falló, el número era 5'

continua:
	print 'Fin del programa'
	
-- case (estilo switch)
select * from teditoriales
select nombre, 
	case codigo_prov
		when 15 then 'A Coruña'
		when 27 then 'Lugo'
		when 32 then 'Ourense'
		when 36 then 'Pontevedra'
		else 'Otras'
	end 
	as Provincia
	from teditoriales
	
select titulo, precio,
	case
		when precio > 15 then 'Caro'
		else 'Barato'
	end 
	as 'Tipo libro'
	from tlibros







