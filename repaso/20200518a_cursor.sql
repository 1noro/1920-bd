USE BDBiblioteca
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH ABSOLUTE 1 FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            PRINT @titulo
            FETCH NEXT FROM c1 INTO @titulo
        END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

/*
 * Crear un cursor que lea todas las tuplas pares de la tabla libros, mostrando el ISBN, TITULO, 
 * PRECIO, IVA, y PRECIO más IVA (16%). Al final del listado se visualizará el precio total de 
 * todos los libros (sin iva). Se visualizarán en la pantalla de mensajes.
 * 
 */

PRINT '------------------------------------------------------------------'

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT isbn, titulo, precio 
    	FROM tlibros

DECLARE @isbn char(13) 
DECLARE @titulo char(50)
DECLARE @precio money
DECLARE @iva money
DECLARE @precio_tot money

SET @precio_tot = 0

OPEN c1
    FETCH ABSOLUTE 2 FROM c1 INTO @isbn, @titulo, @precio
    WHILE @@fetch_status = 0
        BEGIN
	        SET @iva = @precio * 0.16
            PRINT @isbn + ' ' + @titulo + ' ' + str(@precio, 7, 2) + ' ' + str(@iva, 7, 2) + ' ' + str(@precio + @iva, 7, 2)
            SET @precio_tot = @precio_tot + @precio
            FETCH RELATIVE 2 FROM c1 INTO @isbn, @titulo, @precio
        END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria

PRINT ''
PRINT 'Total: ' + str(@precio_tot, 7, 2)

/*
SELECT * FROM tlibros
GO
*/



