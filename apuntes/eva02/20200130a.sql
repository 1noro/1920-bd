-- # Apuntes SQL Alfredo RG 20200130a
USE BD_biblioteca

-- #EX: Que liste el titulo, autor y fecha de edicion de todos los libros editados en una
--      fecha posterior a la fecha que pasamos como parametro.
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'consultaFecha' AND type = 'P')
        DROP PROCEDURE consultaFecha
GO

CREATE PROCEDURE consultaFecha @_fecha datetime AS
    SELECT titulo, autor, fech_ed
        FROM tlibros
        WHERE fech_ed > @_fecha
        ORDER BY fech_ed
GO

-- #EX: Procedimiento al que se le envia un ISBN y visualizar en mensajes
--      el titulo, autor y fecha de edicion (formato, dia, mes con letras y año)
--      y además en la ventana de cuadricula todos los libros editados desde
--      la fecha del libro introducido, llamando al otro procedimiento anterior.
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'getDateFromISBN' AND type = 'P')
        DROP PROCEDURE getDateFromISBN
GO

CREATE PROCEDURE getDateFromISBN @_isbn char(13) AS
    DECLARE
        @titulo char(50),
        @autor char(30),
        @fecha datetime

    SELECT @titulo = titulo, @autor = autor, @fecha = fech_ed
        FROM tlibros
        WHERE isbn = @_isbn

    PRINT rtrim(@titulo) + ', ' + rtrim(@autor) + ', ' + ltrim(str(day(@fecha))) + ' ' + left(datename(mm, @fecha), 3) + ' ' + ltrim(str(year(@fecha)))

    EXEC consultaFecha @fecha
GO

EXEC getDateFromISBN '0-261-10320-2'
GO
