-- # Apuntes SQL Alfredo RG 20200124a
USE BD_biblioteca

-- # PROCEDIMIENTOS ALMACENADOS

/*
    En sysobjects existen los siguientes tipos:
        U - para tablas
        V - para vistas
        P - para procedimientos
*/

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'procedimiento1' AND type = 'P')
        DROP PROCEDURE procedimiento1
GO

CREATE PROCEDURE procedimiento1 AS
    SELECT titulo, autor, precio
        FROM tlibros
GO

EXEC procedimiento1
