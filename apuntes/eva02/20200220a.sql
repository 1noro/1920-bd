-- # Apuntes SQL Alfredo RG 20200220a
USE BD_biblioteca

-- cuando usamos el update sobre cualquier atributo el trigger se dispara independientemente del atributo actualizado, pero podemos restringir el trigger a un atributo en concreto

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tprimero' AND type = 'TR')
        DROP TRIGGER tprimero
GO

CREATE TRIGGER tprimero
    ON tprueba
    AFTER UPDATE -- || INSTEAD OF
    AS
        IF update(titulo) or update(precio)
            PRINT 'Se modificó titulo o precio'
        ELSE
            PRINT 'No se modificó titulo o precio'
GO

-- Se modificó titulo o precio
UPDATE tprueba
    SET precio = 33.25
    WHERE isbn = '0-261-10320-2'
GO

-- No se modificó titulo o precio
UPDATE tprueba
    SET autor = 'Patchy'
    WHERE isbn = '0-261-10320-2'
GO


-- #EX: trigger cada vez que borre una tupla de tprueba me crea una tupla en trescate con todos los atributos y la fecha de borrado, comprobar si existe la tabala y sino crearla

USE BD_Biblioteca
GO

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 't1' AND type = 'TR')
        DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tprueba
    AFTER DELETE -- || INSTEAD OF
    AS
        DECLARE
            @isbn char(13),
            @titulo varchar(50),
            @autor varchar(30),
            @genero varchar(15),
            @encuadernacion varchar(15),
            @editorial char(4),
            @fech_ed datetime,
            @num_ed numeric(10),
            @num_pag numeric(18),
            @precio money

        IF NOT EXISTS (
            SELECT name FROM sysobjects
            WHERE name = 'trescate' AND type = 'U'
        ) BEGIN

            CREATE TABLE trescate (
                isbn char(13),
                titulo varchar(50),
                autor varchar(30),
                genero varchar(15),
                encuadernacion varchar(15),
                editorial char(4),
                fech_ed datetime,
                num_ed numeric(10),
                num_pag numeric(18),
                precio money,
                fech_del datetime
            )

        END

        SELECT 
            @isbn = isbn,
            @titulo = titulo,
            @autor = autor,
            @genero = genero,
            @encuadernacion = encuadernacion,
            @editorial = editorial,
            @fech_ed = fech_ed,
            @num_ed = num_ed,
            @num_pag = num_pag,
            @precio = precio
        FROM
            deleted

        INSERT INTO trescate
            VALUES (
                @isbn,
                @titulo,
                @autor,
                @genero,
                @encuadernacion,
                @editorial,
                @fech_ed,
                @num_ed,
                @num_pag,
                @precio,
                getdate()
        )           
GO

-- select * from tprueba

DELETE FROM tprueba
    WHERE isbn = '14-10-82338-0'
GO

select * from tprueba
select * from trescate

-- OTRA VERSION
USE BD_Biblioteca
GO

-- DROP TABLE trescate

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 't1' AND type = 'TR')
        DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tprueba
    AFTER DELETE -- || INSTEAD OF
    AS

        IF NOT EXISTS (
            SELECT name FROM sysobjects
            WHERE name = 'trescate' AND type = 'U'
        ) BEGIN

            /*SELECT * INTO trescate From tprueba WHERE 1 = 2
            ALTER TABLE trescate ADD fech_del datetime*/
            SELECT *, getdate() fech_res INTO trescate FROM deleted

        END
        ELSE
            INSERT INTO trescate SELECT *, getdate() FROM deleted
          
GO

-- select * from tprueba

DELETE FROM tprueba
    WHERE isbn = '84-0133-720-8'
GO

select * from tprueba
select * from trescate

-- OTRA VERSION2
USE BD_Biblioteca
GO

-- DROP TABLE trescate

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 't1' AND type = 'TR')
        DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tprueba
    AFTER DELETE -- || INSTEAD OF
    AS

        IF NOT EXISTS (
            SELECT name FROM sysobjects
            WHERE name = 'trescate' AND type = 'U'
        ) AND (SELECT titulo FROM deleted) IS NOT NULL BEGIN
            SELECT *, getdate() fech_res INTO trescate FROM deleted
        END
        ELSE BEGIN
            IF (SELECT titulo FROM deleted) IS NOT NULL
                INSERT INTO trescate SELECT *, getdate() FROM deleted
        END
          
GO

-- select * from tprueba

DELETE FROM tprueba
    WHERE isbn = '84-0133-720-8'
GO

select * from tprueba
select * from trescate
