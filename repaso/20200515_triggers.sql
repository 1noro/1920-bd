USE BDBiblioteca
GO

-- EX: proc <- isbn
--     proc -> precio

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'get_precio' AND type = 'P')
	DROP PROCEDURE get_precio
GO

CREATE PROCEDURE get_precio @isbn char(13), @precio money OUTPUT AS
	-- SET @precio = (SELECT precio from tlibros WHERE @isbn = isbn)
	SELECT @precio = precio from tlibros WHERE @isbn = isbn
GO

/*DECLARE @mi_precio money
EXEC get_precio '0-261-10320-2', @mi_precio OUTPUT
PRINT @mi_precio
GO*/

-- EX: proc. que crea una copia de la tabla tlibros llamada tlibros1
--     previamente borrar tlibros1 si existe
/*IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p2' AND type = 'P')
	DROP PROCEDURE p2
GO

CREATE PROCEDURE p2 AS
	IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tlibros1' AND type = 'U')
		DROP TABLE tlibros1
	SELECT * INTO tlibros1 FROM tlibros
GO

EXEC p2
GO*/

-- EX: Trigger sobre la tabla tlibros1 que una vez que elimines una tupla,
--     usando el procedimiento anterior borrar todos los libros con mayor
--     precio que el libro borrado.
IF EXISTS (SELECT name FROM sysobjects WHERE name = 't1' AND type = 'TR')
    DROP TRIGGER t1
GO

CREATE TRIGGER t1
    ON tlibros1
    AFTER DELETE
    AS
    	DECLARE @isbn char(13)
        DECLARE @mi_precio money

        SELECT @isbn = isbn FROM DELETED

        EXEC get_precio @isbn, @mi_precio OUTPUT
        DELETE FROM tlibros1 WHERE precio > @mi_precio
GO

DELETE FROM tlibros1 WHERE isbn = '0-261-10320-2'
GO

SELECT * FROM tlibros1
GO
