-- # Apuntes SQL Alfredo RG 20191118a
USE BD_biblioteca

SELECT * FROM tprovincias
SELECT * FROM teditoriales
SELECT * FROM tlibros

-- creamos una copia de tlibros para trabajar con esa
SELECT * INTO tprueba FROM tlibros

-- modificar nombre de la eiditorial por su codigo en tprueba
SELECT ed.codigo, li.editorial
    FROM teditoriales as ed, tprueba as li 
    WHERE li.editorial = ed.codig

UPDATE tprueba
    SET editorial = 
        (SELECT codigo FROM teditoriales WHERE editorial = nombre)

SELECT titulo, editorial
    FROM tprueba

DROP TABLE tprueba

UPDATE tlibros
    SET editorial = (SELECT codigo 
                            FROM teditoriales 
                            WHERE editorial = nombre)
-- mas clarito
UPDATE tlibros
    SET editorial = (SELECT codigo 
                            FROM teditoriales 
                            WHERE tlibros.editorial = teditoriales.nombre)

SELECT titulo, editorial
    FROM tlibros

-- modifcamos el campo editorial de la tabla tlibros para que coincida 
-- con el char(4) not null de teditoriales.codigo
ALTER TABLE tlibros
    ALTER COLUMN editorial char(4) not null












