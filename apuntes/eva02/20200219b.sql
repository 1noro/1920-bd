-- # Apuntes SQL Alfredo RG 20200219b
USE BD_biblioteca

-- TRIGGERS
-- se puede cambiar la prioridad en la que se ejecutan, pero solo puedes enviarlos al principio o al final de la lista

-- EJEMPLO (creamos 3 triggers)
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tprimero' AND type = 'TR')
        DROP TRIGGER tprimero
GO

CREATE TRIGGER tprimero
    ON tprueba
    AFTER UPDATE -- || INSTEAD OF
    AS
        PRINT 'Trigger: tprimero'
GO

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tsegundo' AND type = 'TR')
        DROP TRIGGER tsegundo
GO

CREATE TRIGGER tsegundo
    ON tprueba
    AFTER UPDATE -- || INSTEAD OF
    AS
        PRINT 'Trigger: tsegundo'
GO

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tterceroo' AND type = 'TR')
        DROP TRIGGER ttercero
GO

CREATE TRIGGER ttercero
    ON tprueba
    AFTER UPDATE -- || INSTEAD OF
    AS
        PRINT 'Trigger: ttercero'
GO

-- el orden se modifica con: 
sp_settriggerorder ttercero, first, 'update'
GO

sp_settriggerorder tsegundo, last, 'update'
GO

-- ejecutamos el update
UPDATE tprueba
    SET precio = 30.25
    WHERE isbn = '0-261-10320-2'
GO

-- deshabilitar un trigger
ALTER TABLE tprueba DISABLE TRIGGER tsegundo
ALTER TABLE tprueba ENABLE TRIGGER tsegundo
