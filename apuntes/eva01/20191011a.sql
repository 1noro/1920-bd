-- # Apuntes SQL Alfredo RG 20191011a
USE BD_Biblioteca
-- # Funciones matemáticas
SELECT ABS(-89) -- -> 89 (valor absoluto)

SELECT CEILING(256.23)  -- -> 257 (redondeo raro)
SELECT CEILING(-256.23) -- -> 256
SELECT CEILING(256.99)  -- -> 256
SELECT CEILING(-256.99) -- -> 257

SELECT ROUND(-256.99, 1)    -- -> -257.00
SELECT ROUND(-256.6533, 2)  -- -> -256.6500

SELECT POWER(5, 4)  -- -> 625 (elevar 5^4)
SELECT SQUARE(25)   -- -> 625 (elevar 25^2)

SELECT SQRT(25)   -- -> 5.0 (elevar 25^(1/2) - raiz cuadrada)

SELECT SIGN(-625)   -- -> -1 (evalua el signo)
SELECT SIGN(5)      -- -> 1
SELECT SIGN(0)      -- -> 0

SELECT PI() -- -> 3.1415926535897931

SELECT SIN(90)  -- -> 0.89399666360055785 (seno)

SELECT AVG(precio)  -- -> 14.8470 (media aritmética)
    FROM tlibros

-- talumnos(nombre, nota1, nota2, nota3) tabla hipotética
-- media de tres atributos separados
SELECT nombre, (nota1 + nota2 + nota3)/3
    FROM talumnos

-- media global
SELECT AVG((nota1 + nota2 + nota3)/3)
    FROM talumnos

-- precio de todos los libros cuyo precio sea mayor a la media global
SELECT titulo, precio
    FROM tlibros
    WHERE precio > (SELECT AVG(precio) FROM tlibros)



