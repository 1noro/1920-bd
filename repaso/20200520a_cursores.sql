USE bdalumnos
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            PRINT @titulo
            FETCH NEXT FROM c1 INTO @titulo
        END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- Crear un cursor que visualice los dni, nombre y apellidos de todos los alumnos que no hayan alcanzado
-- la nota de cinco en alguna de las tres notas. Y al final nos indique el sexo de los alumnos que 
-- predominen en dicha selección.

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT a.dni, a.nombre, a.apel_1, a.apel_2, a.sexo
    	FROM talumnos a, tnotas n
    	WHERE a.dni = n.dni AND (n.nota1 < 5 OR n.nota2 < 5 OR n.nota2 < 5)

DECLARE @dni char(9)
DECLARE @nombre char(15)
DECLARE @apel_1 char(15)
DECLARE @apel_2 char(15)
DECLARE @sexo   char(1)

DECLARE @cont_m int
DECLARE @cont_v int

SET @cont_m = 0
SET @cont_v = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @dni, @nombre, @apel_1, @apel_2, @sexo
    WHILE @@fetch_status = 0 BEGIN
        PRINT @dni + ' ' + @nombre + ' ' + @apel_1 + ' ' + @apel_2 + ' ' + @sexo
        IF @sexo = 'M' SET @cont_m = @cont_m + 1
        ELSE SET @cont_v = @cont_v + 1
        FETCH NEXT FROM c1 INTO @dni, @nombre, @apel_1, @apel_2, @sexo
    END
CLOSE c1
DEALLOCATE c1

PRINT ''
IF @cont_m > @cont_v PRINT '>> Suspenden más los CHICAS.'
IF @cont_m < @cont_v PRINT '>> Suspenden más las CHICOS.'
IF @cont_m = @cont_v PRINT '>> Suspenden los dos sexos por IGUAL.'


