-- # Apuntes SQL Alfredo RG 20191217a
USE BD_biblioteca

-- #sobre el ejercicio anterior calcular la media por cada provincia
DECLARE ruptura_00 CURSOR
    KEYSET LOCAL
    FOR SELECT l.titulo, l.autor, e.nombre, l.precio, e.codigo_prov
        FROM tlibros AS l, teditoriales AS e
        WHERE l.editorial = e.codigo
        ORDER BY e.codigo_prov

--DECLARE @titulo varchar(50), @vocales int, @i int, @c char
DECLARE
    @titulo char(50),
    @autor char(30),
    @nEditorial char(35),
    @precio money,
    @cprov int,
    @cprovAnt int,
    @sumPrecio money,
    @nProv char(23),
    @cont int
    
SET @sumPrecio = 0
SET @cont = 0

OPEN ruptura_00
    FETCH FIRST FROM ruptura_00 INTO @titulo, @autor, @nEditorial, @precio, @cprov
    SET @cprovAnt = @cprov
    WHILE @@fetch_status = 0
        BEGIN
            IF @cprovAnt != @cprov
                BEGIN
                    -- PUNTO DE RUPTURA
                    SET @nProv = (SELECT nombre FROM tprovincias WHERE codigo = @cprovAnt)
                    PRINT ''
                    PRINT '#Total, ' + rtrim(@nProv) + ': ' + str(@sumPrecio, 7, 2) + ' (AVG: ' + ltrim(str(@sumPrecio / @cont, 7, 2)) + ')'
                    PRINT ''
                    SET @sumPrecio = 0
                    SET @cprovAnt = @cprov
                END
            PRINT rtrim(@titulo) + ', ' + rtrim(@autor) + ', ' + rtrim(@nEditorial) + ', ' + ltrim(str(@precio, 7, 2))
            SET @sumPrecio = @sumPrecio + @precio
            SET @cont = @cont + 1
            FETCH NEXT FROM ruptura_00 INTO @titulo, @autor, @nEditorial, @precio, @cprov
        END

PRINT ''
PRINT '#Total, ' + rtrim(@nProv) + ': ' + str(@sumPrecio, 7, 2)
PRINT ''

CLOSE ruptura_00
DEALLOCATE ruptura_00 -- liberamos la memoria

-- #EX PROX DIA: ver titulo, genero y precio de todos los libros y a l final de cada genero que te diga el número de libros, la suma de los precios y el priecio medio. Y al final el total de temas que tengo, y el tema que mas libros tiene
