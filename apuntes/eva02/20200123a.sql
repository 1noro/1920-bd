-- # Apuntes SQL Alfredo RG 20200123a
USE BD_biblioteca

-- VARIABLES DE TABLA
DECLARE @tabla TABLE(tit varchar(50), precio money)

INSERT INTO @tabla
    SELECT titulo, precio
    FROM tlibros
    WHERE precio > 20

SELECT * FROM @tabla