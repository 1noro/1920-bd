-- # Apuntes SQL Alfredo RG 20200227a
USE BD_biblioteca

-- borramos tprueba
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tprueba' AND type = 'U')
        DROP TABLE tprueba
GO

-- borramos tprueba1
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tprueba1' AND type = 'U')
        DROP TABLE tprueba1
GO

-- creamos tprueba
SELECT * INTO tprueba FROM tlibros
GO

-- añadir a tprueba un atributo de tipo binario prestado, por defecto a 0
ALTER TABLE tprueba ADD prestado bit DEFAULT 0 NOT NULL
GO

-- select * from tprueba

-- trigger que cuente los libros prestados en una tabla y si el numero es 0 se borra la tabla
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 't1' AND type = 'TR')
        DROP TRIGGER t1
GO

-- DROP TABLE contpresta

CREATE TRIGGER t1
    ON tprueba
    AFTER UPDATE -- || INSTEAD OF
    AS
        IF UPDATE(prestado) BEGIN
            DECLARE 
                @cont int,
                @action bit,
                @num int

            SET @cont = 0
            SET @action = (SELECT TOP 1 prestado FROM inserted)
            -- SET @num = @@rowcount
            SET @num = (SELECT COUNT(isbn) FROM inserted)

            IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = 'contpresta' AND type = 'U') BEGIN
                CREATE TABLE contpresta (cont int DEFAULT 0 not null)
                INSERT INTO contpresta VALUES (0)
            END ELSE BEGIN
                SET @cont = (SELECT cont FROM contpresta)
            END

            IF @action = 1 BEGIN
                -- PRINT 'Preststados: ' + str(@num) + ' ' + str(@action)
                UPDATE contpresta SET cont = @cont + @num
            END ELSE BEGIN
                -- PRINT 'Devueltos: ' + str(@num) + ' ' + str(@action)
                UPDATE contpresta SET cont = @cont - @num
            END

            SET @cont = (SELECT cont FROM contpresta)

            -- PRINT 'Al final hay: ' + str(@cont)

            IF @cont <= 0 DROP TABLE contpresta

        END
GO

/*
SELECT * FROM tprueba
GO

SELECT * FROM tprueba WHERE prestado = 1
GO

UPDATE contpresta SET cont = 0
GO

UPDATE tprueba
    SET prestado = 1
    WHERE isbn = '0-261-10320-2'
GO

UPDATE tprueba
    SET prestado = 1
    WHERE isbn = '14-10-82338-0'
GO

UPDATE tprueba
    SET prestado = 1
    WHERE editorial = 'PLAZ'
GO

UPDATE tprueba
    SET prestado = 0
    WHERE isbn = '0-261-10320-2'
GO

UPDATE tprueba
    SET prestado = 0
    WHERE isbn = '14-10-82338-0'
GO

UPDATE tprueba
    SET prestado = 0
    WHERE editorial = 'PLAZ'
GO

SELECT cont FROM contpresta
GO
*/
