-- # Apuntes SQL Alfredo RG 20191014a
USE BD_Biblioteca
-- # Funciones de grupo
SELECT COUNT(*) -- -> 142
    FROM tlibros
SELECT COUNT(titulo) -- -> 142
    FROM tlibros

SELECT MAX(precio) 'libro más caro'
    FROM tlibros
SELECT MIN(precio) 'libro más barato'
    FROM tlibros
SELECT SUM(precio) suma -- si la palabra no tiene espacios se puede poner sin ''
    FROM tlibros

SELECT * 
    FROM tlibros
    WHERE num_pag = (SELECT MIN(num_pag) FROM tlibros)

-- # CARACTERES Y STRINGS
SELECT ASCII('A')  -- -> número en ascii
SELECT CHAR(65) -- -> devuelve la 'A'

SELECT LOWER(titulo), titulo -- -> convertir a minuscula
    FROM tlibros
SELECT UPPER(titulo), titulo -- -> convertir a mayúscula
    FROM tlibros

SELECT LEN('hola') -- -> 4; tamaño de una cadena de caracteres

-- concatenación
SELECT 'col'+' y '+'flor'
SELECT 'col'+space(1)+'y'+space(5)+'flor'