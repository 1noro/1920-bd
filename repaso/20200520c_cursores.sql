USE BDBiblioteca
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0 BEGIN
        PRINT @titulo
        FETCH NEXT FROM c1 INTO @titulo
    END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- cursor que imprima titulo y numero de vocales (acentos y dieresis)
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)
DECLARE @vocales int
DECLARE @i int

SET @vocales = 0
SET @i = 1

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo
    WHILE @@fetch_status = 0 BEGIN
	    WHILE @i <= len(@titulo) BEGIN
		    IF substring(@titulo, @i, 1) LIKE '[aeiouáéíóúü]' 
		    	SET @vocales = @vocales + 1
		    SET @i = @i + 1
	    END
        PRINT '> ' + @titulo +  ' (' + ltrim(str(@vocales, 10, 0)) + ')'
        SET @vocales = 0
        SET @i = 1
        FETCH NEXT FROM c1 INTO @titulo
    END
CLOSE c1
DEALLOCATE c1







