-- # Apuntes SQL Alfredo RG 20191111a
USE BD_biblioteca

DROP TABLE prueba
DROP TABLE tprueba1
DROP TABLE tprueba2

-- Crear 2 tablas:
-- teditoriales()
-- tprovincias()

-- A MI MANERA
IF OBJECT_ID('teditoriales') IS NOT NULL DROP TABLE teditoriales
IF OBJECT_ID('tprovincias') IS NOT NULL DROP TABLE tprovincias

CREATE TABLE tprovincias (
    codigo int primary key,
    nombre varchar(23)
)

CREATE TABLE teditoriales (
    codigo char(4) primary key,
    nombre varchar(35),
    codigo_prov int,
    foreign key (codigo_prov) references tprovincias(codigo)
)

-- OTRAS FORMAS
IF OBJECT_ID('teditoriales') IS NOT NULL DROP TABLE teditoriales
IF OBJECT_ID('tprovincias') IS NOT NULL DROP TABLE tprovincias

CREATE TABLE tprovincias (
    codigo int primary key,
    nombre varchar(23)
)

CREATE TABLE teditoriales (
    codigo char(4) primary key,
    nombre varchar(35),
    codigo_prov int references tprovincias(codigo)
)

-- LLENAMOS LAS TABLAS
SELECT count(DISTINCT editorial) FROM tlibros -- -> 61 editoriales hay en total

-- llenamos provincias
INSERT INTO tprovincias(codigo, nombre) VALUES (15, 'Coruña, A')
INSERT INTO tprovincias(codigo, nombre) VALUES (27, 'Lugo')
INSERT INTO tprovincias(codigo, nombre) VALUES (32, 'Ourense')
INSERT INTO tprovincias(codigo, nombre) VALUES (36, 'Pontevedra')
INSERT INTO tprovincias(codigo, nombre) VALUES (1, 'Alava')
INSERT INTO tprovincias(codigo, nombre) VALUES (2, 'Albacete')
INSERT INTO tprovincias(codigo, nombre) VALUES (28, 'Madrid')
INSERT INTO tprovincias(codigo, nombre) VALUES (8, 'Barcelona')
INSERT INTO tprovincias(codigo, nombre) VALUES (50, 'Zaragoza')

SELECT * FROM tprovincias

-- llenamos teditoriales
SELECT DISTINCT UPPER(left(editorial, 4)), editorial , NULL 
    FROM tlibros
    WHERE UPPER(left(editorial, 4)) <> 'EDIT'

/* -- no funciona
SELECT DISTINCT UPPER(left(editorial, 4)), editorial , NULL 
    INTO teditoriales
    FROM tlibros
*/

INSERT INTO teditoriales(codigo, nombre, codigo_prov)
    SELECT DISTINCT UPPER(left(editorial, 4)), editorial , NULL 
        FROM tlibros
        WHERE UPPER(left(editorial, 4)) <> 'EDIC' -- porque se repiten

INSERT INTO teditoriales(codigo, nombre)
    SELECT DISTINCT UPPER(left(editorial, 4)), editorial 
        FROM tlibros
        WHERE UPPER(left(editorial, 4)) <> 'EDIC'

SELECT * FROM teditoriales

-- EL PRIMER GRUPO DE REPETIDAS ----------------------------------------------------

SELECT DISTINCT editorial, left(upper(editorial), 2) + substring(UPPER(editorial), 11, 2)
    FROM tlibros
    WHERE left(lower(editorial), 9) = 'ediciones'

INSERT INTO teditoriales(codigo, nombre)
    SELECT DISTINCT left(upper(editorial), 2) + substring(UPPER(editorial), 11, 2), editorial
        FROM tlibros
        WHERE left(lower(editorial), 9) = 'ediciones'

SELECT * FROM teditoriales

-- EL SEGUNDO GRUPO DE REPETIDAS ---------------------------------------------------

SELECT DISTINCT editorial, left(upper(editorial), 2) + substring(UPPER(editorial), 10, 2)
    FROM tlibros
    WHERE left(lower(editorial), 9) = 'edicións'

INSERT INTO teditoriales(codigo, nombre)
    SELECT DISTINCT left(upper(editorial), 2) + substring(UPPER(editorial), 10, 2), editorial
        FROM tlibros
        WHERE left(lower(editorial), 9) = 'edicións'

SELECT * FROM teditoriales
































