USE BD_Alumnos
GO

/*
USE BD_Alumnos
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p_backup' AND type = 'P')
    DROP PROCEDURE p_backup
GO

CREATE PROCEDURE p_backup AS
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'talumnos1' AND type = 'U')
        DROP TABLE talumnos1
    SELECT * INTO talumnos1 FROM talumnos
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'talumnos2' AND type = 'U')
        DROP TABLE talumnos2
    SELECT * INTO talumnos2 FROM talumnos
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tnotas1' AND type = 'U')
        DROP TABLE tnotas1
    SELECT * INTO tnotas1 FROM tnotas
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tciclos1' AND type = 'U')
        DROP TABLE tciclos1
    SELECT * INTO tciclos1 FROM tciclos
GO

EXEC p_backup
GO
*/

-- SELECT * INTO tnotas1 FROM tnotas
-- Hacer un procedure que devuelva como variable de salida el dni del alumno con mayor media en tnotas1
-- Hacer un trigger que cuando borres una tupla en tnotas1 no borre esa tupla, sino la tupla con el dni que devuelve el procedure anterior.

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'procedure1' AND type = 'P')
    DROP PROCEDURE procedure1
GO

CREATE PROCEDURE procedure1 @dni char(9) OUTPUT AS
    SET @dni = (SELECT TOP 1 dni FROM tnotas1 ORDER BY (nota1 + nota2 + nota3) / 3 DESC)
GO

/*
DECLARE @miDni char(9)
EXEC procedure1 @miDni OUTPUT
SELECT @miDni
*/

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'trigger1' AND type = 'TR')
    DROP TRIGGER trigger1
GO

CREATE TRIGGER trigger1
    ON tnotas1
    INSTEAD OF DELETE
    AS BEGIN
        DECLARE @dni char(9)
        EXEC procedure1 @dni OUTPUT
        DELETE FROM tnotas1 WHERE dni = @dni
    END
GO

/*
DELETE FROM tnotas1 WHERE dni = '66666666l'
*/

/*
SELECT * FROM tnotas1
*/