USE BD_Alumnos
GO

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO
/*
 * Crear un cursor que nos muestre el nombre del ciclo, curso, número, nombre, primer apellido,
segundo apellido, la nota media y el nombre literal de dicha nota, de todos los alumnos,
ordenados por ciclo, curso y número. (los literales serán los siguientes ‘MD’ para las notas
inferiores a 2.5; ‘IN’ para las notas inferiores a 4.75; ‘SF’ para las inferiores a 6; ‘B’ para las
inferiores a 7; ‘NT’ para las inferiores a 8,5 y ‘SB’ para el resto ).
 */

DECLARE cursor1 CURSOR
    KEYSET LOCAL
    FOR SELECT 
    		c.nombre AS nombre_ciclo,
    		n.curso,
    		n.numero,
    		a.nombre AS nombre_alumno,
    		a.ape1,
    		a.ape2,
    		(n.nota1 + n.nota2 + n.nota3) / 3 AS media
    	FROM tnotas AS n, tciclos AS c, talumnos AS a
    	WHERE n.dni = a.dni AND n.ciclo = c.ciclo
    	ORDER BY c.ciclo, n.curso, n.numero

DECLARE @nombre_ciclo char(28)
DECLARE @curso int
DECLARE @numero int
DECLARE @nombre_alumno char(15)
DECLARE @ape1 char(15)
DECLARE @ape2 char(15)
DECLARE @media numeric(5, 2)

DECLARE @media_str char(2)
SET @media_str = '??'

OPEN cursor1
    FETCH FIRST FROM cursor1 INTO @nombre_ciclo, @curso, @numero, @nombre_alumno, @ape1, @ape2, @media
    WHILE @@fetch_status = 0 BEGIN
	    IF @media < 2.50 SET @media_str = 'MD'
	    ELSE IF @media < 4.75 SET @media_str = 'IN'
	    	ELSE IF @media < 6.00 SET @media_str = 'SF'
	    		ELSE IF @media < 7.00 SET @media_str = 'B '
	    			ELSE IF @media < 8.50 SET @media_str = 'NT'
	    				ELSE SET @media_str = 'SB'
        PRINT @nombre_ciclo + ' ' + str(@curso, 7, 0) + ' ' + str(@numero, 7, 0) + ' ' + @nombre_alumno + ' ' + @ape1 + ' ' + @ape2 + ' ' + str(@media, 7, 2) + ' ' + @media_str
        FETCH NEXT FROM cursor1 INTO @nombre_ciclo, @curso, @numero, @nombre_alumno, @ape1, @ape2, @media
    END
CLOSE cursor1
DEALLOCATE cursor1



