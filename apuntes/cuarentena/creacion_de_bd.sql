-- ultimo tema, creacion de bases de datos
-- clases: 20200416, 20200421, 20200423

USE master
GO

-- BASE DE DATOS DE PRUEBA
/*
CREATE DATABASE prueba
    ON PRIMARY (
        name = prueba,
        filename = 'C:\bd\prueba.mdf',
        size = 8MB,
        maxsize = 50MB,
        filegrowth = 3% -- 5MB también se puede
    )
GO

ALTER DATABASE prueba
    MODIFY FILE (
        name = prueba,
        size = 11MB,
        maxsize = 60MB,
        filegrowth = 5MB
    )
GO

DROP DATABASE prueba
GO
*/

-- 20200421 continuamois creando la bd

-- BASE DE DATOS DE MUEBLES
USE master
GO

IF EXISTS (SELECT name FROM sys.databases WHERE name = 'bdmuebles')
    DROP DATABASE bdmuebles
GO

CREATE DATABASE bdmuebles
    ON PRIMARY (
        name = bdmuebles,
        filename = 'C:\bd\bdmuebles.mdf',
        size = 20MB,
        maxsize = 300MB,
        filegrowth = 15%
    )
GO

USE bdmuebles
GO

-- BORRAMOS LAS TABLAS EN ORDEN INVERSO
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'tmuebles' AND type = 'U')
    DROP TABLE tmuebles
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'tproveed' AND type = 'U')
    DROP TABLE tproveed
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'tfamilias' AND type = 'U')
    DROP TABLE tfamilias
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'tprovincias' AND type = 'U')
    DROP TABLE tprovincias
GO

CREATE TABLE tprovincias (
    codigo int NOT NULL, -- 4
    nombre varchar(23),
    PRIMARY KEY (codigo)
)
GO

CREATE TABLE tfamilias (
    codigo char(5) NOT NULL,
    nombre varchar(15),
    PRIMARY KEY (codigo)
)
GO

CREATE TABLE tproveed (
    codigo char(4) NOT NULL,
    nombre varchar(30),
    cod_prov int NOT NULL, -- 4
    FOREIGN KEY (cod_prov) REFERENCES tprovincias(codigo),
    PRIMARY KEY (codigo)
)
GO

CREATE TABLE tmuebles (
    codigo char(10) NOT NULL,
    familia char(5) NOT NULL,
    denominacion varchar(20) NOT NULL,
    cod_proveedor char(4) NOT NULL,
    tipo char(1) NOT NULL, -- pomer un check con el alter, mas tarde
    stock_max decimal(9, 2) NOT NULL, -- 5
    stock_min decimal(9, 2) NOT NULL, -- 5
    stock_actual decimal(9, 2) NOT NULL, -- 5
    precio money, -- default: 8
    aviso char(1) NOT NULL DEFAULT 'N',
    FOREIGN KEY (familia) REFERENCES tfamilias(codigo),
    FOREIGN KEY (cod_proveedor) REFERENCES tproveed(codigo),
    CONSTRAINT clave_primaria PRIMARY KEY (codigo, familia) -- constraint es para poner un nombre a la clave en conjunto. Luego podemos referirnos a este nombre
)
GO

/*SELECT * FROM sysobjects WHERE name = 'tmuebles'
GO*/

-- llenar las tablas
INSERT INTO tprovincias VALUES (15, 'Coruña, A');
INSERT INTO tprovincias VALUES (27, 'Lugo');
INSERT INTO tprovincias VALUES (32, 'Ourense');
INSERT INTO tprovincias VALUES (36, 'Pontevedra');
GO

/*SELECT * FROM tprovincias
GO*/

INSERT INTO tproveed VALUES ('ARJ1', 'Muebles de Galicia S.A.', 27);
INSERT INTO tproveed VALUES ('BVC1', 'Luz Galicia', 27);
INSERT INTO tproveed VALUES ('BWC1', 'Estilo hogar S.L.', 36);
INSERT INTO tproveed VALUES ('GJK8', 'Muebles Artesanos S.A.', 15);
INSERT INTO tproveed VALUES ('GRQ2', 'Decoración Orensana S.L.', 32);
INSERT INTO tproveed VALUES ('MNB3', 'Muebles Noroeste', 15);
GO

/*SELECT * FROM tproveed
GO*/

INSERT INTO tfamilias VALUES ('ASEOS', 'Baño');
INSERT INTO tfamilias VALUES ('COCIN', 'Cocina');
INSERT INTO tfamilias VALUES ('COMP', 'Complementos');
INSERT INTO tfamilias VALUES ('DORMI', 'Dormitorio');
INSERT INTO tfamilias VALUES ('SALON', 'Salón');
GO

/*SELECT * FROM tfamilias
GO*/

INSERT INTO tmuebles VALUES ('ERTY23',  'DORMI',    'Cama nido',            'GJK8', 'D', 30,    12,  1,    300.00,  'B');
INSERT INTO tmuebles VALUES ('FE2345',  'COCIN',    'Alacena',              'ARJ1', 'A', 25,     5,  1,  20025.00,  'B');
INSERT INTO tmuebles VALUES ('FE9876',  'COCIN',    'Encimera',             'GRQ2', 'A', 20,     3, 10,    178.20,  'N');
INSERT INTO tmuebles VALUES ('GHR4',    'SALON',    'Mesa Centro Baja',     'GRQ2', 'B', 15,     2, 22,  12532.00,  'A');
INSERT INTO tmuebles VALUES ('GDT23',   'SALON',    'Silla napolitana',     'ARJ1', 'C', 75,     5,  5,    100.00,  'B');
INSERT INTO tmuebles VALUES ('JRT4',    'DORMI',    'Armario tres cuerpos', 'MNB3', 'C', 10,     1, 10,    400.00,  'A');
INSERT INTO tmuebles VALUES ('LMPS5J',  'COMP',     'Lámpara mesa',         'BVC1', 'F', 12,     3,  7,      4.75,  'N');
INSERT INTO tmuebles VALUES ('PLK7',    'DORMI',    'Cama matrimonio. 2m',  'MNB3', 'C',  9,     3,  9,  59236.00,  'A');
INSERT INTO tmuebles VALUES ('POQ4',    'SALON',    'Mesa comedor ovalada', 'GRQ2', 'B',  8,     2,  5,    609.25,  'N');
INSERT INTO tmuebles VALUES ('POU55',   'COMP',     'Aplique pared oval',   'GJK8', 'F', 40,    10,  7,    158.36,  'B');
INSERT INTO tmuebles VALUES ('RTH12',   'COMP',     'Pomo Cisne',           'BVC1', 'F', 60,    10,  7,     82.95,  'B');
INSERT INTO tmuebles VALUES ('SLLAT5',  'SALON',    'Silla Venecia',        'ARJ1', 'C', 30,     8,  3,     62.50,  'B');
GO

/*SELECT * FROM tmuebles
GO*/

-- EX1:
SELECT denominacion, f.nombre nombre_familia, prove.nombre nombre_proveedor, provi.nombre nombre_provincia
    FROM tmuebles m, tfamilias f, tproveed prove, tprovincias provi
    WHERE
        m.familia = f.codigo AND
        m.cod_proveedor = prove.codigo AND
        prove.cod_prov = provi.codigo
GO

-- EX2: convertir la select anterior en una vista
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'vista1' AND type = 'V')
    DROP VIEW vista1
GO

CREATE VIEW vista1 AS -- hay que poner columnas con diferentes alias para que no haya conflicto
    SELECT denominacion, f.nombre nombre_familia, prove.nombre nombre_proveedor, provi.nombre nombre_provincia
        FROM tmuebles m, tfamilias f, tproveed prove, tprovincias provi
        WHERE
            m.familia = f.codigo AND
            m.cod_proveedor = prove.codigo AND
            prove.cod_prov = provi.codigo
GO

/*
SELECT * FROM vista1
GO
*/

-- nombre de prooveedor y el numero de muebles
-- cual es el que tiene mas articulos?
-- si 2 coinciden mostrarlos los 2
SELECT TOP 1 WITH TIES p.nombre nombre_proveedor, COUNT(m.codigo)
    FROM tproveed p, tmuebles m
        WHERE p.codigo = m.cod_proveedor
        GROUP BY p.nombre
        ORDER BY COUNT(m.codigo) DESC
GO


-- nombre proveed y el valor del estoc (precio*stock actual) pero solo de los
-- proveed que tengan entre 1000 y 300000
SELECT p.nombre nombre_proveedor, SUM(m.precio * m.stock_actual)
    FROM tproveed p, tmuebles m
        WHERE p.codigo = m.cod_proveedor
        GROUP BY p.nombre
        HAVING SUM(m.precio * m.stock_actual) BETWEEN 1000 AND 300000
GO

-- @ LO mismo pero solo para los articulos de tipo C o tipo F
SELECT p.nombre nombre_proveedor, SUM(m.precio * m.stock_actual)
    FROM tproveed p, tmuebles m
        WHERE p.codigo = m.cod_proveedor AND (m.tipo = 'C' OR m.tipo = 'F')
        GROUP BY p.nombre
        HAVING SUM(m.precio * m.stock_actual) BETWEEN 1000 AND 300000
GO

USE master
GO
