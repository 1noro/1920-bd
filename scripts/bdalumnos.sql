USE master
GO

-- DATABSE --------------------------------------------------------------
IF EXISTS (SELECT name FROM sys.databases WHERE name = 'bdalumnos')
    DROP DATABASE bdalumnos
GO

CREATE DATABASE bdalumnos
GO

-- TABLES ---------------------------------------------------------------
USE bdalumnos
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'tnotas' AND type = 'U')
    DROP TABLE tnotas
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'tciclos' AND type = 'U')
    DROP TABLE tciclos
GO

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'talumnos' AND type = 'U')
    DROP TABLE talumnos
GO

CREATE TABLE talumnos (
	dni varchar(9),
	apel_1 varchar(15) NOT NULL,
	apel_2 varchar(15) NOT NULL,
	nombre varchar(15),
	fec_nac datetime,
	fec_mat datetime DEFAULT getdate(),
	PRIMARY KEY (dni)
)

CREATE TABLE tciclos (
	ciclo char(3),
	nombre varchar(28),
	PRIMARY KEY (ciclo)
)

CREATE TABLE tnotas (
	ciclo char(3),
	curso int,
	numero int,
	dni varchar(9),
	nota1 numeric(6, 2),
	nota2 numeric(6, 2),
	nota3 numeric(6, 2),
	FOREIGN KEY (ciclo) REFERENCES tciclos(ciclo),
	FOREIGN KEY (dni) REFERENCES talumnos(dni)
)

-- INSERTS ---------------------------------------------------------------
USE bdalumnos
GO

INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('11111111A', 'Gómez', 'López', 'Juan', '12/10/1980', '05/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('11111112M', 'Muñiz', 'Grandio', 'Jacobo', '08/03/1986', '06/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('11111113N', 'Jato', 'Rovira', 'Luis', '05/04/1985', '04/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('11111114O', 'Lobato', 'Mazaricos', 'Beatriz', '27/10/1983', '03/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('11111115P', 'Geremías', 'Rodríguez', 'Laura', '26/12/1984', '01/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('22222222B', 'Santiso', 'Fernández', 'Sara', '05/06/1984', '07/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('33333333C', 'García', 'Boado', 'Luis', '06/04/1982', '01/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('44444444D', 'Mosquera', 'Serantes', 'María', '05/05/1986', '15/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('55555555H', 'López', 'López', 'Oscar', '12/08/1981', '05/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('66666666I', 'Médez', 'Losada', 'Pilar', '07/01/1982', '02/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('77777777J', 'Basante', 'Argüelles', 'Francisco', '30/08/1984', '14/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('88888888K', 'Sandá', 'Pita', 'José', '25/11/1981', '11/09/2006')
INSERT INTO talumnos(dni, apel_1, apel_2, nombre, fec_nac, fec_mat)
	VALUES ('99999999L', 'Penedo', 'Gutierrez', 'Ana', '07/08/1985', '06/09/2006')
GO

INSERT INTO tciclos(ciclo, nombre)
	VALUES ('AyF', 'Administración y Finanzas')
INSERT INTO tciclos(ciclo, nombre)
	VALUES ('Dai', 'Desarrollo de apl. Informát.')
INSERT INTO tciclos(ciclo, nombre)
	VALUES ('Sec', 'Seretariado')
GO

INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('AyF', 1, 1, '66666666I', 7.0, 2.0, 7.0)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('AyF', 1, 2, '77777777J', 8.5, 9.2, 6.8)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('AyF', 2, 1, '88888888K', 4.5, 6.0, 2.5)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Dai', 1, 1, '11111111A', 9.0, 8.7, 9.5)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Dai', 1, 2, '22222222B', 6.0, 5.6, 10.0)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Dai', 1, 3, '33333333C', 5.8, 7.6, 9.2)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Dai', 2, 1, '44444444D', 9.0, 8.7, 9.5)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Dai', 2, 2, '55555555H', 6.0, 4.0, 4.0)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Sec', 1, 2, '99999999L', 6.0, 3.6, 8.1)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Sec', 1, 2, '11111112M', 7.5, 8.2, 4.8)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Sec', 1, 3, '11111113N', 8.0, 2.45, 10.0)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Sec', 1, 4, '11111114O', 5.0, 6.7, 5.5)
INSERT INTO tnotas(ciclo, curso, numero, dni, nota1, nota2, nota3)
	VALUES ('Sec', 1, 5, '11111115P', 8.7, 6.0, 5.5)
GO

-- ALTERACIONES -----------------------------------------------------------------
ALTER TABLE talumnos ADD sexo char CHECK(sexo = 'V' OR sexo = 'M')
GO

UPDATE talumnos SET sexo = 'V' WHERE dni = '11111111A'
UPDATE talumnos SET sexo = 'V' WHERE dni = '11111112M'
UPDATE talumnos SET sexo = 'V' WHERE dni = '11111113N'
UPDATE talumnos SET sexo = 'M' WHERE dni = '11111114O'
UPDATE talumnos SET sexo = 'M' WHERE dni = '11111115P'
UPDATE talumnos SET sexo = 'M' WHERE dni = '22222222B'
UPDATE talumnos SET sexo = 'V' WHERE dni = '33333333C'
UPDATE talumnos SET sexo = 'M' WHERE dni = '44444444D'
UPDATE talumnos SET sexo = 'V' WHERE dni = '55555555H'
UPDATE talumnos SET sexo = 'M' WHERE dni = '66666666I'
UPDATE talumnos SET sexo = 'V' WHERE dni = '77777777J'
UPDATE talumnos SET sexo = 'V' WHERE dni = '88888888K'
UPDATE talumnos SET sexo = 'M' WHERE dni = '99999999L'
GO

-- COMPROBACIONES ---------------------------------------------------------------
/*
USE bdalumnos
GO

SELECT * FROM talumnos
GO

SELECT * FROM tciclos
GO

SELECT * FROM tnotas
GO
*/
