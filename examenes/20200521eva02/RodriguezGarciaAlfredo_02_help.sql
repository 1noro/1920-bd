-- ALFREDO RODRIGUEZ GARCIA 1DAM 20200522
USE BD_Alumnos
GO

/*
SELECT * FROM talumnos
GO
SELECT * FROM tnotas
GO
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO 1

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'procedure1' AND type = 'P')
    DROP PROCEDURE procedure1
GO

CREATE PROCEDURE procedure1 @nota numeric(5, 2) AS
    SELECT a.nombre, a.ape1, a.ape2, (n.nota1 + n.nota2 + n.nota3) / 3 AS media, c.nombre
        FROM tnotas AS n, talumnos AS a, tciclos AS c
        WHERE n.dni = a.dni AND n.ciclo = c.ciclo AND (n.nota1 + n.nota2 + n.nota3) / 3 > @nota
    PRINT 'Alumnos listados: ' + str(@@ROWCOUNT, 7, 0)
GO

/*
-- EJECUCIÓN DE MUESTRA
EXEC procedure1 5.00
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO 2

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'procedure2' AND type = 'P')
    DROP PROCEDURE procedure2
GO

CREATE PROCEDURE procedure2 @dni char(9), @media numeric(5, 2) OUTPUT AS
    SELECT @media = (nota1 + nota2 + nota3) / 3 FROM tnotas WHERE dni = @dni
GO

/* 
-- EJECUCIÓN DE MUESTRA
DECLARE @media_out numeric(5, 2)
EXEC procedure2 '11111111A', @media_out OUTPUT
PRINT 'Media: ' + str(@media_out, 7, 2)
*/


PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO 3

/*
DROP TABLE thistorico
*/

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'historico' AND type = 'TR')
    DROP TRIGGER historico
GO

CREATE TRIGGER historico
    ON tnotas
    AFTER UPDATE
    AS BEGIN
        IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = 'thistorico' AND type = 'U') BEGIN
            CREATE TABLE thistorico (
                ciclo char(3),
                curso int,
                numero int,
                dni char(9),
                nota1 numeric(5, 2),
                nota2 numeric(5, 2),
                nota3 numeric(5, 2),
                fech_mod datetime
            )
        END
        INSERT INTO thistorico SELECT *, getdate() FROM DELETED
    END
GO

/*
-- EJECUCIÓN DE MUESTRA
-- 66666666l nota1: 7.00
SELECT * FROM tnotas
GO

UPDATE tnotas
    SET nota1 = 7.50
    WHERE dni = '66666666l'
GO

SELECT * FROM thistorico
GO

UPDATE tnotas
    SET nota1 = 7.00
    WHERE dni = '66666666l'
GO

SELECT * FROM thistorico
GO
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO 4
ALTER TABLE talumnos ADD media numeric(5, 2) DEFAULT 0
GO

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO 5
/*
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'actualizarMedia' AND type = 'TR')
    DROP TRIGGER actualizarMedia
GO

CREATE TRIGGER actualizarMedia
    ON tnotas
    AFTER UPDATE
    AS BEGIN
        IF UPDATE(nota1) OR UPDATE(nota2) OR UPDATE(nota3) BEGIN
            DECLARE @media_out numeric(5, 2)
            DECLARE @dni char(9)
            SET @dni = (SELECT dni FROM INSERTED)
            EXEC procedure2 @dni, @media_out OUTPUT
            UPDATE talumnos
                SET media = @media_out
                WHERE dni = @dni
        END
    END
GO
*/

if exists (select name from sysobjects where name = 'actualizarMedia' and type = 'TR')
    drop trigger actualizarMedia
go

create trigger actualizarMedia on tnotas after update as
begin
    declare @dni char(13)
    set @dni=(select dni from inserted)
    declare @mediaout numeric(5,2)
    exec procedure2 @dni,@mediaout output
    update talumnos set media = @mediaout  where dni=@dni
end
go

/*
-- EJECUCIÓN DE MUESTRA
-- 66666666l nota1: 7.00, nota2: 2.00
SELECT dni, nota1, nota2, nota3, (nota1 + nota2 + nota3) / 3 AS media FROM tnotas
GO

UPDATE tnotas
    SET nota2 = 9.50
    WHERE dni = '66666666l'
GO

SELECT dni, media FROM talumnos
GO

UPDATE tnotas
    SET nota2 = 2.00
    WHERE dni = '66666666l'
GO

SELECT dni, media FROM talumnos
GO
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO 6
/*
sp_settriggerorder actualizarMedia, first, 'update'
GO

sp_settriggerorder historico, last, 'update'
GO
*/

-- DYANGO

if exists (select name from sysobjects where name = 'p2' and type = 'P')
        drop procedure p2
go

create procedure p2 @dni char(9), @media numeric(5,2) output AS
    select @media=(nota1 + nota2 + nota3) / 3 
    from tnotas where dni = @dni
go

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'actualizarMedia' AND type = 'TR')
    DROP TRIGGER actualizarMedia
GO

CREATE TRIGGER actualizarMedia
    ON tnotas
    AFTER UPDATE
    AS BEGIN
        IF UPDATE(nota1) OR UPDATE(nota2) OR UPDATE(nota3) BEGIN
            DECLARE @media numeric(5, 2)
            DECLARE @dni char(9)
            SET @dni = (SELECT dni FROM INSERTED)
            EXEC p2 @dni, @media OUTPUT
            UPDATE talumnos
                SET media = @media
                WHERE dni = @dni
        END
    END
GO

-- FRAN

if exists (select name from sysobjects where name = 'ejercicio2' and type = 'P')
    drop procedure ejercicio2
go
create procedure ejercicio2
@dni char(9),
@media numeric(5, 2) output
as
select @media = (nota1+nota2+nota3)/3 from tnotas
    where dni = @dni
go
declare @mimedia numeric(5, 2)
exec ejercicio2 '11111111A', @mimedia output
print 'La media es ' + str(@mimedia, 7, 2)

-- FRAN 3
if exists (select name from sysobjects where name = 'ejercicio3' AND type ='TR')
    drop trigger ejercicio3
go
create trigger ejercicio3
    on tnotas
    after update
as
    if not exists(select name from sysobjects where name ='thistorico' AND type ='U')
    begin
    create table thistorico
    (
        ciclo char(3),
        curso int,
        numero int,
        dni varchar(9),
        nota1 numeric (6,2),
        nota2 numeric (6,2),
        nota3 numeric (6,2),
        fech_mod datetime
    )
    end
    insert into thistorico(ciclo,curso,numero,dni,nota1,nota2,nota3,fech_mod)
        select ciclo,curso,numero,dni,nota1,nota2,nota3,getdate()from inserted

-- AI

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p1' AND type = 'P')
    DROP PROCEDURE p1
GO

CREATE PROCEDURE p1 @dni char(9), @media numeric(5, 2) OUTPUT AS
    SELECT @media = (nota1 + nota2 + nota3) / 3 FROM tnotas WHERE dni = @dni
GO

if exists (select name from sysobjects where name = 'actualizarMedia' and type = 'TR')
    drop trigger actualizarMedia
go

create trigger actualizarMedia
on tnotas
after update
as
begin
        if update(nota1) or update(nota2) or update(nota3) 
    begin
        declare @mediam numeric(5, 2)
        declare @dni char(9)
                set @dni = (select dni from inserted)
        exec p1 @dni, @mediam output
        update talumnos
        set media = @mediam where dni = @dni
    end
end
go

/*
-- EJECUCIÓN DE MUESTRA
-- 66666666l nota1: 7.00, nota2: 2.00
SELECT dni, nota1, nota2, nota3, (nota1 + nota2 + nota3) / 3 AS media FROM tnotas
GO

UPDATE tnotas
    SET nota2 = 9.50
    WHERE dni = '66666666l'
GO

SELECT dni, media FROM talumnos
GO

UPDATE tnotas
    SET nota2 = 2.00
    WHERE dni = '66666666l'
GO

SELECT dni, media FROM talumnos
GO
*/