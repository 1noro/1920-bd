-- # Apuntes SQL Alfredo RG 20191024a
USE BD_biblioteca

-- # TEMA 6 (MODIFICAR)
-- ## SELECT INTO
-- DROP TABLE tcopialib -- IF EXISTS
SELECT * INTO tcopialib -- -> Guarda el resultado del select en tcopialib (toda la tabla)
    FROM tlibros

SELECT * FROM tcopialib

-- ## INSERT
INSERT INTO tcopialib(
    isbn, titulo, autor, genero, encuadernacion, 
    editorial, fech_ed, num_ed, num_pag, precio)
    VALUES (
        '978-84-283-3857-8', 'Aprende a programar con java', 
        'Alfonso Jimenez Marín y Fco.M.P.M.', 'Didactico', 
        'Rústica', 'Paraninfo', '12-04-2016', 2, 386, 27)
-- da error porque el isbn, y/o otros atributos son mas largos de los permitidos en la tabla
INSERT INTO tcopialib(
    isbn, titulo, autor, genero, encuadernacion, 
    editorial, fech_ed, num_ed, num_pag, precio)
    VALUES (
        '84-283-3857-8', 'Aprende a programar con java', 
        'Alfonso Jimenez Marín y Fco.', 'Didactico', 
        'Rústica', 'Paraninfo', '12-04-2016', 2, 386, 27)
SELECT * FROM tcopialib
    WHERE editorial = 'Paraninfo'

/*DELETE FROM tcopialib
    WHERE editorial = 'Paraninfo'*/

INSERT INTO tcopialib
    VALUES ('84-16288-95-3', 'El honor de Dios', 
            'Lidia Falcó O`Neill', 'Narrativa', 
            'Rústica', 'El viejo topo', '3-04/2016', 1, 556, 26)

SELECT * FROM tcopialib
    WHERE isbn = '84-16288-95-3'

-- ###EX: libros que estan en la segunda tabla (tcopialib) y no estan en la primera (tlibros)
SELECT * FROM tcopialib
    WHERE isbn NOT IN (SELECT isbn FROM tlibros)

-- ###EX: copiar los 2 libros que faltan en tlibros desde tcopialib
INSERT INTO tlibros 
    SELECT * FROM tcopialib
        WHERE isbn NOT IN (SELECT isbn FROM tlibros)

-- ## UPDATE
SELECT genero 
    FROM tcopialib
    WHERE left(genero, 2) = 'Po' 

SELECT DISTINCT genero -- -> 'Poesia', 'Poesía'
    FROM tcopialib
    WHERE left(genero, 2) = 'Po' 

UPDATE tcopialib 
    SET genero = 'Poesía'
    WHERE genero = 'Poesia'

SELECT DISTINCT genero -- -> 'Poesía'
    FROM tcopialib
    WHERE left(genero, 2) = 'Po' 

-- lo aplicamos a tlibros
UPDATE tlibros 
    SET genero = 'Poesía'
    WHERE genero = 'Poesia'

-- modificar mas de un atributo al mismo tiempo
SELECT * FROM tcopialib WHERE isbn = '0-261-10320-2' -- -> 'Sin título'

UPDATE tcopialib
    SET titulo = 'Ya tengo título',
        precio = 45
    WHERE isbn = '0-261-10320-2'

SELECT * FROM tcopialib WHERE isbn = '0-261-10320-2' -- -> 'Ya tengo título'

-- DELETE FROM tlibros -- -> BORRA TODOS LOS DATOS DE UNA TABLA

-- ## DELETE
-- no lo guardé (pero tampoco era muy complicado)

DELETE FROM tcopialib
DROP TABLE tcopialib








