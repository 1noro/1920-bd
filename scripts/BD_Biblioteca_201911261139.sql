if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK__teditoria__codig__0DCF0841]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[teditoriales] DROP CONSTRAINT FK__teditoria__codig__0DCF0841
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vista1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vista1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vista2]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vista2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[teditoriales]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[teditoriales]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tlibros]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tlibros]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tprovincias]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tprovincias]
GO

CREATE TABLE [dbo].[teditoriales] (
	[codigo] [char] (4) COLLATE Modern_Spanish_CI_AS NOT NULL ,
	[nombre] [varchar] (35) COLLATE Modern_Spanish_CI_AS NULL ,
	[codigo_prov] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tlibros] (
	[isbn] [char] (13) COLLATE Modern_Spanish_CI_AS NOT NULL ,
	[titulo] [varchar] (50) COLLATE Modern_Spanish_CI_AS NOT NULL ,
	[autor] [varchar] (30) COLLATE Modern_Spanish_CI_AS NOT NULL ,
	[genero] [varchar] (15) COLLATE Modern_Spanish_CI_AS NOT NULL ,
	[encuadernacion] [nvarchar] (15) COLLATE Modern_Spanish_CI_AS NOT NULL ,
	[editorial] [char] (4) COLLATE Modern_Spanish_CI_AS NOT NULL ,
	[fech_ed] [datetime] NOT NULL ,
	[num_ed] [numeric](10, 0) NOT NULL ,
	[num_pag] [numeric](18, 0) NOT NULL ,
	[precio] [money] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tprovincias] (
	[codigo] [int] NOT NULL ,
	[nombre] [varchar] (23) COLLATE Modern_Spanish_CI_AS NULL 
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW vista1
    AS SELECT titulo, autor, editorial, precio 
            FROM tlibros
            WHERE precio > 25

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW vista2 (titulo, autor, nombre_ed, nombre_pro, precio)
    AS SELECT l.titulo, l.autor, e.nombre, p.nombre, l.precio
        FROM tlibros as l, teditoriales as e, tprovincias as p
        WHERE l.editorial = e.codigo AND e.codigo_prov = p.codigo

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

