-- # Apuntes SQL Alfredo RG 20200305a
USE BD_biblioteca

-- listado de los libros agrupados por autor y que despues ponga el nombre del autor, el numero de libros y el total de precio de los libros del autor

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, precio, autor
            FROM tlibros
            ORDER BY autor

DECLARE 
    @tit char(50),
    @prec money,
    @aut char(30),
    @autAnt char(30),
    @cont int,
    @suma int

SET @cont = 0
SET @suma = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @tit, @prec, @aut
    SET @autAnt = @aut
    WHILE @@fetch_status = 0 BEGIN
        IF (@autAnt != @aut) BEGIN
            PRINT ''
            PRINT ' # ' + rtrim(@autAnt) + ' (' + ltrim(str(@cont, 7, 0)) + ' libros) ' + ltrim(str(@suma, 7, 2)) + ' euros'
            PRINT ''
            SET @autAnt = @aut
            SET @cont = 0
            SET @suma = 0
        END
        PRINT ' - ' + @tit
        SET @cont = @cont + 1
        SET @suma = @suma + @prec
        FETCH NEXT FROM c1 INTO @tit, @prec, @aut
    END

CLOSE c1
DEALLOCATE c1

PRINT ''
PRINT ' # ' + rtrim(@autAnt) + ' (' + ltrim(str(@cont, 7, 0)) + ' libros) ' + ltrim(str(@suma, 7, 2)) + ' euros'
