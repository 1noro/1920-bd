-- Alfredo Rodríguez García 20200428a
USE bdmuebles
GO

-- nombre proveed y el valor del estoc (precio*stock actual) pero solo de los
-- proveed que tengan entre 1000 y 300000
-- pero solo para los articulos de tipo C o tipo F
/*
SELECT p.nombre nombre_proveedor, SUM(m.precio * m.stock_actual)
    FROM tproveed p, tmuebles m
        WHERE p.codigo = m.cod_proveedor AND (m.tipo = 'C' OR m.tipo = 'F')
        GROUP BY p.nombre
        HAVING SUM(m.precio * m.stock_actual) BETWEEN 1000 AND 300000
GO
*/

-- cursor que me de lo mismo que esta ultima select pero visualizando los
-- articulos de cada grupo (denom, tipo y valor stock (stock*precio))
-- SIMPLIFICACIÓN POSTERIOR DE EMILIO, QUE NO PIENSA LAS COSAS: Que muestre 
-- todos los proveedores
-- CURSOR
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT p.nombre nombre_proveedor, m.denominacion, m.tipo, m.stock_actual, m.precio
            FROM tproveed p, tmuebles m
            WHERE p.codigo = m.cod_proveedor AND (m.tipo = 'C' OR m.tipo = 'F')
            ORDER BY p.nombre

-- DECLARACIÓN DE VARIABLES
DECLARE
    @nproveed char(30),
    @nproveedAnt char(30),
    @denom char(20),
    @tipo char(1),
    @stock decimal(9, 2),
    @precio money,
    @suma money

-- VALORES INICIALES
SET @suma = 0

-- MECANICA DEL CURSOR
PRINT '########################################################################'
OPEN c1
    FETCH FIRST FROM c1 INTO @nproveed, @denom, @tipo, @stock, @precio
    SET @nproveedAnt = @nproveed
    WHILE @@fetch_status = 0
        BEGIN
            IF @nproveed != @nproveedAnt BEGIN
                PRINT ''
                IF (@suma >= 1000 AND @suma <= 300000)
                    PRINT ' # ' + RTRIM(@nproveedAnt) + ' ' + LTRIM(str(@suma, 12, 2)) + ' [SIP]'
                ELSE 
                    PRINT ' # ' + RTRIM(@nproveedAnt) + ' ' + LTRIM(str(@suma, 12, 2)) + ' [NOP]'
                PRINT ''
                SET @suma = 0
                SET @nproveedAnt = @nproveed
            END
            PRINT ' ' + @denom + ' ' + @tipo + ' ' + str((@stock * @precio), 12, 2)
            SET @suma = @suma + (@stock * @precio)
            FETCH NEXT FROM c1 INTO @nproveed, @denom, @tipo, @stock, @precio
        END

PRINT ''
IF (@suma >= 1000 AND @suma <= 300000)
    PRINT ' # ' + RTRIM(@nproveedAnt) + ' ' + LTRIM(str(@suma, 12, 2)) + ' [SIP]'
ELSE 
    PRINT ' # ' + RTRIM(@nproveedAnt) + ' ' + LTRIM(str(@suma, 12, 2)) + ' [NOP]'
PRINT ''

CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
GO
