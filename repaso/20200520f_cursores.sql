USE BDBiblioteca
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0 BEGIN
        PRINT @titulo
        FETCH NEXT FROM c1 INTO @titulo
    END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- Crear un cursor que me liste los títulos, autores y precios de los libros por editoriales 
-- y me visualice al final de cada una de ellas el total de libros de dicha editorial y el 
-- precio medio de los libros de cada una de ellas. Todo ello en la pestaña de mensajes. 
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, autor, precio, editorial FROM tlibros ORDER BY editorial 

DECLARE @titulo char(50)
DECLARE @autor char(30)
DECLARE @precio money
DECLARE @cod_ed char(4)
DECLARE @cod_ed_ant char(4)

DECLARE @cont int
DECLARE @suma money

SET @cont = 0
SET @suma = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo, @autor, @precio, @cod_ed
    SET @cod_ed_ant = @cod_ed
    WHILE @@fetch_status = 0 BEGIN
		IF @cod_ed != @cod_ed_ant BEGIN
			PRINT ''
			PRINT '#' + @cod_ed_ant + ': ' + ltrim(str(@cont, 7, 0)) + ' (' + ltrim(str(@suma / @cont, 7, 2)) + ')'
			PRINT ''
			SET @cont = 0
			SET @suma = 0
			SET @cod_ed_ant = @cod_ed
		END
		PRINT '' + @titulo + ' ' + @autor + ' ' + str(@precio, 7, 2)
		SET @cont = @cont + 1
		SET @suma = @suma + @precio
        FETCH NEXT FROM c1 INTO @titulo, @autor, @precio, @cod_ed
    END
CLOSE c1
DEALLOCATE c1

PRINT ''
PRINT '#' + @cod_ed_ant + ': ' + ltrim(str(@cont, 7, 0)) + ' (' + ltrim(str(@suma / @cont, 7, 2)) + ')'
PRINT ''





