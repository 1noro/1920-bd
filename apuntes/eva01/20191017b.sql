-- # Apuntes SQL Alfredo RG 20191017b
USE BD_biblioteca
-- # MANEJO DE FECHAS
SELECT titulo, fech_ed FROM tlibros

-- ## descomponer un valor de tipo datetime
SELECT fech_ed, day(fech_ed), month(fech_ed), year(fech_ed) 
    FROM tlibros

SELECT fech_ed, day(fech_ed) 'Día', month(fech_ed) 'Mes', year(fech_ed) 'Año'
    FROM tlibros

-- ### datepart(patron_de_valor_a_extraer, valor_datetime)
SELECT 
    fech_ed,
    datepart(dd, fech_ed) 'Día',
    datepart(mm, fech_ed) 'Mes',
    datepart(yy, fech_ed) 'Año'
FROM tlibros

SELECT 
    fech_ed,
    datepart(day, fech_ed) 'Día',
    datepart(month, fech_ed) 'Mes',
    datepart(year, fech_ed) 'Año'
FROM tlibros

-- ## FECHA DEL SISTEMA: getdate()
SELECT getdate()
SELECT getutcdate() -- greenwich

SELECT 
    getdate(),
    datepart(dd, getdate()) 'Día',
    datepart(mm, getdate()) 'Mes',
    datepart(yy, getdate()) 'Año'

-- ## SUMA Y RESTA DE FECHAS
-- ### SUMA dateadd(patron_de_valor_a_sumar, fecha_a_sumar || num, fecha)
SELECT getdate(), dateadd(dd, 4, getdate()) -- -> 2019-10-17 13:03:10.700, 2019-10-21 13:03:10.700
SELECT getdate(), dateadd(mm, 4, getdate()) -- -> 2019-10-17 13:03:55.487, 2020-02-17 13:03:55.487
SELECT getdate(), dateadd(yy, 4, getdate()) -- -> 2019-10-17 13:04:32.163, 2023-10-17 13:04:32.163
















