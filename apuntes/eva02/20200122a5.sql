-- # Apuntes SQL Alfredo RG 20200122a5
USE BD_biblioteca
 -- #EX:visualizar todos los nombres de las editoriales por tprovincia
 -- y al final de cada provincia, el numero de editoriales que tenga cada una
 -- y al final de todo, el total de prov, el total de editoriales y la prov con
 -- mas editoriales. Y que editorial tiene el libro con el titulo con mas letras

-- DECLARACIONES
DECLARE ruptura_00 CURSOR
    KEYSET LOCAL
    FOR SELECT codigo, nombre, codigo_prov
        FROM teditoriales
        ORDER BY codigo_prov

DECLARE
    @cEd char(4),
    @nEd char(35),
    @cProv int,
    @nProv char(23)

DECLARE
    @cProvAnt int

DECLARE
    @totProv int,
    @totEd int

DECLARE
    @contEd int

DECLARE
    @contEdMaxAnt int,
    @contEdMaxLAnt int

DECLARE
    @cProvMax int,
    @nEdMaxL char(35)

-- VALORES INICIALES
SET @contEd = 0
SET @contEdMaxAnt = 0
SET @contEdMaxLAnt = 0
SET @totProv = 1
SET @totEd = 0

-- MECANICA DEL CURSOR
OPEN ruptura_00
    FETCH FIRST FROM ruptura_00 INTO @cEd, @nEd, @cProv
    SET @cProvAnt = @cProv
    WHILE @@fetch_status = 0
        BEGIN
            IF @cProvAnt != @cProv
                BEGIN
                    -- PUNTO DE RUPTURA
                    SELECT @nProv = nombre FROM tprovincias WHERE @cProvAnt = codigo
                    PRINT ''
                    PRINT '#Total de ' + rtrim(@nProv) + ' (' + ltrim(str(@cProvAnt, 7, 0)) + '): ' + ltrim(str(@contEd, 7, 0))
                    PRINT ''
                    IF @contEd > @contEdMaxAnt
                        BEGIN
                            SET @contEdMaxAnt = @contEd
                            SET @cProvMax = @cProvAnt
                        END
                    SET @contEd = 0
                    SET @cProvAnt = @cProv
                    SET @totProv = @totProv + 1
                END
            PRINT ' - ' + rtrim(@nEd)
            SET @contEd = @contEd + 1
            SET @totEd = @totEd + 1
            FETCH NEXT FROM ruptura_00 INTO @cEd, @nEd, @cProv
        END

SELECT @nProv = nombre FROM tprovincias WHERE @cProvAnt = codigo
PRINT ''
PRINT '#Total de ' + rtrim(@nProv) + ' (' + ltrim(str(@cProvAnt, 7, 0)) + '): ' + ltrim(str(@contEd, 7, 0))
PRINT ''
PRINT '#Número total de Provincias: ' + ltrim(str(@totProv, 7, 0))
PRINT '#Número total de Editoriales: ' + ltrim(str(@totEd, 7, 0))
SELECT @nProv = nombre FROM tprovincias WHERE @cProvMax = codigo
PRINT '#Provincia con mas Editoriales: ' + rtrim(@nProv) + ' (' + ltrim(str(@cProvMax, 7, 0)) + '): ' + ltrim(str(@contEdMaxAnt, 7, 0))

CLOSE ruptura_00
DEALLOCATE ruptura_00 -- liberamos la memoria

-- ## OTRO CURSOR
DECLARE myCursor CURSOR
    KEYSET LOCAL
    FOR SELECT TOP 1 WITH TIES l.titulo, len(l.titulo), e.nombre
            FROM tlibros AS l, teditoriales AS e
            WHERE l.editorial = e.codigo
            ORDER BY len(titulo) DESC

DECLARE
    @myTit char(50),
    @myTitL int,
    @myNEd char(35)

PRINT '#Lista de libros con mas letras en cada editorial:'

-- MECANICA DEL CURSOR
OPEN myCursor
    FETCH FIRST FROM myCursor INTO @myTit, @myTitL, @myNEd
    WHILE @@fetch_status = 0
        BEGIN
            PRINT ' - ' + rtrim(@myTit) + ' (' + ltrim(str(@myTitL, 7, 0)) + '), ' + rtrim(@myNEd)
            FETCH NEXT FROM myCursor INTO @myTit, @myTitL, @myNEd
        END

CLOSE myCursor
DEALLOCATE myCursor -- liberamos la memoria