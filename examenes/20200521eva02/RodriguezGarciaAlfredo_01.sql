-- ALFREDO RODRIGUEZ GARCIA 1DAM 20200521
USE BD_Alumnos
GO

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO 1

DECLARE cursor1 CURSOR
    KEYSET LOCAL
    FOR SELECT 
    		c.nombre AS nombre_ciclo,
    		n.curso,
    		n.numero,
    		a.nombre AS nombre_alumno,
    		a.ape1,
    		a.ape2,
    		(n.nota1 + n.nota2 + n.nota3) / 3 AS media
    	FROM tnotas AS n, tciclos AS c, talumnos AS a
    	WHERE n.dni = a.dni AND n.ciclo = c.ciclo
    	ORDER BY c.ciclo, n.curso, n.numero

DECLARE @nombre_ciclo char(28)
DECLARE @curso int
DECLARE @numero int
DECLARE @nombre_alumno char(15)
DECLARE @ape1 char(15)
DECLARE @ape2 char(15)
DECLARE @media numeric(5, 2)

DECLARE @media_str char(2)
SET @media_str = '??'

OPEN cursor1
    FETCH FIRST FROM cursor1 INTO @nombre_ciclo, @curso, @numero, @nombre_alumno, @ape1, @ape2, @media
    WHILE @@fetch_status = 0 BEGIN
	    IF @media < 2.50 SET @media_str = 'MD'
	    ELSE IF @media < 4.75 SET @media_str = 'IN'
	    	ELSE IF @media < 6.00 SET @media_str = 'SF'
	    		ELSE IF @media < 7.00 SET @media_str = 'B '
	    			ELSE IF @media < 8.50 SET @media_str = 'NT'
	    				ELSE SET @media_str = 'SB'
        PRINT @nombre_ciclo + ' ' + str(@curso, 7, 0) + ' ' + str(@numero, 7, 0) + ' ' + @nombre_alumno + ' ' + @ape1 + ' ' + @ape2 + ' ' + str(@media, 7, 2) + ' ' + @media_str
        FETCH NEXT FROM cursor1 INTO @nombre_ciclo, @curso, @numero, @nombre_alumno, @ape1, @ape2, @media
    END
CLOSE cursor1
DEALLOCATE cursor1
GO

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- EJERCICIO 2

DECLARE cursor2 CURSOR
    KEYSET LOCAL
    FOR SELECT 
    		c.nombre AS nombre_ciclo,
    		a.dni,
    		a.nombre AS nombre_alumno,
    		a.ape1,
    		a.ape2,
    		(n.nota1 + n.nota2 + n.nota3) / 3 AS media
    	FROM tnotas AS n, tciclos AS c, talumnos AS a
    	WHERE n.dni = a.dni AND n.ciclo = c.ciclo
    	ORDER BY c.nombre

DECLARE @nombre_ciclo char(28)
DECLARE @nombre_ciclo_ant char(28)
DECLARE @dni char(9)
DECLARE @nombre_alumno char(15)
DECLARE @ape1 char(15)
DECLARE @ape2 char(15)
DECLARE @media numeric(5, 2)

DECLARE @suma_media_ciclo numeric(5, 2)
DECLARE @cont_alumno_ciclo int

DECLARE @suma_media_tot numeric(5, 2)
DECLARE @cont_ciclos int

SET @suma_media_ciclo = 0
SET @cont_alumno_ciclo = 0
SET @suma_media_tot = 0
SET @cont_ciclos = 0

OPEN cursor2
    FETCH FIRST FROM cursor2 INTO @nombre_ciclo, @dni, @nombre_alumno, @ape1, @ape2, @media
    SET @nombre_ciclo_ant = @nombre_ciclo
    WHILE @@fetch_status = 0 BEGIN
	    IF @nombre_ciclo != @nombre_ciclo_ant BEGIN
		    PRINT ''
		    PRINT '>> Medi adel ciclo, ' + @nombre_ciclo_ant + ' ... ' + ltrim(str((@suma_media_ciclo / @cont_alumno_ciclo), 7, 2))
		    PRINT ''
			SET @cont_ciclos = @cont_ciclos + 1
			SET @suma_media_tot = @suma_media_tot + (@suma_media_ciclo / @cont_alumno_ciclo)
			SET @suma_media_ciclo = 0
			SET @cont_alumno_ciclo = 0
		    SET @nombre_ciclo_ant = @nombre_ciclo
	    END
        PRINT ' ' + @dni + ' ' + @nombre_alumno + ' ' + @ape1 + ' ' + @ape2 + ' ' + str(@media, 7, 2) 
        SET @suma_media_ciclo = @suma_media_ciclo + @media
		SET @cont_alumno_ciclo = @cont_alumno_ciclo + 1
        FETCH NEXT FROM cursor2 INTO @nombre_ciclo, @dni, @nombre_alumno, @ape1, @ape2, @media
    END

SET @cont_ciclos = @cont_ciclos + 1
SET @suma_media_tot = @suma_media_tot + (@suma_media_ciclo / @cont_alumno_ciclo)
PRINT ''
PRINT '>> Medi adel ciclo, ' + @nombre_ciclo_ant + ' ... ' + ltrim(str((@suma_media_ciclo / @cont_alumno_ciclo), 7, 2))
PRINT ''
PRINT '>> Media general ... ' + str((@suma_media_tot / @cont_ciclos), 7, 2)
PRINT '>> Número de ciclos listados ... ' + str(@cont_ciclos, 7, 0)

CLOSE cursor2
DEALLOCATE cursor2