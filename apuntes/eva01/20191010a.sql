-- # Apuntes SQL Alfredo RG 20191010a
USE BDPrueba
-- # Sentencias SELECT de ejemplo
SELECT dni, nombre, edad
    FROM t_personas
    
SELECT *
    FROM t_personas

SELECT dni, nombre, edad
    FROM t_personas
    WHERE edad > 33

SELECT *
    FROM t_personas
    ORDER BY edad 

-- por defecto es ASC, por lo que es opcional ponerlo
SELECT *
    FROM t_personas
    ORDER BY edad ASC

SELECT *
    FROM t_personas
    ORDER BY edad DESC

-- no se repite ninguna edad
SELECT DISTINCT edad
    FROM t_personas
    ORDER BY edad DESC

-- ## LIKE
/* % - comodín para cualquier caracter o conjunto de caracteres
 * _ - comodín para un único caracter
 */
SELECT *
    FROM t_personas
    WHERE nombre LIKE '%e%'

SELECT *
    FROM t_personas
    WHERE nombre LIKE '_e%'

-- todos los que empizan por 'j', por 'c' o por 's'
SELECT *
    FROM t_personas
    WHERE nombre LIKE '[jcs]%'

-- todos los que tengan una 'a' o una 'l' en la 2a posición
SELECT *
    FROM t_personas
    WHERE nombre LIKE '_[al]%'

-- todos los que NO tengan una 'a' o una 'l' en la 2a posición
SELECT *
    FROM t_personas
    WHERE nombre LIKE '_[^al]%'

-- # DELETE
DELETE FROM t_personas
    WHERE edad > 30

-- borra todos los elementos de la tabla (pero no la tabla)
DELETE FROM t_personas

-- # DROP
DROP TABLE t_personas

USE master -- salimos de la BD 'BDPrueba'
DROP DATABASE BDPrueba -- borramos la DB






