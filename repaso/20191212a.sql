-- # Repaso SQL Alfredo RG 20191212a

-- union
select titulo, autor from tlibros
union
select titulo, autor from libpresta

select top 3 with ties nombre, count(*) from tlibros, teditoriales
        where editorial = codigo
        group by nombre
        having count(*) <= 10
        order by count(*) desc
