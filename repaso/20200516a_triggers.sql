USE bdmuebles
GO
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tmuebles1' AND type = 'U')
	DROP TABLE tmuebles1
GO
SELECT * INTO tmuebles1 FROM tmuebles
GO

/*
 * Crear un desencadenador que cada vez que un mueble llegué a stock mínimo cree una tupla en la tabla
 * de tpedidos con la siguiente información: el código del artículo, el código del proveedor, las
 * unidades pedidas (serán las que le faltan en ese momento para alcanzar el stock_max, y la fecha 
 * del pedido que será la fecha en la que se produce el evento. Si no existiese la tabla, el encadenador 
 * la creará. En caso de que existiese un desencadenador con este nombre se borrará este previamente. 
 * El desencadenador se creará en la base de datos BMUEBLES, independientemente de la base de datos en 
 * la que se esté en ese momento.
 * 
 * */

IF EXISTS (SELECT name FROM sysobjects WHERE name = 't_pedidos' AND type = 'TR')
	DROP TRIGGER t_pedidos
GO

CREATE TRIGGER t_pedidos
	ON tmuebles1
	AFTER UPDATE
	AS BEGIN
		IF UPDATE(stock_actual) AND (SELECT stock_actual FROM INSERTED) < (SELECT stock_min FROM INSERTED) BEGIN
			IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = 'tpedidos' AND TYPE = 'U') BEGIN
				CREATE TABLE tpedidos (codigo char(10), cod_proveedor char(4), unidades int, fecha datetime)	
			END
			INSERT INTO tpedidos
				SELECT codigo, cod_proveedor, stock_max - stock_actual, getdate()
					FROM INSERTED
					WHERE stock_actual <= stock_min
		END
	END
GO

UPDATE tmuebles1 
	SET stock_actual = 1
	WHERE codigo = 'ERTY23'
GO

SELECT * FROM tpedidos
GO

SELECT * FROM tmuebles
GO

				
				