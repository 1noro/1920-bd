declare editorialTituloLargo cursor
keyset local
for select titulo, nombre, cod_prov,editorial from tlibros, teditoriales 
	where editorial = codigo order by cod_prov 
declare @titulo varchar(50),@nombre varchar(35),@cod_prov int,@cod_provAnt int,@editorial char(4)
declare @contEd int, @totalEd int,@numMaxEdProv int, @cod_provMaxEd int,@editAnt char(4)
declare @nombreEdMaTi varchar(35),@nombreProv varchar(23),@nombreProvMax varchar(23),@sw int
declare @tnombre table (nombre varchar(35))
set @contEd = 0
set @totalEd = 0
set @numMaxEdProv = 0
set @sw = 1
open editorialTituloLargo
fetch first from editorialTituloLargo into @titulo, @nombre, @cod_prov,@editorial
set @cod_provAnt = @cod_prov
set @editAnt = @editorial
while @@fetch_status = 0
begin
	if @cod_prov != @cod_provAnt
	begin
		select @nombreProv = nombre from tprovincias where codigo = @cod_provAnt
		print''
		print 'Numero de editoriales en: '+@nombreProv+' ..........: '+str(@contEd,4)
		print''print''
		if @contEd > @numMaxEdProv
		begin
			set @numMaxEdProv = @contEd
			set @cod_provMaxEd = @cod_provAnt
		end 
		set @contEd = 0
		set @cod_provAnt = @cod_prov
	end
	if @sw = 1
	begin
 		print @nombre
		set @sw = 0
	end
	fetch next from editorialTituloLargo into @titulo, @nombre, @cod_prov, @editorial
	if @editAnt != @editorial
	begin
		set @contEd = @contEd + 1
		set @editAnt = @editorial
		set @sw = 1
	end
end
select @nombreProv = nombre from tprovincias where codigo = @cod_provAnt
print 'Numero de editoriales en: '+@nombreProv+' ..........: '+str(@contEd,4)
if @contEd > @numMaxEdProv
begin
	set @numMaxEdProv = @contEd
	set @cod_provMaxEd = @cod_provAnt
end 
select @nombreProvMax= nombre from tprovincias where codigo = @cod_provMaxEd
print' '
print 'La provincia con m�s editoriales es: '+ @nombreProvMax+' con: '+str(@numMaxEdProv,4)+' editoriales.'
insert into @tnombre
select distinct(nombre) from tlibros,teditoriales where editorial = codigo and len(titulo) = (select max(len(titulo)) from tlibros)
select * from @tnombre
close editorialTituloLargo
deallocate editorialTituloLargo

--select max(len(titulo)) from tlibros
--select distinct(nombre) from tlibros,teditoriales where editorial = codigo and len(titulo) = (select max(len(titulo)) from tlibros)
