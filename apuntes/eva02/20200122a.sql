-- # Apuntes SQL Alfredo RG 20200122a
USE BD_biblioteca
 -- #EX:visualizar todos los nombres de las editoriales por tprovincia
 -- y al final de cada provincia, el numero de editoriales que tenga cada una
 -- y al final de todo, el total de prov, el total de editoriales y la prov con
 -- mas editoriales. Y que editorial tiene el nombre con mas letras

-- DECLARACIONES
DECLARE ruptura_00 CURSOR
    KEYSET LOCAL
    FOR SELECT nombre, codigo_prov
        FROM teditoriales
        ORDER BY codigo_prov

DECLARE
    @nEd char(35),
    @cProv int,
    @nProv char(23)

DECLARE
    @cProvAnt int

DECLARE
    @totProv int,
    @totEd int

DECLARE
    @contEd int

DECLARE
    @contEdMaxAnt int,
    @contEdMaxLAnt int

DECLARE
    @cProvMax int,
    @nEdMaxL char(35)

-- VALORES INICIALES
SET @contEd = 0
SET @contEdMaxAnt = 0
SET @contEdMaxLAnt = 0
SET @totProv = 0
SET @totEd = 0

-- MECANICA DEL CURSOR
OPEN ruptura_00
    FETCH FIRST FROM ruptura_00 INTO @nEd, @cProv
    SET @cProvAnt = @cProv
    WHILE @@fetch_status = 0
        BEGIN
            IF @cProvAnt != @cProv
                BEGIN
                    -- PUNTO DE RUPTURA
                    SELECT @nProv = nombre FROM tprovincias WHERE @cProvAnt = codigo
                    PRINT ''
                    PRINT '#Total de ' + rtrim(@nProv) + ' (' + ltrim(str(@cProvAnt, 7, 0)) + '): ' + ltrim(str(@contEd, 7, 0))
                    PRINT ''
                    IF @contEd > @contEdMaxAnt
                        BEGIN
                            SET @contEdMaxAnt = @contEd
                            SET @cProvMax = @cProvAnt
                        END
                    SET @contEd = 0
                    SET @cProvAnt = @cProv
                    SET @totProv = @totProv + 1
                END
            PRINT ' - ' + rtrim(@nEd)
            IF len(@nEd) > @contEdMaxLAnt
                BEGIN
                    SET @contEdMaxLAnt = len(@nEd)
                    SET @nEdMaxL = @nEd
                END
            SET @contEd = @contEd + 1
            SET @totEd = @totEd + 1
            FETCH NEXT FROM ruptura_00 INTO @nEd, @cProv
        END

SELECT @nProv = nombre FROM tprovincias WHERE @cProvAnt = codigo
PRINT ''
PRINT '#Total de ' + rtrim(@nProv) + ' (' + ltrim(str(@cProvAnt, 7, 0)) + '): ' + ltrim(str(@contEd, 7, 0))
PRINT ''
PRINT '#Número total de Provincias: ' + ltrim(str(@totProv, 7, 0))
PRINT '#Número total de Editoriales: ' + ltrim(str(@totEd, 7, 0))
SELECT @nProv = nombre FROM tprovincias WHERE @cProvMax = codigo
PRINT '#Provincia con mas Editoriales: ' + rtrim(@nProv) + ' (' + ltrim(str(@cProvMax, 7, 0)) + '): ' + ltrim(str(@contEdMaxAnt, 7, 0)) + ''
PRINT '#Editorial con mas letras: ' + rtrim(@nEdMaxL) + ' (' + ltrim(str(@contEdMaxLAnt, 7, 0)) + ')'

CLOSE ruptura_00
DEALLOCATE ruptura_00 -- liberamos la memoria

