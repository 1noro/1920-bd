-- 20200331
USE BDBiblioteca
GO

SELECT * FROM tlibros
GO

-- Funcion escalar con un cursor
/*
 * 4 parametros
 * 
 * 1 - num de la tupla de inio
 * 2 - slto de tuplaen tupla
 * 3 - n de letras que quiero que contengan como maximo el titulo 
 * 4 - letra que le quieres enviar
 * 
 */

--------------------------------------------------------------------------------

-- TIPOS DE FUNCIONES
-- escalares 'FN'
-- # funciones en linea de tipo 'IF'
-- multisentencia 'TF'

--------------------------------------------------------------------------------

-- 20200402
-- función devuelva el precio maximo del libro de la editorial que yo envie
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'fun' and type = 'FN')
    DROP FUNCTION fun
GO 

CREATE FUNCTION fun(@cod_ed char(4)) RETURNS money
AS BEGIN
    RETURN (SELECT MAX(precio) FROM tlibros WHERE editorial = @cod_ed)
END
GO

SELECT dbo.fun('PLAZ');

-- cod edit num libros y precio max
SELECT editorial, count(titulo), dbo.fun(editorial) FROM tlibros GROUP BY editorial
GO

-- fun < code_ed y proyecta nombre_ed y precio medio de libros de esa editorial 
-- que superen el precio maximo del libro de la ed que envio

SELECT nombre, dbo.fun(codigo) precio_medio_libros
    FROM teditoriales 
    WHERE dbo.fun(codigo) > dbo.fun('PLAZ')
GO

-- 20200414 acabando el tema de funciones
-- funciones multisentencia
USE BDBiblioteca
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'multifun' and type = 'TF')
    DROP FUNCTION multifun
GO

CREATE FUNCTION multifun()
    RETURNS @tedit_prov TABLE (
        nombre_prov varchar(23),
        num_edit int
    ) BEGIN
        INSERT INTO @tedit_prov
            SELECT p.nombre, count(*)
                FROM tprovincias p, teditoriales e
                WHERE p.codigo = e.codigo_prov
                GROUP BY p.nombre 
        RETURN -- a secas, porque ya lo indicamos antes
    END
GO

SELECT * FROM multifun()
GO

-- con parámetros --------------------------
USE BDBiblioteca
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'multifun' and type = 'TF')
    DROP FUNCTION multifun
GO

CREATE FUNCTION multifun(@num int)
    RETURNS @tedit_prov TABLE (
        nombre_prov varchar(23),
        num_edit int
    ) BEGIN
        INSERT INTO @tedit_prov
            SELECT p.nombre, count(*)
                FROM tprovincias p, teditoriales e
                WHERE p.codigo = e.codigo_prov
                GROUP BY p.nombre
                HAVING count(*) >= @num
        RETURN -- a secas, porque ya lo indicamos antes
    END
GO

SELECT * 
    FROM multifun(10) 
    ORDER BY num_edit DESC
GO

-- #EX: devolver var de tabla que contiene titulo, genero, precio de todos los 
-- libros que pertenezcan a un genero enviado y que valgan igual o mas que un 
-- precio tambien enviado

/*SELECT titulo, genero, precio 
    FROM tlibros
    WHERE genero = 'Novela' AND precio >= 10*/

USE BDBiblioteca
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'multifun' and type = 'TF')
    DROP FUNCTION multifun
GO

CREATE FUNCTION multifun(@_genero varchar(15), @_precio money)
    RETURNS @tout TABLE (
        titulo varchar(50),
        genero varchar(15),
        precio money
    ) BEGIN
        INSERT INTO @tout
            SELECT titulo, genero, precio 
                FROM tlibros
                WHERE genero = @_genero AND precio >= @_precio
        RETURN -- a secas, porque ya lo indicamos antes
    END
GO

SELECT * FROM multifun('Novela', 10)
GO





