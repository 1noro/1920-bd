-- # Apuntes SQL Alfredo RG 20200219c
USE BD_biblioteca

-- hay 2 tablas temporales que existen durante la ejecucion del trigger
--  INSERTED: lo que estamos insertando (habilitada en sentencias INSERT y UPDATE)
--  DELETED: lo que hemos borrado (habilitada en sentencias DELETE y UPDATE)

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'tprimero' AND type = 'TR')
        DROP TRIGGER tprimero
GO

CREATE TRIGGER tprimero
    ON tprueba
    AFTER UPDATE -- || INSTEAD OF
    AS
        SELECT * FROM deleted
        SELECT * FROM inserted
GO

UPDATE tprueba
    SET precio = 33.25
    WHERE isbn = '0-261-10320-2'
GO
