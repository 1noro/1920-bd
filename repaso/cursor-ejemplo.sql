-- DECLARACIONES
DECLARE nombreCursor CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, precio FROM tlibros

DECLARE
    @titulo char(50),
    @precio money

-- VALORES INICIALES
SET @titulo = ''
SET @precio = 0

-- MECANICA DEL CURSOR
OPEN nombreCursor
    FETCH FIRST FROM nombreCursor INTO @titulo, @precio
    WHILE @@fetch_status = 0
        BEGIN
            PRINT ' - ' + rtrim(@titulo) + ', ' + ltrim(str(@cProvAnt, 7, 0))
            FETCH NEXT FROM nombreCursor INTO @titulo, @precio
        END

CLOSE nombreCursor
DEALLOCATE nombreCursor -- liberamos la memoria
