USE BD_Alumnos
GO

/*
USE BD_Alumnos
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'p_backup' AND type = 'P')
    DROP PROCEDURE p_backup
GO

CREATE PROCEDURE p_backup AS
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'talumnos1' AND type = 'U')
        DROP TABLE talumnos1
    SELECT * INTO talumnos1 FROM talumnos
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tnotas1' AND type = 'U')
        DROP TABLE tnotas1
    SELECT * INTO tnotas1 FROM tnotas
    IF EXISTS (SELECT name FROM sysobjects WHERE name = 'tciclos1' AND type = 'U')
        DROP TABLE tciclos1
    SELECT * INTO tciclos1 FROM tciclos
GO

EXEC p_backup
GO
*/

-- Lo mismo que antes pero que se visualize en la pantalla de mensajes recogiendo el valor 
-- de una variable de salida.

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'procedure1' AND type = 'P')
    DROP PROCEDURE procedure1
GO

CREATE PROCEDURE procedure1 @dni char(9), @media numeric(5, 2) OUTPUT AS
    SET @media = (SELECT (nota1 + nota2 + nota3) / 3 FROM tnotas WHERE dni = @dni)
    -- SELECT @media = (nota1 + nota2 + nota3) / 3 FROM tnotas WHERE dni = @dni
GO

DECLARE @miMedia numeric(5, 2)
EXEC procedure1 '77777777J', @miMedia OUTPUT
PRINT str(@miMedia, 7, 2)
GO

/*
SELECT * FROM tnotas1
SELECT * FROM talumnos1
SELECT * FROM thistorico
*/