-- # Apuntes SQL Alfredo RG 20200206a
USE BD_biblioteca

-- #EX: Hacer con un cursor el resultado de la siguiente consulta
-- SELECT titulo, autor FROM tlibros WHERE left(titulo, 1) = left(autor, 1)

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, autor
        FROM tlibros

-- DECLARACIÓN DE VARIABLES
DECLARE
    @tit char(50),
    @aut char(30)

-- VALORES INICIALES

-- MECANICA DEL CURSOR
OPEN c1
    FETCH FIRST FROM c1 INTO @tit, @aut
    WHILE @@fetch_status = 0
        BEGIN
            IF left(@tit, 1) = left(@aut, 1)
                PRINT @tit + ' ' + @aut
            FETCH NEXT FROM c1 INTO @tit, @aut
        END

CLOSE c1
DEALLOCATE c1 -- liberamos la memoria

-- #EX: mod del ant + al final num libros listados

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, autor
        FROM tlibros

-- DECLARACIÓN DE VARIABLES
DECLARE
    @tit char(50),
    @aut char(30),
    @cont int

-- VALORES INICIALES
SET @cont = 0

-- MECANICA DEL CURSOR
OPEN c1
    FETCH FIRST FROM c1 INTO @tit, @aut
    WHILE @@fetch_status = 0
        BEGIN
            IF left(@tit, 1) = left(@aut, 1) BEGIN
                PRINT @tit + ' ' + @aut
                SET @cont = @cont + 1
            END
            FETCH NEXT FROM c1 INTO @tit, @aut
        END

CLOSE c1
DEALLOCATE c1 -- liberamos la memoria

PRINT 'Libros listados: ' + ltrim(str(@cont, 7, 0))

-- #EX: mod del ant + meter el cursor en un proc al que le pasamos la letra que quiero que coincida

CREATE PROCEDURE p1 @letra char(1)
AS
    DECLARE c1 CURSOR
        KEYSET LOCAL
        FOR SELECT titulo, autor
            FROM tlibros

    -- DECLARACIÓN DE VARIABLES
    DECLARE
        @tit char(50),
        @aut char(30),
        @cont int

    -- VALORES INICIALES
    SET @cont = 0

    -- MECANICA DEL CURSOR
    OPEN c1
        FETCH FIRST FROM c1 INTO @tit, @aut
        WHILE @@fetch_status = 0
            BEGIN
                IF left(@tit, 1) = left(@aut, 1) AND left(@tit, 1) = @letra BEGIN
                    PRINT @tit + ' ' + @aut
                    SET @cont = @cont + 1
                END
                FETCH NEXT FROM c1 INTO @tit, @aut
            END

    CLOSE c1
    DEALLOCATE c1 -- liberamos la memoria

    IF @cont = 0 PRINT 'No coincide ningún libro.'
        ELSE PRINT 'Libros listados: ' + ltrim(str(@cont, 7, 0))
GO

EXEC p1 'a'
GO
