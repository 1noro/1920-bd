-- # Apuntes SQL Alfredo RG 20200213a2
USE BD_biblioteca

-- ruptura por dias de la semana
-- total de libros y el total de precio
-- al final el dia con mas y con menos 

/*SELECT titulo, precio, datename(weekday, fech_ed)
    FROM tlibros
    ORDER BY datename(weekday, fech_ed)*/

-- CURSOR
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, precio, datename(weekday, fech_ed)
            FROM tlibros
            ORDER BY datepart(weekday, fech_ed)

-- DECLARACIÓN DE VARIABLES
DECLARE
    @tit char(50),
    @precio money,
    @dia char(9),
    @diaAnt char(9)

DECLARE
    @contLibro int,
    @sumPrecio money

DECLARE
    @maxLibroNum int,
    @maxLibroNom char(9),
    @minLibroNum int,
    @minLibroNom char(9)

-- VALORES INICIALES
SET @contLibro = 0
SET @sumPrecio = 0

SET @maxLibroNum = 0
SET @minLibroNum = 999

-- MECANICA DEL CURSOR
OPEN c1
    FETCH FIRST FROM c1 INTO @tit, @precio, @dia
    SET @diaAnt = @dia
    WHILE @@fetch_status = 0
        BEGIN
            IF @dia != @diaAnt BEGIN
                PRINT ''
                PRINT '#' + @diaAnt + ': ' + ltrim(str(@contLibro, 7, 0)) + ' (' + ltrim(str(@sumPrecio, 7, 2)) + ')'
                PRINT ''
                IF (@contLibro > @maxLibroNum) BEGIN
                    SET @maxLibroNum = @contLibro
                    SET @maxLibroNom = @diaAnt
                END
                IF (@contLibro < @minLibroNum) BEGIN
                    SET @minLibroNum = @contLibro
                    SET @minLibroNom = @diaAnt
                END
                SET @contLibro = 0
                SET @sumPrecio = 0
                SET @diaAnt = @dia
            END
            PRINT ' ' + @tit + ': ' +  ltrim(str(@precio, 7, 2))
            SET @sumPrecio = @sumPrecio + @precio
            SET @contLibro = @contLibro + 1
            FETCH NEXT FROM c1 INTO @tit, @precio, @dia
        END

PRINT ''
PRINT '#' + @diaAnt + ': ' + ltrim(str(@contLibro, 7, 0)) + ' (' + ltrim(str(@sumPrecio, 7, 2)) + ')'
PRINT ''

PRINT '#Día max: ' + @maxLibroNom + ' (' + ltrim(str(@maxLibroNum, 7, 0)) + ')'
PRINT '#Día min: ' + @minLibroNom + ' (' + ltrim(str(@minLibroNum, 7, 0)) + ')'

CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
