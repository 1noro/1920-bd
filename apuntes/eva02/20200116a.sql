-- # Apuntes SQL Alfredo RG 20191216a
USE BD_biblioteca

-- #añadimos la variable @sumTotal (total final)
DECLARE ruptura_00 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, editorial, precio
        FROM tlibros
        ORDER BY editorial

--DECLARE @titulo varchar(50), @vocales int, @i int, @c char
DECLARE
    @titulo char(50),
    @editorial char(4),
    @precio money,
    @editAnt char(4),
    @sumPrecio money,
    @sumTotal money,
    @nEditorial char(35)

SET @sumPrecio = 0
SET @sumTotal = 0

OPEN ruptura_00
    FETCH FIRST FROM ruptura_00 INTO @titulo, @editorial, @precio
    SET @editAnt = @editorial
    WHILE @@fetch_status = 0
        BEGIN
            IF @editAnt != @editorial
                BEGIN
                    -- PUNTO DE RUPTURA
                    -- SELECT @nEditorial = nombre FROM teditoriales WHERE codigo = @editAnt
                    SET @nEditorial = (SELECT nombre FROM teditoriales WHERE codigo = @editAnt)
                    PRINT ''
                    PRINT '#Total ' + rtrim(@nEditorial) + ': ' + ltrim(str(@sumPrecio, 7, 2))
                    PRINT ''
                    SET @sumPrecio = 0
                    SET @editAnt = @editorial
                END
            PRINT @titulo + ' ' + str(@precio, 7, 2)
            SET @sumPrecio = @sumPrecio + @precio
            SET @sumTotal = @sumTotal + @precio
            FETCH NEXT FROM ruptura_00 INTO @titulo, @editorial, @precio
        END

PRINT ''
PRINT '#Total ' + @editAnt + ': ' + str(@sumPrecio, 7, 2)
PRINT ''

PRINT '#TOTAL FINAL: ' + str(@sumTotal, 7, 2)
PRINT ''

CLOSE ruptura_00
DEALLOCATE ruptura_00 -- liberamos la memoria



-- #titulo, autor y nombre de la editorial y precio. y los totales de libros por provincia y que salga el nombre de la provincia
DECLARE ruptura_00 CURSOR
    KEYSET LOCAL
    FOR SELECT l.titulo, l.autor, e.nombre, l.precio, e.codigo_prov
        FROM tlibros AS l, teditoriales AS e
        WHERE l.editorial = e.codigo
        ORDER BY e.codigo_prov

--DECLARE @titulo varchar(50), @vocales int, @i int, @c char
DECLARE
    @titulo char(50),
    @autor char(30),
    @nEditorial char(35),
    @precio money,
    @cprov int,
    @cprovAnt int,
    @sumPrecio money,
    @nProv char(23)
    
SET @sumPrecio = 0

OPEN ruptura_00
    FETCH FIRST FROM ruptura_00 INTO @titulo, @autor, @nEditorial, @precio, @cprov
    SET @cprovAnt = @cprov
    WHILE @@fetch_status = 0
        BEGIN
            IF @cprovAnt != @cprov
                BEGIN
                    -- PUNTO DE RUPTURA
                    SET @nProv = (SELECT nombre FROM tprovincias WHERE codigo = @cprovAnt)
                    PRINT ''
                    PRINT '#Total, ' + rtrim(@nProv) + ': ' + str(@sumPrecio, 7, 2)
                    PRINT ''
                    SET @sumPrecio = 0
                    SET @cprovAnt = @cprov
                END
            PRINT rtrim(@titulo) + ', ' + rtrim(@autor) + ', ' + rtrim(@nEditorial) + ', ' + ltrim(str(@precio, 7, 2))
            SET @sumPrecio = @sumPrecio + @precio
            FETCH NEXT FROM ruptura_00 INTO @titulo, @autor, @nEditorial, @precio, @cprov
        END

PRINT ''
PRINT '#Total, ' + rtrim(@nProv) + ': ' + str(@sumPrecio, 7, 2)
PRINT ''

CLOSE ruptura_00
DEALLOCATE ruptura_00 -- liberamos la memoria
