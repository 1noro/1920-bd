use bdmuebles
go
declare c cursor
keyset local
for select tipo,familia,denominacion,stock_actual,precio from tmuebles order by familia
declare @tip char(1),@literal char(8),@fam char(5),@denom varchar(20),@stock decimal(9),@p money,@pSt money,@pFam money,@pTot money
declare @famAnt char(5), @numArt int,@nomFam char(15)
set @pFam=0
set @pTot=0
set @numArt=0
open c
fetch first from c into @tip,@fam,@denom,@stock,@p
while @@FETCH_STATUS=0
set @famAnt=@fam
	begin
		if @fam!= @famAnt
			begin
				select @nomFam= nombre from tfamilias where codigo=@famAnt
				print 'La fam '+ @nomFam+' tiene'+str(@numArt,4) +'artículos y un valor de estock actual de'+str(@pFam,8,2)
				set @pTot=@pTot+@pFam
				set @pFam=0
				set @numArt=0
				set @famAnt=@fam
			end
		set @literal= case @tip 
						when 'A' then 'Muy alta' when 'B' then 'Alta' when 'C' then 'Normal' when 'D' then 'Baja' 
						else 'Muy baja'
					  end
		set @pSt=@stock*@p
		set @pFam=@pFam+@pSt
		set @numArt=@numArt+1
		print @literal+' '+@denom+' '+convert(char(8),@stock)+' '+convert(char(10),@p)+' '+convert(char(10),@pSt)
		fetch next from c into @tip,@fam,@denom,@stock,@p
	end
	set @pTot=@pTot+@pFam
	select @nomFam= nombre from tfamilias where codigo=@famAnt
	print 'La fam '+ @nomFam+' tiene'+ @numArt +'artículos y un valor de estock actual de'+@pFam
	print ' '
	print 'El valor total es ?'+@pTot
close c
deallocate c
