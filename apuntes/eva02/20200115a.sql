-- # Apuntes SQL Alfredo RG 20191215a
USE BD_biblioteca

-- #total de precios por editorial
DECLARE ruptura_00 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, editorial, precio
        FROM tlibros
        ORDER BY editorial

--DECLARE @titulo varchar(50), @vocales int, @i int, @c char
DECLARE
    @titulo char(50),
    @editorial char(4),
    @precio money,
    @editAnt char(4),
    @sumPrecio money

SET @sumPrecio = 0

OPEN ruptura_00
    FETCH FIRST FROM ruptura_00 INTO @titulo, @editorial, @precio
    SET @editAnt = @editorial
    WHILE @@fetch_status = 0
        BEGIN
            IF @editAnt != @editorial
                BEGIN
                    -- PUNTO DE RUPTURA
                    PRINT ''
                    PRINT '#Total ' + @editAnt + ': ' + str(@sumPrecio, 7, 2)
                    PRINT ''
                    SET @sumPrecio = 0
                    SET @editAnt = @editorial
                END
            PRINT @titulo + ' ' + str(@precio, 7, 2)
            SET @sumPrecio = @sumPrecio + @precio
            FETCH NEXT FROM ruptura_00 INTO @titulo, @editorial, @precio
        END

PRINT ''
PRINT '#Total ' + @editAnt + ': ' + str(@sumPrecio, 7, 2)
PRINT ''

CLOSE ruptura_00
DEALLOCATE ruptura_00 -- liberamos la memoria

-- #añadimos la variable @sumTotal (total final)
DECLARE ruptura_00 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, editorial, precio
        FROM tlibros
        ORDER BY editorial

--DECLARE @titulo varchar(50), @vocales int, @i int, @c char
DECLARE
    @titulo char(50),
    @editorial char(4),
    @precio money,
    @editAnt char(4),
    @sumPrecio money,
    @sumTotal money

SET @sumPrecio = 0
SET @sumTotal = 0

OPEN ruptura_00
    FETCH FIRST FROM ruptura_00 INTO @titulo, @editorial, @precio
    SET @editAnt = @editorial
    WHILE @@fetch_status = 0
        BEGIN
            IF @editAnt != @editorial
                BEGIN
                    -- PUNTO DE RUPTURA
                    PRINT ''
                    PRINT '#Total ' + @editAnt + ': ' + str(@sumPrecio, 7, 2)
                    PRINT ''
                    SET @sumPrecio = 0
                    SET @editAnt = @editorial
                END
            PRINT @titulo + ' ' + str(@precio, 7, 2)
            SET @sumPrecio = @sumPrecio + @precio
            SET @sumTotal = @sumTotal + @precio
            FETCH NEXT FROM ruptura_00 INTO @titulo, @editorial, @precio
        END

PRINT ''
PRINT '#Total ' + @editAnt + ': ' + str(@sumPrecio, 7, 2)
PRINT ''

PRINT '#TOTAL FINAL: ' + str(@sumTotal, 7, 2)
PRINT ''

CLOSE ruptura_00
DEALLOCATE ruptura_00 -- liberamos la memoria
