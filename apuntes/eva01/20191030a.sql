-- # Apuntes SQL Alfredo RG 20191030a
USE BD_biblioteca

-- borramos el objeto tabla tprueba1 con todo su contenido
DROP TABLE tprueba1 

-- creamos tprueba1 y lo llenamos con el contenido de tlibros
/*SELECT * INTO tprueba1
    FROM tlibros*/

-- para ponerme al dia con respecto a las tablas de emilio
/*SELECT *
    FROM tprueba2
    WHERE isbn = '0-261-10320-2'

UPDATE tlibros
    SET 
        titulo = 'Ya tengo título',
        precio = 45
    WHERE isbn = '0-261-10320-2'

UPDATE tprueba2
    SET 
        titulo = 'Ya tengo título',
        precio = 45 * 1.05
    WHERE isbn = '0-261-10320-2'*/

-- mostramos la comparación de la modificación de precios del día anterior
SELECT l.titulo, l.precio, p.precio
    FROM tlibros l, tprueba2 p
    WHERE l.isbn = p.isbn

-- distintas formas de hacerlo
SELECT l.titulo, l.precio, p.precio
    FROM tlibros as l, tprueba2 as p
    WHERE l.isbn = p.isbn

SELECT tlibros.titulo, tlibros.precio, tprueba2.precio
    FROM tlibros, tprueba2 
    WHERE tlibros.isbn = tprueba2.isbn

-- ##EX: crear la tabla tprueba1 que solo tenga los atributos anteriores y el autor
SELECT l.titulo 'titulo', l.autor 'autor', l.precio 'precio', p.precio 'precio_nuevo'
    INTO tprueba1
    FROM tlibros l, tprueba2 p
    WHERE l.isbn = p.isbn

SELECT * FROM tprueba1

-- ##EX: union de la tabla tprueba1 y tlibros titulo, autor, precio de las 2 tablas
SELECT titulo, autor, precio_nuevo FROM tprueba1
    UNION ALL
    SELECT titulo, autor, precio FROM tlibros

-- ##EX: lo mismo pero mostrando el precio nuevo y el viejo
SELECT titulo, autor, precio, precio_nuevo FROM tprueba1
    UNION ALL
    SELECT titulo, autor, precio, precio FROM tlibros

SELECT titulo, autor, precio, precio_nuevo FROM tprueba1
    UNION ALL
    SELECT titulo, autor, precio, precio 'precio_nuevo' FROM tlibros

SELECT titulo, autor, precio, precio_nuevo FROM tprueba1
    UNION ALL
    SELECT titulo, autor, precio, 0 FROM tlibros

SELECT titulo, autor, precio, precio_nuevo FROM tprueba1
    UNION ALL
    SELECT titulo, autor, precio, NULL FROM tlibros











