USE bdalumnos
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0 BEGIN
        PRINT @titulo
        FETCH NEXT FROM c1 INTO @titulo
    END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

--Ruptura que te dice la cantidad de alumnos matriculados que tienen los ciclos  y después listarlos por pantalla

/*
SELECT TOP 1 WITH TIES c.ciclo, COUNT(n.dni)
	FROM tnotas n, tciclos c
	WHERE n.ciclo = c.ciclo
	GROUP BY c.ciclo
	ORDER BY COUNT(n.dni) DESC

SELECT TOP 1 WITH TIES ciclo
	FROM tnotas
	GROUP BY ciclo
	ORDER BY COUNT(dni) DESC
*/

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT ciclo, dni FROM tnotas ORDER BY ciclo

DECLARE @ciclo char(3)
DECLARE @ciclo_ant char(3)
DECLARE @dni char(9)

DECLARE @alumnos_en_ciclo int

SET @alumnos_en_ciclo = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @ciclo, @dni
    SET @ciclo_ant = @ciclo
    WHILE @@fetch_status = 0
        BEGIN
            IF @ciclo != @ciclo_ant BEGIN
	            PRINT '>> ' + @ciclo_ant + ' (' + ltrim(str(@alumnos_en_ciclo, 7, 0)) + ')'
	            SET @alumnos_en_ciclo = 0
	            SET @ciclo_ant = @ciclo
            END
            -- PRINT @ciclo + ' ' + @dni
            SET @alumnos_en_ciclo = @alumnos_en_ciclo + 1
            FETCH NEXT FROM c1 INTO @ciclo, @dni
        END
CLOSE c1
DEALLOCATE c1

PRINT '>> ' + @ciclo_ant + ' (' + ltrim(str(@alumnos_en_ciclo, 7, 0)) + ')'
GO

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- Con el ejercicio anterior mostrar solo los ciclos que contienen más alumnos

DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT ciclo, dni FROM tnotas ORDER BY ciclo

DECLARE @ciclo char(3)
DECLARE @ciclo_ant char(3)
DECLARE @dni char(9)
DECLARE @maximo int

DECLARE @alumnos_en_ciclo int

SET @alumnos_en_ciclo = 0
SET @maximo = 0

OPEN c1
    FETCH FIRST FROM c1 INTO @ciclo, @dni
    SET @ciclo_ant = @ciclo
    WHILE @@fetch_status = 0 BEGIN
        IF @ciclo != @ciclo_ant BEGIN
            -- PRINT '>> ' + @ciclo_ant + ' (' + ltrim(str(@alumnos_en_ciclo, 7, 0)) + ')'
            IF @alumnos_en_ciclo > @maximo SET @maximo = @alumnos_en_ciclo
            SET @alumnos_en_ciclo = 0
            SET @ciclo_ant = @ciclo
        END
        -- PRINT @ciclo + ' ' + @dni
        SET @alumnos_en_ciclo = @alumnos_en_ciclo + 1
        FETCH NEXT FROM c1 INTO @ciclo, @dni
    END
    FETCH FIRST FROM c1 INTO @ciclo, @dni
    SET @ciclo_ant = @ciclo
    WHILE @@fetch_status = 0 BEGIN
        IF @ciclo != @ciclo_ant BEGIN
            IF @alumnos_en_ciclo = @maximo 
            	PRINT '>> ' + @ciclo_ant + ' (' + ltrim(str(@alumnos_en_ciclo, 7, 0)) + ')'
            SET @alumnos_en_ciclo = 0
            SET @ciclo_ant = @ciclo
        END
        -- PRINT @ciclo + ' ' + @dni
        SET @alumnos_en_ciclo = @alumnos_en_ciclo + 1
        FETCH NEXT FROM c1 INTO @ciclo, @dni
    END
CLOSE c1
DEALLOCATE c1

IF @alumnos_en_ciclo = @maximo 
	PRINT '>> ' + @ciclo_ant + ' (' + ltrim(str(@alumnos_en_ciclo, 7, 0)) + ')'

