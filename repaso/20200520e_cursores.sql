USE BDBiblioteca
GO

/*
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo FROM tlibros

DECLARE @titulo varchar(50)

OPEN c1
    FETCH FIRST FROM c1 INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0 BEGIN
        PRINT @titulo
        FETCH NEXT FROM c1 INTO @titulo
    END
CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
*/

PRINT '---------------------------------------------------------------------------------------'
PRINT ''

-- Escribir un cursor que obtenga el precio medio de todos los libros que estén en posiciones pares.
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT titulo, precio FROM tlibros

DECLARE @titulo char(50)
DECLARE @precio money
DECLARE @cont int
DECLARE @suma money

SET @cont = 0
SET @suma = 0

OPEN c1
    FETCH ABSOLUTE 2 FROM c1 INTO @titulo, @precio
    WHILE @@fetch_status = 0 BEGIN
		-- PRINT @titulo + ' ' + @precio
		SET @cont = @cont + 1
		SET @suma = @suma + @precio
        FETCH RELATIVE 2 FROM c1 INTO @titulo, @precio
    END
CLOSE c1
DEALLOCATE c1

PRINT @suma / @cont






