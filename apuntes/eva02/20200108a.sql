-- # Apuntes SQL Alfredo RG 20200108a
USE BD_biblioteca

-- #Leer el 1o, 2o, 4o, 7o
DECLARE cursorPrueba CURSOR
    KEYSET
    LOCAL
    FOR SELECT titulo
        FROM tlibros

DECLARE @titulo varchar(50), @incremento int, @i int

SET @incremento = 1
SET @i = 1

OPEN cursorPrueba
    FETCH ABSOLUTE @i FROM cursorPrueba INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            FETCH RELATIVE @incremento FROM cursorPrueba INTO @titulo  -- es recomendable poner el next
            PRINT @titulo
            PRINT @incremento
            PRINT @i
            SET @incremento = @incremento + 1
            SET @i = @i + @incremento
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria

-- #Leer el 1o, 2o, 4o, 7o (alternative)
DECLARE cursorPrueba CURSOR
    KEYSET
    LOCAL
    FOR SELECT titulo
        FROM tlibros

DECLARE @titulo varchar(50), @incremento int

SET @incremento = 1

OPEN cursorPrueba
    FETCH ABSOLUTE 1 FROM cursorPrueba INTO @titulo --posicionamos el cursor en el 1
    WHILE @@fetch_status = 0
        BEGIN
            PRINT str(@incremento, 3, 0) + ' ' + @titulo
            FETCH RELATIVE @incremento FROM cursorPrueba INTO @titulo  -- es recomendable poner el next
            -- PRINT @incremento
            SET @incremento = @incremento + 1
        END
CLOSE cursorPrueba
DEALLOCATE cursorPrueba -- liberamos la memoria
