-- # Apuntes SQL Alfredo RG 20200219a
USE BD_biblioteca

-- SELECT * FROM tlibros

-- #EX: isbn, titulo, autor, genero, precio cursor que lea de 3 en 3 y que solo salgan los 20 primeros
-- CURSOR
DECLARE c1 CURSOR
    KEYSET LOCAL
    FOR SELECT isbn, titulo, autor, genero, precio
            FROM tlibros

-- DECLARACIÓN DE VARIABLES
DECLARE
    @isbn char(13),
    @tit char(50),
    @aut char(30),
    @gen char(15),
    @prec money,
    @cont int

-- VALORES INICIALES
SET @cont = 0

-- MECANICA DEL CURSOR
OPEN c1
    FETCH FIRST FROM c1 INTO @isbn, @tit, @aut, @gen, @prec
    WHILE @@fetch_status = 0
        BEGIN
            PRINT str(@cont, 2, 0) + ' - ' + @isbn + ' - ' +  @tit + ' ' + @aut + ' ' + @gen + ' ' + str(@prec, 7, 2)
            SET @cont = @cont + 1
            IF (@cont = 20) BREAK
            ELSE FETCH RELATIVE 3 FROM c1 INTO @isbn, @tit, @aut, @gen, @prec
        END

CLOSE c1
DEALLOCATE c1 -- liberamos la memoria
