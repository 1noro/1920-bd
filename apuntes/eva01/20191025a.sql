-- # Apuntes SQL Alfredo RG 20191025a
USE BD_biblioteca

-- # TEMA 6 (MODIFICAR)
SELECT isbn, titulo, autor, editorial, precio
    INTO tprueba
    FROM tlibros

DROP TABLE tprueba

SELECT isbn, titulo, autor, editorial, precio
    INTO tprueba1
    FROM tlibros

SELECT titulo, precio FROM tlibros WHERE precio > 15

SELECT isbn, titulo, autor, precio
    INTO tprueba2
    FROM tlibros
    WHERE precio > 15

-- ## UNION
SELECT * FROM tprueba1 -- -> error porque no tienen los mismos atributos
    UNION SELECT * FROM tprueba2

-- Devuelve el continido de las 2 tablas (pero evita las tuplas repetidas)
SELECT  isbn, titulo, precio FROM tprueba1 
    UNION SELECT isbn, titulo, precio FROM tprueba2

-- Devuelve el continido de las 2 tablas (Y REPITE TUPLAS)
SELECT  isbn, titulo, precio FROM tprueba1
    UNION ALL SELECT isbn, titulo, precio FROM tprueba2
    
-- Devuelve el continido de las 2 tablas (pero de la 2 pone el autor en 
-- ved del titulo y repite tuplas y salen mezclados)
SELECT  isbn, titulo, precio FROM tprueba1
    UNION ALL SELECT isbn, autor, precio FROM tprueba2

-- ##EX: los libros que faltan en tprueba2 con respecto a tprueba2
SELECT * FROM tprueba1
    WHERE isbn NOT IN (SELECT isbn FROM tprueba2)

-- ##EX: borrar todos los libros de las editoriales que empiecen por volcal
SELECT editorial FROM tprueba1 WHERE editorial LIKE '[a, e, i, o, u]%'
SELECT editorial FROM tprueba1 WHERE left(editorial, 1) IN ('a', 'e', 'i', 'o', 'u')
/*
DELETE FROM tprueba1
    WHERE editorial LIKE '[a, e, i, o, u]%'
*/

-- ##EX: Visualizar la intersección de las 2 tablaas de pruebas
SELECT titulo
    FROM tprueba1
    WHERE isbn IN (SELECT isbn FROM tprueba2)

SELECT titulo
    FROM tprueba2
    WHERE isbn IN (SELECT isbn FROM tprueba1)





