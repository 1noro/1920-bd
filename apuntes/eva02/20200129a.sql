-- # Apuntes SQL Alfredo RG 20200129a
USE BD_biblioteca

-- #EX: Nombre de la editorial cuando le pasas el ISBN de un libro.
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'consultaEd' AND type = 'P')
        DROP PROCEDURE consultaEd
GO

CREATE PROCEDURE consultaEd @_isbn char(13), @nEd char(35) OUTPUT AS
    SELECT @nEd = e.nombre
        FROM tlibros AS l, teditoriales AS e
        WHERE l.editorial = e.codigo AND l.isbn = @_isbn
GO

DECLARE 
    @myISBN char(13),
    @myNEd char(35)

SET @myISBN = '0-261-10320-2'
    
EXEC consultaEd @myISBN, @myNEd OUTPUT
PRINT 'La editorial del ISBN "' + rtrim(@myISBN) + '" es: ' + rtrim(@myNEd)
GO

-- #EX: Que liste el titulo, autor y fecha de edicion de todos los libros editados en una 
--      fecha posterir a la fecha que pasamos como parametro.
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'consultaFecha' AND type = 'P')
        DROP PROCEDURE consultaFecha
GO

CREATE PROCEDURE consultaFecha @_fecha datetime AS
    SELECT titulo, autor, fech_ed
        FROM tlibros
        WHERE fech_ed > @_fecha
        ORDER BY fech_ed
GO

DECLARE @myFecha datetime
SET @myFecha = '1993-01-01' 
EXEC consultaFecha @myFecha
GO

-- #EX: basandonos en el anterior enviar 2 fechas, mostrar los comprendidos entre las 2 sin importar el orden de ambas.
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'consultaRangoFecha' AND type = 'P')
        DROP PROCEDURE consultaRangoFecha
GO

CREATE PROCEDURE consultaRangoFecha @_fecha1 datetime, @_fecha2 datetime AS
    DECLARE @aux datetime
    IF @_fecha1 > @_fecha2 BEGIN
        SET @aux = @_fecha1
        SET @_fecha1 = @_fecha2
        SET @_fecha2 = @aux
    END
    SELECT titulo, autor, fech_ed
        FROM tlibros
        WHERE fech_ed > @_fecha1 AND fech_ed < @_fecha2
        ORDER BY fech_ed
GO

EXEC consultaRangoFecha '1995-01-01','1993-01-01'
GO
